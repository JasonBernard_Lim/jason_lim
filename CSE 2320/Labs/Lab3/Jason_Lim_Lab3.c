//Jason Bernard Lim
//ID: 1001640993
//2320-004
//Compilation Instructions:
//gcc Jason_Lim_Lab3.c 
//./a.out < lab3.dat
//NOTE: lab3.dat already has input from one of the test cases given to us (test case: a)
//NOTE: If the lab3.dat file is ever empty at any point, the program when compiled
//      and executed correctly will cause a segmentation fault.
//This program takes in the number of probabilites and the probabilities themselves and sorts them in a tree using
//      the dynamic matrix multiplication. The program then implements order-preserving Huffman to solve the bits
//      and the expected bits per symbol.
#include <stdlib.h>
#include <stdio.h>

//init the global variable *Given to us by the dynamic programming code (all other code given to us will be noted by the *)
//NOTE: The globals are here as they were given to use like this
double p[50];
int size = 0;
double c[50][50];
int trace[50][50];
double expectedBits[50];
int bits = 0;

void tree(int left,int right,int indent) { //*this function prints out the tree given the left node, right node, and the amount of indent
    int i;

    if (left==right) {
    for (i=0;i<indent;i++)
        printf("   ");
        printf("%d\n",left);
        return;
    }
    tree(trace[left][right]+1,right,indent+1);
    for (i=0;i<indent;i++)
    printf("   ");
    printf("%d %d cost %f\n",left,right, c[left][right]);
    tree(left,trace[left][right],indent+1);
}

double summation_of_probs(int left, int right) {//this function calculates the summation of the probabilities given the left and the right
    int i;
    double sum = 0;
    for(i = left; i <= right; i++) {//the summation from left to right of he probabilities
        sum = sum + p[i];
    }
    return sum;
}

void bitsPerSymbol(int left, int right, int bits, int i) {//this function prints out the bit codes and the bits per symbol and stores the bps in an array to find the total bits later
                                                          //*this function is mainly a modified version of the tree function
    if(left == right) {//if the left equals the right we are at the node we are looking for
        expectedBits[left] = bits * p[i]; //store the value into the expected bits array to be totaled later
        printf(" %f\n", expectedBits[left]); //print out this value
        return;
    }
    if(i >= trace[left][right]+1) {//if i which is the node that we are looking for is greater than the right side of k
        printf("1"); //print out a 1 and go right in the tree
        bitsPerSymbol(trace[left][right]+1, right, bits+1,i);
    }
    else { //else print out a 0 and go left in the array
        printf("0");
        bitsPerSymbol(left, trace[left][right], bits+1,i);
    }

}

int main(int argc, char **argv) {//*most of the main is given to us by the dynamic matrix multiplication code
    //setup the variables for the loop and the work(cost) variable
    int i,j,k;
    double work;

    char line[50];
    fgets(line,50,stdin);
    int size = atoi(line);

    for(i = 0; i < size; i++) {//store in the probabilities into our array
        fgets(line,50,stdin);
        p[i] = atof(line); 
    }

    for (i=1;i<=size;i++) {//set the diagonal of the matrix to 0
        c[i][i]=trace[i][i]=0;
    }

    for (i=1;i<size;i++) {//i = 1 so we do not touch the diagonal
        for (j=0;j<=size-i;j++) {//the j increments through all the non-diagonal values in the matrix
            c[j][j+i]=999999;//set the sentinel value
            for (k=j;k<j+i;k++) {//while going from left to right in the matrix set the cost function
            work = summation_of_probs(j, j+i) + c[j][k] + c[k+1][j+i];
                if (c[j][j+i]>work) {
                    c[j][j+i]=work;
                    trace[j][j+i]=k;
                }
            }
        }
    }
    //since we are dealing with huffman the 1.000000 can be hardcoded as all the probabilities should add up to it
    printf("probabilities sum to 1.000000\n");
    printf("Code tree for exact:\n");
    //print out the tree
    tree(0,size-1,0);
    //print out the bit codes and the probabilities
    printf("Codes & prob*# of bits\n");
    double total = 0;
    for(i = 0; i < size; i++) {//print out and add the total expected bits per symbol per the size
        printf("%d ", i);
        bitsPerSymbol(0,size-1, bits,i);
        total = total + expectedBits[i];
    }
    printf("Expected bits per symbol %f\n", total); //output the total
}