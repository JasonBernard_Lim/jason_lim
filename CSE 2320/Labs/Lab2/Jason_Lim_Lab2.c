//Jason Bernard Lim
//ID: 1001640993
//2320-004
//Compilation Instructions:
//gcc Jason_Lim_Lab2.c Lab2Functions.c
//./a.out < lab2.dat
//NOTE: lab2.dat already has input from one of the test cases given to us (test case: a)
//NOTE: If the lab2.dat file is ever empty at any point, the program when compiled
//      and executed correctly will cause a segmentation fault.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lab2Functions.h"

/*This program reads in serveral "baskets" of laudry with washing times and
  drying times. It then sorts the baskets by the lower of the two times for each
  basket, then applys Johnson's rule. The program will then output all the information
  which includes: index, washing time, drying time, wash start time, dryer
  start time and the makespan. If there are any dryer gaps during the process the
  program will output it to the user.*/
int main(int argc, char **argv) {

  //initialize a char array for input and i for loops
  char line[20];
  int i;
  //initialize the size variable for our baskets and a token pointer to help with initialization
  int size;
  char *token;

  //store the first line of input then strtok to get the number of baskets then store it into the size variable
  fgets(line, 20, stdin);
  token = strtok(line, " \n");

  size = atoi(token);

  //malloc our array of baskets
  Basket *array = (Basket *)malloc(sizeof(Basket) * size);

  //store all the information into the baskets
  read_In(array, size);

  //use qsort() to arrange the baskets by lowest time
  qsort(array, size, sizeof(Basket), cmpfunc);


  //Johnson's Rule Implementation
  Basket *sorted = (Basket *)malloc(sizeof(Basket) * size);
  //initialize a lower index and higher index to keep track of indeces
  int highest_index = size - 1;
  int lower_index = 0;

  for(i = 0; i < size; i++) {//for as long as the size of the baskets array
    if(array[i].lower_time == array[i].wash_time) {//if the lower time is wash time store it in the beginning of the array then update the lower index
      sorted[lower_index] = array[i];
      lower_index = lower_index + 1;
    }
    else if(array[i].lower_time == array[i].dry_time){//if the lower time is dry time store it in the end of the array then update the higher index
      sorted[highest_index] = array[i];
      highest_index = highest_index - 1;
    }
  }

  //print out all of the info
  schedule(sorted, size);

  //free our basket arrays
  free(array);
  free(sorted);
}
