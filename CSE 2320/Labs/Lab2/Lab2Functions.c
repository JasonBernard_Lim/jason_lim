#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lab2Functions.h"

//the cmpfunc() takes the lower_time from one basket and the next and returns the basket with the lower time
int cmpfunc (const void * a, const void * b) {//this function is necessary for qsort
  //the function sorts the baskets using the lower time between washing and drying
  //and sorting the lower times in ascending ordered
  int l = ((Basket *)a)->lower_time;
  int r = ((Basket *)b)->lower_time;
   return (l - r);
}

//the read_In() takes the array of baskets and the size of the array and stores it with the correct information
void read_In(Basket *array, int size) {
  //create the variables to aid us with storing in info
  char line[20];
  char *token;
  int i;

  for(i = 0; i < size; i++) {//for as long as the size of the array
    int lower_time; //initialize the lower_time variable
    fgets(line, 20, stdin); //take the first line of input and split it by space
    token = strtok(line, " ");

    array[i].wash_time = atoi(token); //store into the wash_time variable at index i
    lower_time = array[i].wash_time; //lower_time is wash_time

    token = strtok(NULL, " \n");

    array[i].dry_time = atoi(token); //store into the dry_time variable at index i

    if(array[i].dry_time < array[i].wash_time) { //lower_time is dry_time
      lower_time = array[i].dry_time; //check if lower_time is the dry_time, if so, update lower_time
    }

    array[i].lower_time = lower_time; //store the variable into index i

    array[i].index = i; //store the index into the struct
  }
}

//the schedule() takes the basket array and the size and prints out all of the scheduling info needed
void schedule(Basket *array, int size) {
  //initialize all of the variables needed
  int wash_start_time = 0;
  int wash_finish_time = 0;
  int dry_start_time = 0;
  int dry_finish_time = 0;
  int i;
  int total_dry_gap = 0;

  //Update the dry_start_time, wash_finish_time, and dry_finish_time
  //Print our information for the first basket which is a special case
  dry_start_time = dry_start_time + array[0].wash_time;

  wash_finish_time = wash_finish_time + array[0].wash_time;

  dry_finish_time = wash_finish_time + array[0].dry_time;

  printf("%d %d %d %d %d\n", array[0].index, array[0].wash_time, array[0].dry_time, wash_start_time, dry_start_time);

  //wash_start_time is updated after since you need 0 for the first basket to be the wash_start_time
  wash_start_time = wash_start_time + array[0].wash_time;

  //Go through a for loop for the rest of the baskets
  for(i = 1; i < size; i++) {//for as long as the basket array
    //initialize a dry_gap variable and update wash_finish_time for dryer gap check
    int dry_gap = 0;
    wash_finish_time = wash_finish_time + array[i].wash_time;

    if(wash_finish_time > dry_finish_time) {//if a dryer gap exists print out the gap
      printf("dryer gap from %d to %d\n", dry_finish_time, wash_finish_time);
      //update the dryer gap and total dryer gap along with the dryer start and finish times
      dry_gap = dry_gap + (wash_finish_time - dry_finish_time);
      total_dry_gap = total_dry_gap + dry_gap;

      dry_start_time = dry_start_time + array[i-1].dry_time;

      dry_finish_time = wash_finish_time + array[i].dry_time;

      //print out the info then update wash start time and dry start time according to the special dryer gap state
      printf("%d %d %d %d %d\n", array[i].index, array[i].wash_time, array[i].dry_time, wash_start_time, wash_finish_time);

      wash_start_time = wash_start_time + array[i].wash_time;

      dry_start_time = dry_start_time + dry_gap;
    }
    else {//if no dryer gap exists update dry start time and dry finish time normally
      dry_start_time = dry_start_time + array[i-1].dry_time;

      dry_finish_time = dry_start_time + array[i].dry_time;

      //print out the info then update the wash start time
      printf("%d %d %d %d %d\n", array[i].index, array[i].wash_time, array[i].dry_time, wash_start_time, dry_start_time);

      wash_start_time = wash_start_time + array[i].wash_time;
    }
  }

  //print out the makespan
  printf("makespan is: %d\n", (dry_start_time + array[size-1].dry_time));
}
