//Jason Bernard Lim
//ID: 1001640993
//This is the header file for my functions used in Lab 2.
//For deatailed descriptions see Lab2Functions.c
#ifndef LAB2FUNCTIONS
#define LAB2FUNCTIONS

struct basket {//this is the basket struct which includes the wash time, dry time, the original index of the basket, and the lower of the two times
  int wash_time;
  int dry_time;
  int index;
  int lower_time;
};
typedef struct basket Basket;

int cmpfunc (const void * a, const void * b); //this function is necessary for qsort
void read_In(Basket *array, int size);
void schedule(Basket *array, int size);



#endif
