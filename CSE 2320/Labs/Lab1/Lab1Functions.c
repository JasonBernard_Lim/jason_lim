//Jason Bernard Lim
//ID: 1001640993
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lab1Functions.h"

//The binarySearch() allows us to take the high and the low and calculate our "mid"
int binarySearch(int *low, int *high) {//arguments are the high and low pointers
  //take the high and the low, add them, then divide by 2
  return (*low + *high) / 2;
}

//The read_in() allows us to store the values into our data arrays (NOT INCLUDING THE RANK ARRAY)
void read_in(int *array, int size) {//arguments are the array and size of the array
  //initialize the sentinel variables
  array[0] = -99999;
  array[size+1] = 99999;

  //create a char array to store in the input from the file
  char line[10];

  //initialize a variable for our for loop
  int i;
  for(i = 1; i < size+1; i++) {
    //fgets the number from the line as long as there is space left in the array and store it into the array
    fgets(line,10,stdin);
    //store in the value into the array
    array[i] = atoi(line);
  }
}

//The read_in_ranks() allows us to store the values into our rank array
void read_in_ranks(int *array, int size) {//arguments are the rank array and the size of the rank array
  //initialize the char array for input and the value for our loop
  //NOTE: the sentinel values are not needed for the rank array hence the need to create a new function
  int i;
  char line[10];

  for(i = 0; i < size; i++) {//same as the last read in function (SEE read_in())
    fgets(line,10,stdin);
    array[i] = atoi(line);
  }
}

//The element_in_arrayA() checks to see if the value in arrayA[i] is the value for the correct rank we are looking for
int element_in_arrayA(int *a, int *b, int i, int j, int rank) {//arguments are the a array, the b array, i, j, and the desired rank
  if(i + j == rank) {//if i + j is equal to the rank needed
    if(b[j] < a[i]) {//if the value of b[j] is less than a[i]
      if(a[i] <= b[j+1]) {//if the value of a[i] is less than or equal to b[j+1]
        return 1; //if all of the conditions are met the rank has been found
      }
    }
  }
  return 0; //if any of the conditions fail the rank has not been found
}

//The element_in_arrayB() checks to see if the value in arrayB[j] is the value for the correct rank we are looking for
int element_in_arrayB(int *a, int *b, int i, int j, int rank) {//arguments are the a array, b array, i, j, and the desired rank
  if(i + j == rank) {//if i + j is equal to the rank needed
    if(a[i] <= b[j]) {//if the value of a[i] is less than or equal to b[j]
      if(b[j] < a[i+1]) {//if the value of b[j] is less than a[i+1]
        return 1; //if all of the conditions are met the rank has been found
      }
    }
  }
  return 0; //if any of the conditions fail the rank has not been found
}
