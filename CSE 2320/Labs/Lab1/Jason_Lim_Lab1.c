//Jason Bernard Lim
//ID: 1001640993
//2320-004
//Compilation Instructions:
//gcc Jason_Lim_Lab1.c Lab1Functions.c
//./a.out < labl.dat
//NOTE: lab1.dat already has input from one of the test cases given to us (test case: a)
//NOTE: If the lab1.dat file is ever empty at any point, the program when compiled
//      and executed correctly will cause a segmentation fault.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lab1Functions.h"

/*This program reads in two ordered integer sequences and a sequence of ranks. The
  program then indicates, from the sequence of rank, the corresponding element.
  The program traces the binary search through the output statements when executing
  the program.*/
int main(int argc, char **argv) {

  //initialize a char array to take in input
  char line[20];
  //initialize 3 size variables for our array
  int a_size, b_size, rank_size;
  //initialize a counter variable to track which rank we are looking for in the array
  int rankCounter;
  //initialize a token char pointer to help us with the initializing of our arrays
  char *token;

  //fgets to store in the first line of input
  fgets(line, 20, stdin);

  //strtok the line by a space to get the size of our a array.......etc
  token = strtok(line, " ");
  //store the values into our variables
  a_size = atoi(token);

  token = strtok(NULL, " ");
  b_size = atoi(token);

  token = strtok(NULL, " \n");
  rank_size = atoi(token);

  //initialize the arrays by using dynamic allocation
  int *a = (int*)malloc((sizeof(int) * a_size) + (sizeof(int) * 2));
  int *b = (int*)malloc((sizeof(int) * b_size) + (sizeof(int) * 2));
  //notice how ranks does not need the times 2 as it does not need any sentinel values
  int *ranks = (int*)malloc((sizeof(int) * rank_size));

  //read in all of our arrays by using the functions
  // SEE Lab1Functions.c
  read_in(a, a_size);
  read_in(b, b_size);
  read_in_ranks(ranks, rank_size);

  //dynamically allocate our low and high variables
  int *high = malloc(sizeof(int)), *low = malloc(sizeof(int)), i, j;

  //now use a for loop to start our search for the ranks
  //the loop finishes when the rankCounter goes through all the ranks in the ranks array
  for(rankCounter = 0; rankCounter < rank_size; rankCounter++) {
    //initialize a check variable to know when we have found the correct rank
    int check = 0;

      //initialize a variable to know which rank we are looking for
      int rank = ranks[rankCounter];

      //find the values of our low and high
      if(rank <= a_size) {//if the rank you are looking for is less than the size of the first array
        *high = rank;
        *low = 0;
      }
      else if(rank <= b_size) {//if the rank you are looking for is greater than the size of the first
                               // array but less than the size of the second
        *high = a_size;
        *low = 0;
      }
      else {//if neither of the conditions are met the rank you are looking for has to be greater
            //  than both sizes of the arrays
        *high = a_size;
        *low = rank - b_size;
      }

    //finding the correct rank
    while(check != 1) {//while the rank has not been found, go through the while loop

      //perform a binary search to find our "mid" in this case the variable i
      i = binarySearch(low, high);
      //calculate j by taking the desired rank and subracting i from it
      j = rank - i;

      if(a[i] > b[j]) {//if the value of the first array at i is greater than the value of the second
                       // array at j

        //SEE: Lab1Functions.c
        check = element_in_arrayA(a,b,i,j,rank);
        //print out the info
        printf("low %d high %d i %d j %d b[%d]=%d a[%d]=%d b[%d]=%d\n", *low, *high, i, j, j, b[j], i, a[i], (j+1), b[j+1]);

        if(check == 1) {//if the rank has been found print out the desired rank
          printf("a[%d]=%d has rank %d\n", i, a[i], rank);
        }
        else {//if the rank was not found create the new high value as i - 1
          *high = i - 1;
        }
      }
      else {//if the value of the first array at i is less than the value of the second array at j

        //SEE Lab1Functions.c
        check = element_in_arrayB(a,b,i,j,rank);
        //print out info
        printf("low %d high %d i %d j %d a[%d]=%d b[%d]=%d a[%d]=%d\n", *low, *high, i, j, i, a[i], j, b[j], (i+1), a[i+1]);

        if(check == 1) {//if the rank has been found print out the desired rank
          printf("b[%d]=%d has rank %d\n", j, b[j], rank);
        }
        else {//if the rank was not found create the new low value as i + 1
          *low = i + 1;
        }
      }
    }
  }
  //free the memory from our dynamically allocated variables
  free(a);
  free(b);
  free(ranks);
  free(high);
  free(low);

}
