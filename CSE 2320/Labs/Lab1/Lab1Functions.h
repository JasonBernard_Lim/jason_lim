//Jason Bernard Lim
//ID: 1001640993
//This is the header file for my functions used in Lab 1.
//For deatailed descriptions see Lab1Functions.c
#ifndef LAB1FUNCTIONS
#define LAB1FUCNTIONS

void read_in(int *array, int size);
int binarySearch(int *low, int *high);
void read_in_ranks(int *array, int size);
int element_in_arrayA (int *a, int *b, int i, int j, int rank);
int element_in_arrayB(int *a, int *b, int i, int j, int rank);


#endif
