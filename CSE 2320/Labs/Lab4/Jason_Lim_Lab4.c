//Jason Bernard Lim
//ID: 1001640993
//2320-004
//Compilation Instructions:
//gcc Jason_Lim_Lab4.c Lab4Functions.c
//./a.out < lab4.dat
//NOTE: lab4.dat already has input from one of the test cases given to us 
//NOTE: If the lab4.dat file is ever empty at any point, the program when compiled
//      and executed correctly will cause a segmentation fault.
//NOTE: This main was given to us in the lab
// Reads a string for a serialized RB tree, deserializes it,
// performs some insertions, then serializes the revised tree.
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Lab4Functions.h"

int main()
{
int inputBytes;
char *inputString,*outputString;
char formatString[100];
int insertKeys,i,key;

scanf("%d",&inputBytes);
inputString=(char*) malloc(inputBytes);
if (!inputString) {
  printf("malloc failed %d\n",__LINE__);
  exit(0);
  }
sprintf(formatString,"%%%ds",inputBytes);
scanf(formatString,inputString);
STinit();
STdeserialize(inputString);
free(inputString);
//STprintTree();
scanf("%d",&insertKeys);
for (i=0;i<insertKeys;i++) {
  scanf("%d",&key);
  STinsert(key);
  }
//STprintTree();
outputString=STserialize();
printf("%lu %s\n",strlen(outputString)+1,outputString);
free(outputString);
}