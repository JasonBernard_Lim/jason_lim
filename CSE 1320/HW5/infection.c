// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 4: Write a Program

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  char *country;        /*country	name*/
  char virus;           /*virus*/
  int start_cases;      /*number of initial	cases	reported*/
  float infection_rate; /*daily rate of	infection*/
  struct Node *next;
} Node;

Node *readfile(int n) {

  FILE *fp = fopen("infections.txt", "r");
  char *token;
  char *line = (char *)malloc(100);
  Node *head = NULL;
  Node **current = &head;
  Node *temp;

  if (fp == NULL) {
    printf("\n\nNo file opened...\nExiting...");
    exit(0);
  } else {

    while (fgets(line, 100, fp) != NULL) {
      token = strtok(line, ",");

      temp = (Node *)malloc(sizeof(Node));
      temp->country = (char *)malloc(20);
      strcpy(temp->country, token);

      token = strtok(NULL, ",");
      temp->virus = token[0];

      token = strtok(NULL, ",");
      temp->start_cases = atoi(token);

      token = strtok(NULL, ",\n");
      temp->infection_rate = atof(token);

      *current = temp;
      current = &temp->next;
    }
  }
  *current = NULL;

  free(line);
  fclose(fp);
  return head;
}

void free_node(Node *head) { // this function frees the linked list, the
                             // parameter is the head of the list
  Node *temp;
  while (head != NULL) {
    temp = head;
    head = head->next;
    free(temp->country);
    free(temp);
  }
}

void next_day(
    Node *head) { // this program changes the infection cases from one day to
                  // the next, it takes the head of the linked list
  while (head != NULL) {
    head->start_cases = head->start_cases * (head->infection_rate + 1);
    head = head->next;
  }
}

void print_out(
    Node *head, int day,
    int threshold) { // this function prints out the infection info, the
                     // parameters are the head of the linked list, the current
                     // day, and the threshold for the cases
  while (head != NULL) {
    float actual_cases =
        head->start_cases * pow((1 + head->infection_rate), day);
    if (actual_cases >= threshold) {
      printf(
          "-%s-cases %0.0f (Actual: %0.4f | rate: %0.0f%% daily) Careful...\n",
          head->country, actual_cases, actual_cases,
          (head->infection_rate * 100));
    } else {
      printf("-%s-cases %0.0f (Actual: %0.4f | rate: %0.0f%% daily)\n",
             head->country, actual_cases, actual_cases,
             (head->infection_rate * 100));
    }
    head = head->next;
  }
}

int send_workers(
    Node *head,
    int workers) { // this function sends the aid workers to the specific
                   // contry, the parameters are the head of the linked list, and
                   // the current number of aid workers
  char line[100];
  char *token;
  int country_num = 0;
  int workers_sent = 0;
  int i;
  printf("Which country to send aid and how many? ");
  fgets(line, 100, stdin);
  strtok(line, " ");

  token = strtok(NULL, " ");
  if (token == NULL) {
    printf("\nIncorrect Input...\n\n");
    return 0;
  }
  country_num = atoi(token);

  token = strtok(NULL, " \n");
  if (token == NULL) {
    printf("\nIncorrect Input...\n\n");
    return 0;
  }
  workers_sent = atoi(token);

  while (workers_sent % 10 != 0 || workers_sent > workers || workers_sent < 0) {
    if (workers_sent % 10 != 0) {
      printf("Not a valid number of aid workers. Enter a multiple of 10. ");
    } else if (workers_sent > workers) {
      printf("We don't have that many aid workers available. We only have %d. ",
             workers);
    } else if (workers_sent < 0) {
      printf("Aid workers sent cannot be negative. ");
    }
    scanf("%d", &workers_sent);
    fgets(line, 100, stdin);
  }

  for (i = 0; i < (country_num - 1); i++) {
    head = head->next;
  }

  if (head->infection_rate == 0) {
    printf("Already 0%% infection rate. Nothing more to do.\n\n");
    return 0;
  } else if ((head->infection_rate * 100) - ((workers_sent / 10)) < 0) {
    printf(
        "Cannot send %d workers here. Would make infection rate negative.\n\n",
        workers_sent);
    return 0;
  } else {
    head->infection_rate = head->infection_rate - ((workers_sent / 10) * .01);
    if (head->infection_rate < 0) {
      head->infection_rate = 0;
    }
    printf("\nRate for %s is now %0.2f.\n", head->country,
           head->infection_rate);
    return workers_sent;
  }
}

int main(int argc, char **argv) {
  if (argv[1] == NULL) {
    printf(
        "\n\nThreshold was not input into the command line. Exiting......\n\n");
    exit(0);
  }
  int workers = 50;
  int threshold = atoi(argv[1]);
  Node *infections = readfile();
  char answer[10];
  int day = 0;

  while (workers != 0) {
    printf("\n--Day %d (Aid Workers Available: %d)\n\n", day, workers);
    print_out(infections, day, threshold);
    fgets(answer, 10, stdin);

    if (answer[0] == 's') {
      workers = workers - send_workers(infections, workers);
    }
    day++;
  }
  printf("No more aid workers available. Exiting program....\n\n");
  free_node(infections);
  exit(0);
}
