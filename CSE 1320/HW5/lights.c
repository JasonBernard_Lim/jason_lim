// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 3: Write a Program

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  char *color;   /*color of the	light*/
  int **details; /*holds the brightness	and	size*/
  struct Node *next;
} Node;

Node *light_info(char *filename) {

  FILE *fp = fopen(filename, "r");
  char *token;
  char *line = (char *)malloc(100);
  Node *head = NULL;
  Node **current = &head;
  Node *temp;

  if (fp == NULL) {
    printf("\n\nNo file opened...\nExiting...");
    exit(0);
  } else {

    while (fgets(line, 100, fp) != NULL) {
      token = strtok(line, ",");

      temp = (Node *)malloc(sizeof(Node)); // malloc whole struct
      temp->color = (char *)malloc(20);    // malloc color
      strcpy(temp->color, token);

      temp->details = (int **)malloc(sizeof(int *)); // malloc whole details
      temp->details[0] = (int *)malloc(2 * sizeof(int));

      token = strtok(NULL, ",");
      temp->details[0][0] = atoi(token);

      token = strtok(NULL, ",\n");
      temp->details[0][1] = atoi(token);

      *current = temp;
      current = &temp->next;
    }
  }
  *current = NULL;

  free(line);
  fclose(fp);
  return head;
}

void lights_on(Node *head) { // function turns the lights on the parameter is
                             // the head of the linked list
  printf("\n\n");
  printf("***Turning lights on:\n");
  int i;

  while (head != NULL) {
    char light = head->color[0];

    for (i = 0; i < head->details[0][0]; i++) {
      printf("%c", light);
    }

    printf(" ");

    head = head->next;
  }
}

void lights_off(Node *head) { // function turns the lights off the parameter is
                              // the head of the linked list
  printf("\n\n");
  printf("***Turning lights off:\n");
  int i;

  while (head != NULL) {
    for (i = 0; i < head->details[0][0]; i++) {
      printf("-");
    }

    printf(" ");

    head = head->next;
  }
}

void free_node(Node *head) { // this function frees the linked list the
                             // parameter is the head of the list
  Node *temp;
  while (head != NULL) {
    temp = head;
    head = head->next;
    free(temp->color);
    free(temp->details[0]);
    free(temp->details);
    free(temp);
  }
}

int main(int argc, char **argv) {
  int check_lights = 0; // this is going to be used to see if on or off has been
                        // typed in already
  char input[10];
  char *filename = argv[1];

  Node *lights = light_info(filename);

  int check = 1;

  while (check) {
    printf("\n\nWhat would you like to do: ");
    scanf("%s", input);

    if (strcmp(input, "off") == 0) {
      if (check_lights == 0) { // lights are already off
        printf("\n\n--Lights are already off.\n\n");
      } else {
        lights_off(lights);
        printf("\n\n");
        check_lights = 0;
      }
    } else if (strcmp(input, "on") == 0) {
      if (check_lights == 1) { // lights are already on
        printf("\n\n--Lights are already on.\n\n");
      } else {
        lights_on(lights);
        printf("\n\n");
        check_lights = 1;
      }
    } else if (strcmp(input, "exit") == 0) { // exit
      printf("\n\nExiting...\n");
      check = 0;
      // free(input);
      free_node(lights);
    } else { // Incorrect input
      printf("\n\n--Incorrect Input.\n\n");
    }
  }
}
