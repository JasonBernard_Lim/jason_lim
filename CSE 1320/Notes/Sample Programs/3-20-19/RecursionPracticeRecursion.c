#include <stdio.h>

//recursion for fibonacci sequence

int fib_rec(int n){
  if(n==1 || n==2){
    printf("base case! n=%d, returning 1\n",n);
    return 1;
  }

  int stuff = fib_rec(n-1)+fib_rec(n-2); /*function is calling itself*/
  printf("\n\n n=%d, Total Returned:%d fib(%d-1), fib(%d-2)\n\n",n,stuff,n,n);
  return stuff;
}

//recursion for factorials

int factorial_rec(int n){
  if(n==1){
    return 1;
  }
  int num = n*factorial_rec(n-1);
  printf("value returned: %d\n",num);
  return nun;
}

//recursion for binary conversion

void dec_binary_rec(int num){
  if(num == 0){
    return ;
  }

  dec_binary_rec(num / 2);
  printf("%d", num % 2);

}
