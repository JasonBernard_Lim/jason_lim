#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct cookie{
  char message[100];
  int lucky_nums[6];

}fortune_cookie;

void fill_cookies(fortune_cookie *f){
  char line[500];
  int i,j;
  char *token;

  fgets(line,500,stdin);
  token = strtok(line,",");

  for(i = 0, i < 2; i++){
    token = strtok(NULL,",");
    strcpy(f[i].message, token);
    for(j = 0; j < 6; j++){
      token = strtok(NULL, ",\n");
      f[i].lucky_nums[j]=atoi(token);
    }
  }
}

void cookie_info(fortune_cookie *f, int num){
  printf("message: %s", f[num].message);
  int i;
  printf("Lucky numbers are:\n");
  for(i = 0; i < 6; i++){
    printf("%d\n", f[num].lucky_nums[i]);
  }
}

int main(int argc, char **argv){
  fortune_cookie f1[2];
  fill_cookies(f1);
  cookie_info(f1,1);
}
