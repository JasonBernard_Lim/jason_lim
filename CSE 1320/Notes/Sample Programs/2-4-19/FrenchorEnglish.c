#include <stdio.h>
#include <string.h>
#define SIZE_WORDS 4
#define SIZE_BUFF 40

int which_lang(char *word, char **ptr);
void get_rid_newline(char *input);
int check_lang(char *input, char **lang);

int main(int argc, char **argv){
  char *french_words[] = {"amour", "soleil", "oiseau", "coeur"};
  char **ptr_fr = french_words;

  int loop = 1;
  char input[SIZE_BUFF];

  while(loop){
    printf("Enter a sentence:");
    fgets(input, SIZE_BUFF, stdin);

    sttok(input, "\n");

    loop = check_lang(input, ptr_fr);
  }
}

int which_lang(char *word, char **ptr){
  int i ;
  int check = 0;

  for(i = 0; i < SIZE_WORDS && check == 0; i++){
    if(strcmp(*ptr, word) == 0){
      check = 1;
    }

    ptr++;
  }

  return check;
}

int check_lang(char *input, char **lang){
  int check = 0;

  char *token = strtok(input, " ");

  while(token != NULL && check == 0){
      check = which_lang(token, lang);
      token = strtok(NULL, " ");
  }

  if(check == 0){
    printf("English sentence!");
  }

  else{
    printf("French sentence!");
  }
}
