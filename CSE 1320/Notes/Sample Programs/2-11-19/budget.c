//Still need to finish copying 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

float total_spent(FILE *fp){
  float total = 0;
  char line[100];
  char *token;

  while(fgets(line, 100, fp) != NULL) {
    token = strtok(line, ",");
    total = total + atof(token);
  }

  return total;
}

void most_spent(FILE *fp) {
  float hold = 0, current = 0;
  char line[100];
  char *token;

  while(fgets(line,100,fp) != NULL) {
    token = strtok(line, ",");
    current = atof(token);

    if(hold < current) {
      hold = current;
    }

  }

  printf("Most spent in a day: $%.2f", hold);
}

int main(int argc, char **argv){
  FILE *fp;
  float total = 0, avg = 0;
  char answer[20];
  float spent;
  int i = 0;

  fp = fopen(argv[1],"w+");

  char *days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
  char **days_ptr = days;

  int num_days = sizeof(days)/sizeof(days[0]);

  while(i < num_days) {
    printf("Money spent on %s:\n$", days[i]);
    scanf("&f", &spent);
    fprintf(fp, "%.2f, %s\n", spent, days[i]);
    i++;
  }

  fseek(fp, 0, SEEK_SET);

  total = total_spent(fp);
  printf("\n Weekly Spending:");
  printf("Total spent: $%.2f\n", total);
  printf("Daily avg: $%.2f\n", total/7);

  fseek
}
