#include <stdio.h>
#include <string.h>

void print_song(FILE *fp) {
  printf("\nSong currently:\n");
  char line[100];

  while(fgets(line,100,fp) != NULL) {
    printf("%s", line);
  }
}

void add(FILE *fp){
  char line[100];
  printf("Add to the song: ");
  fgets(line, 100, stdin);

  strtok(line, "\n");
  printf("Adding: %s\n", line);

  fprintf(fp, "%s", line);
}

int main(int argc, char **argv){
  FILE *fp;
  char *mode = "a+";
  char line[100];

  printf("Enter song name:");
  scanf("%s", line);
  strcat(line, ".txt");


  fp = fopen(line,mode);

  if(fp==NULL) {
    printf("No file opened.\n");
  }
  else {
    fseek(fp,0,SEEK_SET);

    print_song(fp);

    printf("\nDo you want to add to this song? y or no\n");
    scanf("%s", line);

    if(strcmp(line, "y") == 1){
      add(fp);
    }

    fseek(fp, 0, SEEK_SET);

    print_song(fp);
    printf("Exiting program!\n\n");
    fclose(fp);
  }
}
