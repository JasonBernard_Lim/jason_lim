#include	<stdio.h>
#include	<string.h>
#include	<stdlib.h>

int	winter_coming(FILE	*fp){
  int	current,	previous=0;
  char	line[100];
  char	*token;
  while	(	!feof(fp)	&&current!=-1){
    fgets(line,100,fp);
    token=strtok(line,	",");
    current=atoi(token);

    if(previous<current){
      previous=current;
    }
    else{
      current=-1;
    }
  }
  return	current;
}

int	main(int	argc, char	**argv) {
  FILE	*fp;
  char	*mode="r";
  int	n;

  fp=fopen(argv[1],	mode);

  if(fp==NULL) {
    printf("No	file	found.\n");
  }
  else{
    n=winter_coming(fp);

    if(n==-1) {
      printf("\n***\nWinter	is	NOT	coming	(according	to	this	squirrel).***\n\n");
    }
    else {
      printf("\n***Winter	is	coming	(according	to	this	squirrel).***\n\n");
    }
    fclose(fp);
  }
}
