//this example uses the command line arguments (argv) (Ingredients)
#include	<stdio.h>
#include	<string.h>

void	print_ingredients(FILE	*fp);

int	main	(int	argc,	char	**argv) {
  FILE	*fp; /*create	a	file	pointer*/
  char	*mode="r";
  fp=fopen(argv[1],	mode); /*open	a	file-the	file	pointer	now	points	at	it*/
  /*the	function	fopen	will	return	NULL	if	the	file	did	not	successfully	open*/
  if(fp==NULL){
    printf("No	file	found.\n");
  }
  else{
    print_ingredients(fp);
    fclose(fp); /*close	the	connection	to	the	file*/
  }
}

void	print_ingredients(FILE	*fp){
  char	line[100];
  int	check=1;
  printf("\n***\n");
  /*notice	we	are	reading	from	the	file	using	the	filepointer	in	fgets.		Loop	keeps	going	until fgets
  returns	NULL	OR	check	doesn’t	equal	1 */
  while	(fgets(line,	100,	fp)	!=	NULL	&&	check==1){
    if(strcmp(line,	"Ingredients:\n")==0)	/*found	ingredients*/{
      while	(strcmp(line,	"Recipe:\n")!=0) {
        printf("%s",	line);
        fgets(line,	100,	fp);
      }
      check=0;
    }
  }
}
