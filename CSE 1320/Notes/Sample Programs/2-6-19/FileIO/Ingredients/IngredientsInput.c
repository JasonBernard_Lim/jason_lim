//this example allows the user to type in the file name (Ingredients)
#include	<stdio.h>
#include	<string.h>
void	print_ingredients(FILE	*fp);

int	main	(int	argc,	char	**argv){
  FILE	*fp;
  char	*mode="r";
  char	filename[50];
  printf("Enter	file	name:\n");
  scanf("%s",	filename); /*entering	the	file	name	(program	below	takes	it	from	the	command
    line)*/
    fp=fopen(filename,	mode);
    if(fp==NULL){
        printf("No	file	found.\n");
    }
    else{
				print_ingredients(fp);
        fclose(fp); /*close	the	connection	to	the	file*/
    }
}

void	print_ingredients(FILE	*fp){
  char	line[100];
  int	check=1;
  printf("\n***\n");
  while	(fgets(line,	100,	fp)	!=	NULL	&&	check==1){
    if(strcmp(line,	"Ingredients:\n")==0)	/*found	Ingredients:-add	new	line	because	of fgets*/{
      while	(strcmp(line,	"Recipe:\n")!=0)	/*keep	printing	until	find	Recipe:*/{
      printf("%s",	line);
      fgets(line,	100,	fp);
      }
    check=0;
    }
  }
}
