//Jason Bernard "JB" Lim
// ID: 1001640993
#include <stdio.h>

void enterPatientAges(int ages[], int size);
void compNumOfPatients(int a[], int b[], int size1, int size2);

int main (int argc, char **argv)
{
	int patientsInHospOne;
	int patientsInHospTwo;

	printf("\n*****************************************************\n");
	printf("Number of patients in hospital 1: ");
	scanf("%d", &patientsInHospOne);

	int HospOneInfo[patientsInHospOne];

	enterPatientAges(HospOneInfo, patientsInHospOne);

	printf("\n");

	printf("Number of patients in hospital 2: ");
	scanf("%d", &patientsInHospTwo);

	int HospTwoInfo[patientsInHospTwo];

	enterPatientAges(HospTwoInfo, patientsInHospTwo);

	printf("\n");

	compNumOfPatients(HospOneInfo, HospTwoInfo, patientsInHospOne, patientsInHospTwo);
}

void enterPatientAges(int ages[], int size)
{
	int i;

	for(i = 0; i<size;i++)
	{
		printf("Enter age of patient %d: ", (i+1));
		scanf("%d", &ages[i]);
	}
}

void compNumOfPatients(int a[], int b[], int size1, int size2)
{
	int i;
	int j;
	int counter1=0;
	int counter2=0;

	/*check hospital 1 info*/
	for(i=0; i < size1; i++) {
		if (a[i] >= 65){
			counter1++;
		}
	}

	/*check hospital 2 info*/
	for(j=0; j<size2; j++) {
		if (b[j] >=65) {
			counter2++;
		}
	}


	if (counter1>counter2) /*hosp1 greater than hosp2*/
	{
		printf("\nYou should pick hospital 1-they have more Medicare patients.\n\n");
	}
	else if (counter2>counter1) /*hosp2 greater than hosp1*/
	{
		printf("\nYou should pick hospital 2-they have more Medicare patients.\n\n");
	}
	else /*equal*/
	{
		printf("\nThey have the same number of Medicare patients-go for either.\n\n");
	}
}
