#include "gen8_files.h"
#include "hw8.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  //create the arrays for the trees
  char *arr[] = {"start", "t", "e", "m", "n", "a", "i", "o", "g", "k", "d", "w",
                "r", "u", "s", "~", "~", "q", "z", "y", "c", "x", "b", "j", "p",
                "~", "l", "~", "f", "v", "h"};
  char *arr2[]={"start","T","E","M","N","A","I","O","G","K","D","W","R",
                      "U","S","~","~","S","S","Y","C","X","B","J","P","~","L",
                      "~","F","V","H"};
  //create the trees themselves
  Node* root = NULL;
  Node* root_upper = NULL;
  int n = sizeof(arr)/sizeof(arr[0]);
  root = insertLevelOrder(arr, root, 0, n);
  root_upper = insertLevelOrder(arr2, root_upper, 0, n);

  //use the function to translate the files
  translate(root, root_upper);

  //free the trees
  free_tree(root);
  free_tree(root_upper);

}
