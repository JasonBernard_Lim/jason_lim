#include "gen8_files.h"
#include "hw8.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Node* addNode(char *ptr) {//adds a node to the tree
  Node*	temp = malloc(sizeof(Node));
  temp->word = ptr;
  temp->left = NULL;
  temp->right	=	NULL;

  return	temp;
}

Node* insertLevelOrder(char *arr[], Node *root, int i, int n) {//creates the tree itself
  /* Base case for recursion*/
  if(i < n) {
    Node *temp = addNode(arr[i]);
    root = temp;

    /*insert left child*/
    root->left = insertLevelOrder(arr, root->left, 2 * i + 1, n);

    /*insert right child*/
    root->right = insertLevelOrder(arr, root->right, 2 * i + 2, n);
  }

  return root;
}

char *find_letter(Node *n, char *letter) {//finds the correct letter in the tree
  int num = strlen(letter);
  int i;

  for(i=0;i<num;i++) {
    if(letter[i]=='-') {
      n=n->left;
    }
    else if(letter[i]=='.') {
      n=n->right;
    }
  }
  return n->word;
}
