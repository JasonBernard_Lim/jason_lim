#include "gen8_files.h"
#include "hw8.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void translate(Node* tree, Node *tree_upper) {//this function allows the user to translate morse code sentences
                                              //and save them into a file
  FILE *fp; //create the pointers needed
  char *letter, *token, *temp;
  char *input = malloc(30);
  char *line = malloc(1000);
  while(1) {
    char sentence[50][250];
    memset(sentence, 0, sizeof(sentence));
    printf("What file would you like to translate: "); //ask the user which file to translate
    scanf("%s",input);

    fp=fopen(input,"r");

    if(fp==NULL) {//check if the file exists
      printf("No file found. Returning...\n\n");
    }
    else {
      printf("---Reading in...\n---Translating...\n");
      int i=0, j=0;
      fgets(line, 1000, fp);
      token = strtok(line," ");
      letter = find_letter(tree_upper,token);//find the first letter of the sentence
      sentence[i][j]=letter[0];
      j++;
      while(token != NULL) {//until you are finished translating
        token = strtok(NULL, " \n");

        if(strcmp(token, "") == 0) {
          i++;
          sentence[i][j]=' ';
          token = strtok(NULL," ");
          i++;
          j=0;
          letter = find_letter(tree,token);
          sentence[i][j]=letter[0];
          j++;
        }

        else if(strstr(token,"#") != NULL) {//if this is the end of the sentence
          strncpy(temp, token, strlen(token)-1);
          letter = find_letter(tree,temp);
          sentence[i][j]=letter[0];
          j++;
          sentence[i][j]='.';//add a period at the end
          i++;
          j=0;

          token = strtok(NULL, " \n");
          if(token!=NULL) {//this gets the next letter of the next sentence
            letter = find_letter(tree_upper,token);
            sentence[i][j]=letter[0];
            j++;
          }
        }

        else {//if this is just a letter inside the sentence other than the beginning and the end
          letter = find_letter(tree,token);
          sentence[i][j]=letter[0];
          j++;
        }
      }
      file_print(sentence,i-1);//print out the sentence to the output file
    }
    printf("Would you like to read another file in? y or n\n");//ask the user if he wants to quit or translate another file
    scanf("%s",input);
    if(input[0] == 'n'){
       printf("Exiting...\n\n");
       free(input);
       free(line);
       fclose(fp);
       return;
    }
  }
}

void file_print(char sentence[50][250], int words) {//this function prints the sentence
  char *line = malloc(50);
  printf("What file to save to?\n");//open the output file
  scanf("%s",line);
  FILE *fp = fopen(line, "w+");
  int i;

  for(i=0; i <= words; i++) {//print out every sentence
    fprintf(fp,"%s",sentence[i]);
    fprintf(fp," ");
  }
  fprintf(fp, "\n");
  free(line);
  fclose(fp);
}

void free_tree(Node *tree) {//this function frees the tree used
  if(tree == NULL) {
    return;
  }

  free_tree(tree->left);
  free_tree(tree->right);
  free(tree);
}
