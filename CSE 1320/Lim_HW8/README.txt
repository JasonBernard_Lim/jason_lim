// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files included: hw8.h, hw8.c, gen8_files.h, gen8_files.c, hw8main.c
                sample files in the sample file folder: sample1.txt, sample2.txt
                sample3.txt, sample4.txt
-------------------
Function Descriptions
  void translate allows the user to input files of morse code and translates it.
  The user is then to output the translation to a file. The user then can decide
  if he/she would want to do another and can continue to do so until they say no.

  void file_print is inside of void translate and fprintf's the sentence to the
  output file.

  void free_tree free's the tree used for the translate function.

-------------------
Compilation Instructions
  - With the main (need to make one for this program)

  gcc -g hw8.c gen8_files.c hw8main.c
  a.out
-------------------
Sample run:
[jel0993@omega HW8]$ a.out
What file would you like to translate: sample3.txt
---Reading in...
---Translating...
Segmentation fault
[jel0993@omega HW8]$ a.out
What file would you like to translate: sample2.txt
---Reading in...
---Translating...
What file to save to?
output2.txt
Would you like to read another file in? y or n
y
What file would you like to translate: sample1.txt
---Reading in...
---Translating...
What file to save to?
output.txt
Would you like to read another file in? y or n
n
Exiting...

[jel0993@omega HW8]$ cat output2.txt
Willigetagoodgrade. Diditwork. Ihopethisworks.
[jel0993@omega HW8]$ cat output.txt
Thefunctionworked. Goodjob.

-------------------
Notes:
  Please make sure that whichever other files you wish to translate in the folder
  with all the functions.

  Some files may be too long for the function and may cause a segmentation fault.
