#ifndef GEN_8
#define GEN_8

struct node {
  char* word;
  struct node *left;
  struct node *right;
};
typedef struct node Node;

Node* addNode(char *ptr);
Node* insertLevelOrder(char *arr[], Node *root, int i, int n);
char* find_letter(Node *n, char *letter);


#endif
