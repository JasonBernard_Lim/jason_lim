#include "stack_queue7.h"
#include "hw7.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char *filename = argv[1];
  QueueInfo *q = passengers_info(filename);

  char line[20];
  int flight;
  char *token;

  printf("\n\nWhich flight number to check?: ");
  fgets(line,20,stdin);

  token = strtok(line, "\n");
  flight = atoi(token);

  check_flight(q, flight);
}
