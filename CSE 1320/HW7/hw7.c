#include "stack_queue7.h"
#include "hw7.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/****************Stack Array Implementation ************************/

//Problem 1, Function 1
void enter_tip(Stack *s){//this function allows the user to enter in tips up to full capacity
  float tip = -1;
  int check = 1;
  printf("\n\n***Tip Jar***\n");
  char line[20];
  char *token;

  while(check){
    printf("Enter tip: $");
    fgets(line,20,stdin);

    if(strcmp(line, "\n") == 0) {//this checks if the user just types in enter
      printf("Invalid input!\n");
      break;
    }

    token = strtok(line, "\n");
    if(strcmp(token, "done") == 0){//checks if the user typed in done
      return;
    }
    else {
      tip = atof(token);
      if(tip == 0){//checks if the tip entered had the amount of $0.00
        printf("No money added!\n");
      }
      else {
        push(s,tip);//push the tip into the jar/stack
        if(s->top == (20-1)) {//checks if the jar is full
          printf("Tip jar is full!\n");
          check = 0;
        }
      }
    }
  }
  return;
}


//Problem 1, Function 2
float calculate_pay(Stack *s){//this function calculates the pay for the user
  float tip;                  //and takes the money out of the jar
  float hours;

  if(s->top == -1){//checks if the jar is empty
    printf("Tip jar is empty!\n");
    return 0;
  }

  printf("Hello, how many hours did you work? ");
  scanf("%f", &hours); //ask how many hours

  float tip_needed = 2 * hours; //calculate the tip needed

  while(tip != tip_needed) {//while the actual tip does not equal the one needed

    if(s->stk[s->top] <= 0){//this checks if there is not enough money
      printf("\nTip jar has not enough cash or is empty!\nGiving you the rest of the jar.\n\n");
      break;
    }

    //thic checks if the tip in the jar is larger than the tip needed
    else if(s->stk[s->top] > tip_needed || s->stk[s->top] > (tip_needed - tip)){
      s->stk[s->top] = s->stk[s->top] - (tip_needed - tip);
      tip = tip + (tip_needed - tip);
    }

    //this checks if the tip in the jar is the exact amount needed
    else if(s->stk[s->top] == tip_needed || s->stk[s->top] == (tip_needed - tip)){
      tip = tip + pop(s);
    }

    //if none of the conditions are met just take the money out of the jar
    else{
      tip = tip + pop(s);
    }

  }

  //print the amount given to the user
  printf("OK, giving you $%0.2f\n", tip);
  //print the updated jar
  updated_display(s);
  return(tip);
}


/****************Queue Linked List Implementation ************************/

//Prompt 2, Function 3
QueueInfo *passengers_info(char *filename) {//this function creates the passenger queue
                                            //it takes in the file name of the info
  QueueInfo *info = createQueueInfo();

  FILE *fp = fopen(filename, "r");
  char *token;
  char *line = (char *)malloc(100);
  char *full_name;
  int flight;

  if(fp == NULL){//check if the wrong file or no file was put in
    printf("\n\nNo file openend...\nExiting...\n\n");
    exit(0);
  }
  else{//add the customer info in line by line
    printf("--Adding in customer info from file:\n");
    while(fgets(line, 100, fp) != NULL) {
      token = strtok(line, ",");
      strcpy(full_name, token);//add the first name to the string and add a space at the end
      strcat(full_name, " ");

      token = strtok(NULL, ",");//add the last name to the string
      strcat(full_name, token);

      token = strtok(NULL, "\n");//get the flight number
      flight = atoi(token);

      printf("%s - %d\n", full_name, flight);//print out the info to the user
      enqueue(info, full_name, flight);//add the info to the queue
    }
  }
  printf("\nAll customer info added.");
  free(line);
  fclose(fp);//print that the function has finished and close the file and return the queue
  return info;
}

//Prompt 2, Function 4
void check_flight(QueueInfo *info, int flight) {//this function checks which passenger is on
  FILE *fp=fopen("output.txt","w+");//create an output file
  printf("--Passengers for %d:", flight);
  fprintf(fp,"--Passengers for %d:", flight);
  while(info->head != NULL) {
    if(info->head->flight == flight) {//find the passenger(s) for the flight
      printf("\n%s is taking flight %d.", info->head->full_name, info->head->flight);
      fprintf(fp,"\n%s is taking flight %d.", info->head->full_name, info->head->flight);
    }
    info->head = info->head->next;//move to the next node
  }
  printf("\n");
  fprintf(fp,"\n");
}
