#include "stack_queue7.h"
#include "hw7.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char choice[10];
  Stack *s = malloc(sizeof(Stack));
  int check = 1;
  while(check) {
    printf("\nWhat do you want to do?\n");
    printf("1. Add tips\n");
    printf("2. Clock out\n");
    fgets(choice, 10, stdin);
    strtok(choice, "\n");
    if(strcmp(choice, "a") == 0) {
      enter_tip(s);
    }
    else if(strcmp(choice, "b") == 0) {
      calculate_pay(s);
    }
    else if(strcmp(choice, "c") == 0) {
      printf("Bye...\n\n");
      check = 0;
    }
  }
}
