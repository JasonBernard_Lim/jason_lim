//These are all the function definitions for the header file
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "house_b.h"

/*DO NOT MODIFY STRUCT*/
struct node {
  int price;
  char *city;
  char *address;
  Node *next;

};

struct node *new_house(Node *h) {//this function adds a new house
                                 //the one parameter is the head of the linked list
                                 // it returns a new head of the linked list
  char *line = (char *)malloc(20);

  printf("\n***Adding a new house:***\n");

  //create the node for the new house
  struct node *new_house = (Node *)malloc(sizeof(struct node));

  //add the address of the new house
  new_house->address = (char *)malloc(20);
  printf("Enter address: ");
  fgets(line,20,stdin);
  strtok(line, "\n");

  strcpy(new_house->address, line);

  //add the city of the new house
  new_house->city = (char *)malloc(20);
  printf("Enter city: ");
  fgets(line, 20, stdin);
  strtok(line, "\n");

  strcpy(new_house->city, line);

  //add the pice of the new house
  printf("Enter price: $");
  fgets(line, 20, stdin);
  strtok(line, "\n");
  new_house->price = atoi(line);

  //actually add the new house for the linked list
  new_house->next = h;

  printf("New house added!\n\n");

  //free line so no memory leak will happen
  free(line);
  return new_house;
}

Node *read_in(int n) {//this functions creates the linked list
                      //the parameter is the size of the for loop
                      //it returns the head of the linked list
  FILE *fp = fopen("housestuff1.txt", "r");

  char *token;
  char *line = (char *)malloc(100);

  Node *head = NULL;
  Node **cur = &head;
  struct node *var;

  int i = 0;

  if(fp == NULL){//check to see if the file was read correctly
    printf("\n\nFile not opened.....\nExiting...\n\n");
    exit(0);
  }
  else {

    for (i = 0; i < n; i++) {//for loop to create the linked list
      fgets(line, 100, fp);

      token = strtok(line, ",");

      //add the address
      var = (Node *)malloc(sizeof(Node));
      var->address = (char *)malloc(20);
      strcpy(var->address,token);

      //add the city
  		token=strtok(NULL, ",");
      var->city = (char *)malloc(20);
      strcpy(var->city, token);

      //add the price
      token=strtok(NULL,",\n");
      var->price = atoi(token);

      //add the temp node to the linked list and set cur to the next node
      *cur = var;
      cur = &var->next;
    }
  }
  //terminate the list
  *cur = NULL;
  //free the temp variable and close the file
  free(line);
  fclose(fp);
  return head;
}

struct node *delete_house(struct node *h, char *addy) {//this function deletes one house
                                                       //the parameters are the head of the linked list and an Address
                                                       //it returns the new head of the linked list
  struct node *p = h;
  int check = 0;

  //handle empty list
  if (p == NULL) {
    return p; //return the original head
  }

  struct node* q = p->next;

  //find the house to delete
  if (strcmp(p->address, addy) == 0) {
    printf("House deleted!\n");
    check = 1;
    p = p->next;
    free(h);
    return p;
  }

  while (q != NULL) {//if the first node is not the one to delete
    if (strcmp(addy, q->address) == 0) /*found node to delete*/
    {
      check = 1;
      p->next = q->next;
      free(q->city);
      free(q->address);
      free(q);
      printf("House deleted!\n");
      break;
    }
    //connect the two nodes if there is a gap in the middle
    p = p->next;
    q = q->next;
  }

  if (check == 0) {
    printf("Address not found!\n ");
  }

  return h;
}

void available(int total, struct node *h) {//this function prints out the correct houses
                                           //the parameters are a budget and the head of the linked list
  int i = 0;
  printf("\n");

  while (h != NULL) {//search through the linked list and print all valid houses
    if (h->price <= total) {
      printf("Price: $%d    %s \n", h->price, h->address);
      i++;
    }
    h = h->next;
  }

  printf("--Houses that match your budget: %d\n", i);
}

void free_nodes(Node *h) {//this function frees the linked list
                          //its parameter is the head of the linked list
  Node *temp;
  temp = h;

  if (h == NULL) {//see if the list has been freed
    exit(0);
  }
  else {//set the head to the next node and free the temp node
    h = h->next;
    free(temp->city);
    free(temp->address);
    free(temp);
    free_nodes(h);//use recursion to simplify the process
  }
}
