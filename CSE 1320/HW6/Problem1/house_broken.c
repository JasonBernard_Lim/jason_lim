// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 1: Debug

//This function simulates a realty store
//The user can add or delete a house and also find a house that matches their budget
//This c file only contains the main as it uses a header file
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "house_b.h"

int main(char arg, char **argv) {

  struct node *list_h = read_in(4);
  char *line = (char *)malloc(20);
  int check = 1, total = 0;

  while (check) {
    printf("\n***Welcome to ABC Realty.***\nUpdate inventory or find house?\n");
    fgets(line, 20, stdin);
    strtok(line, "\n");

    if (strcmp(line, "find") == 0) {
      printf("What is your budget? $");
      fgets(line, 20, stdin);
      strtok(line, "\n");
      total = atoi(line);
      available(total, list_h);
    }

    else if (strcmp(line, "update") == 0) {
      printf("Add house or delete house? ");
      fgets(line, 20, stdin);
      strtok(line, "\n");

      if (strcmp(line, "add") == 0){
        list_h = new_house(list_h);
      }
      else {
        printf("Enter address of house to delete: ");
        fgets(line, 20, stdin);
        strtok(line, "\n");
        list_h = delete_house(list_h, line);
      }

    }

   else if (strcmp(line, "quit") == 0) {
        printf("Exiting...\n");
        free(line);
        free_nodes(list_h);
        check = 0;
      }
   else {
        printf("Invalid entry.\n");
      }

  }
}
