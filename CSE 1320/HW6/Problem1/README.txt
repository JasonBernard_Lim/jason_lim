// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 1: Debug

--------------------------------------README------------------------------------
Files included: house_broken.c, house_b.c, house_b.h
-------------------
- The file with the main is in house_broken.c.

- The other files: house_b.c includes the function definitions that are stored
  in the header file house_b.h.

-------------------
- This program simulates a realtor inventory allowing you add or delete a house
  or find one that matches your budget.
-------------------
  Compilation Instructions:

- Since the program includes multiple .c files to compile it you need to type
  in: gcc -g house_broken.c house_b.c, as you need to compile both for the program
  to run.

- When you use a file for the program make sure to name it housestuff1.txt as it
  is hardcoded in the program. (Also make sure it is in the same directory as
  house_broken.c and all the other files as the program would not function to
  its full capacity and will output an error).

- Full compilation example:
  gcc -g house_broken.c house_b.c
  ./a.out
