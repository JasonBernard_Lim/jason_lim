//this is a header file for house_broken.c
#ifndef HOUSE_B
#define HOUSE_B

typedef struct node Node;

struct node *new_house(Node *h);
Node *read_in(int n);
struct node *delete_house(struct node *h, char *addy);
void available(int total, struct node *h);
void free_nodes(Node *h);

#endif
