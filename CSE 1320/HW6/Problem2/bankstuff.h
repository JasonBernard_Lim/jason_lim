#ifndef BANKSTUFF
#define BANKSTUFF

typedef struct account Account;
Account *read_file(int size, char *filename);
Account *find_account(char *name, Account *head);
void free_node(Account *head);
void transfer_funds(Account *user, Account *transfer);

#endif
