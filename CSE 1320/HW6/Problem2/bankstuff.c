#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "bankstuff.h"

struct account{
  char **user_name;	/*first name, last name*/
	char *bank_name;	/*bank name*/
	int	num_accounts;	/*number of	accounts*/
	int	**account_numbers; /*all 6 digits, can have	multiple accounts-checking,saving*/
	float	**total_in_account;	/*amount in	each account*/
	struct account *next;
};

Account *read_file(int size, char *filename) {
  FILE *fp = fopen(filename, "r");
  char *token;
  char *line = (char *)malloc(100);
  Account *head = NULL;
  Account ** current = &head;
  Account *temp;
  int i;

  if (fp == NULL) {
    printf("\n\nNo file openend...\nExiting...\n\n");
    exit(0);
  }
  else{
    for(i = 0; i < size; i++){
      fgets(line, 100, fp);
      token = strtok(line, " ");

      temp = (Account *)malloc(sizeof(Account));
      temp->user_name = (char **)malloc(sizeof(char *));
      temp->user_name[0] = (char *)malloc(40);
      temp->user_name[1] = (char *)malloc(40);
      strcpy(temp->user_name[0], token);

      token = strtok(NULL, ",");
      strcpy(temp->user_name[1], token);

      token = strtok(NULL, ",");
      temp->bank_name = (char *)malloc(25);
      strcpy(temp->bank_name, token);

      token = strtok(NULL, ",");
      temp->num_accounts = atoi(token);

      temp->account_numbers = (int **)malloc(sizeof(int *));
      temp->account_numbers[0] = (int *)malloc(temp->num_accounts * sizeof(int));
      int j;
      for(j = 0; j < temp->num_accounts; j++){
        token = strtok(NULL, ",");
        temp->account_numbers[0][j] = atoi(token);
      }

      temp->total_in_account = (float **)malloc(sizeof(float *));
      temp->total_in_account[0] = (float *)malloc(temp->num_accounts * sizeof(float));
      int k;
      for(k = 0; k < temp->num_accounts; k++){
        token = strtok(NULL, ",");
        temp->total_in_account[0][k] = atof(token);
      }
      strtok(NULL, "\n");
      *current = temp;
      current = &temp->next;
    }
  }
  *current = NULL;

  free(line);
  fclose(fp);
  return head;
}

Account *find_account(char *name, Account *head) { //this function finds the correct account
  char *token = strtok(name, " ");
  while(head != NULL){
    if(strcmp(head->user_name[0], token) == 0 ){
      token = strtok(NULL, "\n");
      if(strcmp(head->user_name[1], token) == 0){
        return head;
      }
    }
    head = head->next;
  }
  return NULL;
}

void transfer_funds(Account *user, Account *transfer) {//this function transfer funds from one account to another
                                     //the parameter is the person transfering and the person getting transfered to
  char *line = (char*)malloc(20);
  float transfer_amount = 0;

  printf("\n\nHow much money would you like to transfer? $");
  fgets(line, 20, stdin);
  strtok(line, "\n");

  transfer_amount = atof(line);

  if(user->num_accounts == 2) {
    printf("From which account? ");
    fgets(line,20,stdin);
    strtok(line, "\n");

    if(strcmp(line, "checking") == 0) {
      if(transfer_amount >= user->total_in_account[0][0] || user->total_in_account[0][0] - transfer_amount < 5){
        printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
      }
      else{
        if(strcmp(user->bank_name, transfer->bank_name) != 0){
          printf("\n\nRecipient is in a different bank. You will be charged a %5 transaction fee. Continue? y or n\n");
          fgets(line,20,stdin);
          strtok(line, "\n");
          if(strcmp(line, "y") == 0){
            if((transfer_amount *1.05) >= user->total_in_account[0][0] || user->total_in_account[0][0] - (transfer_amount * 1.05) < 5){
              printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
            }
            else {
              printf("\nBefore transfer: $%0.2f", user->total_in_account[0][0]);
              user->total_in_account[0][0] = user->total_in_account[0][0] - (transfer_amount * 1.05);
              transfer->total_in_account[0][0] = transfer->total_in_account[0][0] + (transfer_amount * 1.05);
              printf("\nAfter transfer: $%0.2f", user->total_in_account[0][0]);
              printf("\nTransfer successfully completed.");
            }
          }
          else {
            printf("\n\nReturning to the main menu.");
          }
        }
        else {
            printf("\nBefore transfer: $%0.2f", user->total_in_account[0][0]);
          user->total_in_account[0][0] = user->total_in_account[0][0] - transfer_amount;
          transfer->total_in_account[0][0] = transfer->total_in_account[0][0] + transfer_amount;
          printf("\nAfter transfer: $%0.2f", user->total_in_account[0][0]);
          printf("\nTransfer successfully completed.");
        }
      }
    }
    else if(strcmp(line, "savings") == 0) {
      if(transfer_amount >= user->total_in_account[0][1] || user->total_in_account[0][1] - transfer_amount < 5){
        printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
      }
      else{
        if(strcmp(user->bank_name, transfer->bank_name) != 0){
          printf("\n\nRecipient is in a different bank. You will be charged a %5 transaction fee. Continue? y or n\n");
          fgets(line,20,stdin);
          strtok(line, "\n");
          if(strcmp(line, "y") == 0){
            if((transfer_amount *1.05) >= user->total_in_account[0][1] || user->total_in_account[0][1] - (transfer_amount * 1.05) < 5){
              printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
            }
            else {
              printf("\nBefore transfer: $%0.2f", user->total_in_account[0][1]);
              user->total_in_account[0][1] = user->total_in_account[0][1] - (transfer_amount * 1.05);
              transfer->total_in_account[0][1] = transfer->total_in_account[0][1] + (transfer_amount * 1.05);
              printf("\nAfter transfer: $%0.2f", user->total_in_account[0][1]);
              printf("\nTransfer successfully completed.");
            }
          }
          else {
            printf("\n\nReturning to the main menu.");
          }
        }
        else {
            printf("\nBefore transfer: $%0.2f", user->total_in_account[0][1]);
          user->total_in_account[0][1] = user->total_in_account[0][1] - transfer_amount;
          transfer->total_in_account[0][1] = transfer->total_in_account[0][1] + transfer_amount;
          printf("\nAfter transfer: $%0.2f", user->total_in_account[0][1]);
          printf("\nTransfer successfully completed.");
        }
      }
    }
    free(line);
  }
  else{
    if(transfer_amount >= user->total_in_account[0][0] || user->total_in_account[0][0] - transfer_amount < 5){
      printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
    }
    else{
      if(strcmp(user->bank_name, transfer->bank_name) != 0){
        printf("\n\nRecipient is in a different bank. You will be charged a %5 transaction fee. Continue? y or n\n");
        fgets(line,20,stdin);
        strtok(line, "\n");
        if(strcmp(line, "y") == 0){
          if((transfer_amount *1.05) >= user->total_in_account[0][0] || user->total_in_account[0][0] - (transfer_amount * 1.05) < 5){
            printf("\n\nSorry, you do not have sufficient funds to make this transfer. Ending session...\n");
          }
          else {
            printf("\nBefore transfer: $%0.2f", user->total_in_account[0][0]);
            user->total_in_account[0][0] = user->total_in_account[0][0] - (transfer_amount * 1.05);
            transfer->total_in_account[0][0] = transfer->total_in_account[0][0] + (transfer_amount * 1.05);
            printf("\nAfter transfer: $%0.2f", user->total_in_account[0][0]);
            printf("\nTransfer successfully completed.");
          }
        }
        else {
          printf("\n\nReturning to the main menu.");
        }
      }
      else {
          printf("\nBefore transfer: $%0.2f", user->total_in_account[0][0]);
        user->total_in_account[0][0] = user->total_in_account[0][0] - transfer_amount;
        transfer->total_in_account[0][0] = transfer->total_in_account[0][0] + transfer_amount;
        printf("\nAfter transfer: $%0.2f", user->total_in_account[0][0]);
        printf("\nTransfer successfully completed.");
      }
    }
    free(line);
  }

}

void free_node(Account *head) {//this function frees the linked list
                               // parameter is the head of the linked list
  Account *temp;
  while (head != NULL) {
    temp = head;
    head = head->next;
    free(temp->user_name[0]);
    free(temp->user_name[1]);
    free(temp->user_name);
    free(temp->bank_name);
    free(temp->account_numbers[0]);
    free(temp->account_numbers);
    free(temp->total_in_account[0]);
    free(temp->total_in_account);
    free(temp);
  }
}
