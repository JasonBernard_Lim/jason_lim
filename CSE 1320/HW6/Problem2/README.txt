// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Write a program

--------------------------------------README------------------------------------
Files included: bankmain.c, bankstuff.c, bankstuff.h
-------------------
- The file with the main is in bankmain.c

- The other files: bankstuff.c includes the function definitions that are stored
  in the header file bankstuff.h.

-------------------
- This program allows the user to transfer money in real time. Every time the user
  transfers money, the new value is reflected in the account.
-------------------
  Compilation Instructions:

- Since the program includes multiple .c files to compile it you need to type
  in: gcc -g bankmain.c bankstuff.c, as you need to compile both for the program
  to run.

- When you type in your arguments to run the program make sure the second argument
  which is the number of accounts match the same number of lines as the file which
  holds the info as any more or any less than the number of lines in the file will
  cause to program to seg fault or not run to its full potential.

- Full compilation example:
  gcc -g bankmain.c bankstuff.c
  ./a.out 7 bankaccounts.txt
