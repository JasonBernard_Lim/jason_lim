#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "bankstuff.h"

int main(int argc, char **argv){
  if(argc < 3){
    printf("\n\nNot enough arguments.\nExiting....\n\n");
    exit(0);
  }
  int size = atoi(argv[1]);
  char *filename = argv[2];

  char *line = (char *)malloc(20);
  int check = 1;

  Account *head = read_file(size, filename);
  Account *user;
  Account *transfer;

  if(head == NULL){
    printf("\n\nIncorrect Arguments...\nExiting...\n\n");
    exit(0);
  }
  else{
    while(check) {
      printf("\n\n******************************************************************");
      printf("\n***Welcome to Money Transfer-Voted the Best 10 Years in a Row!***");
      printf("\n******************************************************************");

      printf("\nEnter your first and last name: ");
      fgets(line,20,stdin);

      if(strcmp(line, "exit program\n") == 0){
        printf("Exiting...\n\n");
        free(line);
        free_node(head);
        exit(0);
      }

      user = find_account(line, head);

      if(user == NULL){
        printf("\n\nUser not found returning to main menu....\n\n");
      }
      else{
        printf("\nWho do you want to transfer money to? ");
        fgets(line,20,stdin);
        transfer = find_account(line, head);

        if(transfer == NULL){
          printf("\n\nUser not found returning to main menu....\n\n");
        }
        else{
          transfer_funds(user, transfer);
        }
      }
    }
  }
}
