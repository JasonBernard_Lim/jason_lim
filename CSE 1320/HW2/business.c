//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 3: Write a Program

#include <stdio.h>
#include <stdlib.h>

float print_out(float* salary, int* num_of_emps);
int rebudget(float *d, float *budget);

int main(int argc, char **argv){
  int i = 1;
  float budget, total, salary;
  float *money_ptr = &salary;
  float *total_ptr = &total;
  int employees;

  printf("Enter monthly budget: $");
  scanf("%f", &budget);

  while(i){
    printf("Enter monthly worker salary: $");
    scanf("%f", &salary);

    printf("Enter total workers: ");
    scanf("%d", &employees);

    total = print_out(money_ptr, &employees);
    i = rebudget(total_ptr, &budget);
  }
}

float print_out(float* salary, int* num_of_emps){
  /*No fewer than 6 workers are allowed and for every 2 workers there
    is a manager. If the number of workers is odd than it is 3 workers per manager.
    For every 3 managers there is a director, the max number of managers that can
    be handled by a director is 5. If there are less than 3 managers there is
    no director.*/
    int managers;
    int directors;

    if(*num_of_emps < 6){
      printf("There are not enough workers to construct a business.\n\n");
      exit(0);
    }
    else{
      if((*num_of_emps / 2) == 1){//odd
        managers = *num_of_emps / 3;
        if(managers < 3){
          directors = 0;
        }
        else{
          directors = managers / 3;
        }
      }
      else{//even
        managers = *num_of_emps / 2;
        if(managers < 3){
          directors = 0;
        }
        else{
          directors = managers / 3;
        }
      }
    }

    /*A director makes 5 times the amount a worker makes and a manager makes half
      of what a director makes.*/
    float directorSalary = 5 * *salary;
    float managerSalary = directorSalary / 2;
    float totalSpent = (*num_of_emps * *salary) + (directors * directorSalary) + (managers * managerSalary);

    printf("\n\n**Employee info:**\n");
    printf("Total workers: %d  Monthly salary: $%0.2f", *num_of_emps, *salary);
    printf("\nTotal managers: %d  Monthly salary: $%0.2f", managers, managerSalary);
    printf("\nTotal directors: %d  Monthly salary: $%0.2f", directors, directorSalary);
    printf("\nTotal spent: $%0.2f", totalSpent);

    return totalSpent;
}

int rebudget(float *d, float *budget){
  float remainder = *budget - *d;
  int check;
  if (remainder > 0){
    printf("\n--This goes UNDER your budget by $%0.2f", remainder);
    printf("\n\n-------\nWould you like to rebudget? 1 for yes 2 for no.\n");
    scanf("%d", &check);
    if(check == 1){
      return 1;
    }
    else{
      printf("Bye!\n\n");
      return 0;
    }
  }
  else if(remainder < 0){
    remainder = remainder * -1; //to remove the negative sign as abs does not work on float types
    printf("\n--This goes OVER your budget by $%0.2f", remainder);
    printf("\n\n-------\nWould you like to rebudget? 1 for yes 2 for no.\n");
    scanf("%d", &check);
    if(check == 1){
      return 1;
    }
    else{
      printf("Bye!\n\n");
      return 0;
    }
  }
  else{
    printf("\n--This equals your budget.");
    printf("\n\n-------\nWould you like to rebudget? 1 for yes 2 for no.\n");
    scanf("%d", &check);
    if(check == 1){
      return 1;
    }
    else{
      printf("Bye!\n\n");
      return 0;
    }
  }
}
