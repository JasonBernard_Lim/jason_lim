//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code
//    The hotel got a shipment of 50 new beach umbrellas.

#include <stdio.h>

int main(int argc, char **argv) {
  int umbrellaStock = 60;
  int shipmentAmt = 0;

  printf("\n***********************************************************\n");
  printf("Hello! The current stock of umbrellas is: %d", umbrellaStock);
  printf("\nHow many umbrellas were shipped in today?: ");
  scanf("%d", &shipmentAmt);

  umbrellaStock = shipmentAmt + umbrellaStock;

  printf("The new total stock of the umbrellas is: %d",umbrellaStock);
  printf("\n\n");
}
