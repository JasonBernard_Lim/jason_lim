//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code
//    I lost half of my money in the stock market last year.

#include <stdio.h>
#include <math.h>

void checkMoney(int money1, int money2, int diffMoney, int time);

int main(int argc, char **argv){
  int startingMoney;
  int currentMoney;
  int timeSpent;

  printf("\n***********************************************\n");
  //ask for the inputs from the user
  printf("How much money did you start with?: ");
  scanf("%d", &startingMoney);

  printf("\nHow much money do you have currently?: ");
  scanf("%d", &currentMoney);

  printf("\nHow long have you spent in the stock market?: ");
  scanf("%d", &timeSpent);

  //calculate the difference in money
  int diffMoney = abs(startingMoney - currentMoney);

  checkMoney(startingMoney, currentMoney, diffMoney, timeSpent);
}

void checkMoney(int money1, int money2, int diffMoney, int time) {
  if(money1 > money2) {//lost money
    printf("\nOver %d in the stock market, you have lost $%d.\n\n", time, diffMoney);
  }
  else if (money1 < money2) {//gained money
      printf("\nOver %d in the stock market, you have gained $%d.\n\n", time, diffMoney);
  }
  else {//money equal
    printf("\nOver %d in the stock market, your money has remained the same.\n\n", time);
  }
}
