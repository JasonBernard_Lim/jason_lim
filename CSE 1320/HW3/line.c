//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 3: Write a Program

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*This function takes	a	char** pointer and integer.
    It returns a -1	if the word	filename is	not	found
    in the char** pointer and any other value	otherwise.*/
int find_filename(int n, char **b) {
  int i;
  int counter = 0; /*equals -1 if didn't find */
  int check = 0;

  for(i = 0; i < n && check == 0; i++) {
    if(strcmp(*b, "filename") == 0) {
      check = 1;
    }
    counter++;
    b++;
  }

  if(check != 1) {
    counter = 0;
  }
  return(counter-1);
}

void print_out(FILE *fp) {
  printf("Contents of the file:\n");

  char line[100];

  while(fgets(line, 100, fp) != NULL) {
    printf("%s", line);
  }
  printf("\n");
}

int main(int argc, char **argv) {
  int check = find_filename(argc, argv);

  if(check == -1) {//filename not found
    printf("No filename given. Bye!\n\n");
    exit(0);
  }
  else {//filename found so now searching for the _______.txt
    FILE *fp;
    char *mode = "r";
    fp = fopen(argv[check+1], mode);

    if(fp == NULL) {//could not open file
      printf("The file does not exist.\n\n");
    }
    else{//file found and opened now checking info type and printing out the contents
      printf("Filename: %s\n", argv[check+1]);
      if(argc < 4){
        printf("File info type not given.\n\n");
        fclose(fp);
      }
      else if(strcmp(argv[check+2], "string")==0){//strings
        printf("We're dealing with string info.\n\n");
        print_out(fp);
        fclose(fp);
      }
      else if(strcmp(argv[check+2], "numbers") == 0) {
        printf("We're dealing with string info.\n\n");
        print_out(fp);
        fclose(fp);
      }
      else {//info type not given
        printf("File info type not given.\n\n");
        fclose(fp);
      }

    }
  }
}
