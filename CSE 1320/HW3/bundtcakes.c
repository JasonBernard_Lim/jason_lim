
//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 4: Write a Program

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>

void print_out(int *num, char *f[]) {
  int i;
  for(i = 0; i < *num; i++){
    printf("%s",f[i]);
    printf("\n");
  }
  printf("\n");
}

int name_flavors(char *n, char *f[], int *s){
  int counter = 0;
  int i;
  for(i = 0; i < *s; i++){
    char letter = *f[0];
    if(*n == letter){
      printf("Flavor Match! %s", *f);
      printf("\n");
      counter++;
    }
    else if(toupper(*n) == letter){
      printf("Flavor Match! %s", *f);
      printf("\n");
      counter++;
    }
    f++;
  }
  return counter;
}

int main(int argc, char ** argv){
  int choice;
  int check = 1;
  char *flavors[] = {"Peanut Butter Chocolate Swirl", "Chocolate Chocolate Chip", "Classic Vanilla", "Red Velvet", "White Chocolate Raspberry", "Confetti", "Carrot", "Lemon", "Marble", "Pecan Praline"};

  while(check){
    printf("\n\n***Menu:***\n");
    printf("1-pick how many you want\n");
    printf("2-see if any match the first letter of your name\n");
    printf("3-to exit\n");
    scanf("%d", &choice);

    if(choice == 1){//printout
      int num;
      printf("\n\nHow many do you want? ");
      scanf("%d", &num);
      int *num_ptr = &num;

      if(num > (sizeof(flavors)/sizeof(flavors[0]))){
        printf("\nNumber entered is greater than amount of flavors in stock!\n\n");
      }
      else{
        printf("%d Flavors:\n", num);
        print_out(num_ptr,flavors);
      }
    }
    else if(choice == 2){//name
      char name [100];
      int length = sizeof(flavors)/sizeof(flavors[0]);
      int *length_ptr = &length;

      printf("\n\nEnter a name:\n");
      scanf("%s", &name);

      char first_letter = name[0];
      char *letter_ptr = &first_letter;

      int matches = name_flavors(letter_ptr, flavors, length_ptr);
      printf("Number of matches: %d", matches);
    }
    else if(choice == 3){//exit
      printf("\nBye\n");
      check = 0;
    }
    else{
      printf("\n\nInvalid Choice.\n\n");
    }
  }
}
