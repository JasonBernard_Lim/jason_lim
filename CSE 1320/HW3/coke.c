//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code
//    Each crate of	Coke holds a tenth of	the	total	number of	Cokes in the	truck.
//    Anything that	doesn’t	go into a crate ends up in the trash.
#include <stdio.h>

int main(int argc, char **argv){
  int cokes;

  printf("\nHello! How many Cokes are we going to ship today?: ");
  scanf("%d", cokes);

  int crates = cokes / 10;
  int trashed = cokes % 10;

  if(trashed > 0) {//some Cokes were trashed
    printf("\nOK! We will be using %d crates. However %d Cokes will be trashed.\n\n", crates, trashed);
  }
  else {
    printf("\nOK! We will be using %d crates.\n\n", crates);
  }

}
