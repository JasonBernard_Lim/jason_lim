//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code
//    After her 3 older sisters evenly split all of Halloween candy, Maria gets the leftovers.
#include <stdio.h>

int main(int argc, char **argv) {
  int candy;

  printf("Hello Maria! How much candy did you get this Halloween?: ");
  scanf("%d", candy);

  int leftovers = candy % 3;

  if(leftovers == 0) {
    printf("\nSorry Maria, you get no candy this year. :(\n\n");
  }
  else {
    print("\nAfter your sisters split the candy, you get %d candy!", leftovers);
  }
}
