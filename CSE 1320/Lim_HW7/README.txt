// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files included: hw7.h, hw7.c, stack_queue7.h, stack_queue7.c
-------------------
Function Descriptions :
  - void enter_tip(Stack *s) allows the user to enter in tips in a tip jar
    and it is implemented as a stack array.
  - float calculate _pay(Stack *s) calculates the pay for the user by asking
    how many hours did the user work (1 hr = $2). The program then pulls tips
    out of the tip jar and gives it to the user then displaying the updated
    tip jar to the user.

  - QueueInfo *passengers_info(char *filename) reads in the file containing the
    passenger/flight info and creates a queue of it and displays it to the user.
    The queue is implemented using a linked list.
  - void check_flight(QueueInfo *info, int flight) allows the user to find the
    passenger(s) on a specific flight. Outputs the results to the user and sends
    it to a text file called "output.txt".
-------------------
Compilation Instructions:

  For Tips: gcc hw7.c stack_queue7.c [program_name].c
            ./a.out

  For Flights: gcc hw7.c stack_queue7.c [program_name].c
               ./a.out [file name containing passengers].txt
-------------------
Notes:
  The flight functions will always output to a file called "output.txt". Also
  remember to type in the file name right after ./a.out otherwise the functions
  might break.
