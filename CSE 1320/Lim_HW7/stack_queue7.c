#include "stack_queue7.h"
#include "hw7.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

QNode *newNode(char *full_name, int flight) {
  //this creates a new node for the queue
  QNode *temp = malloc(sizeof(QNode));
  temp->full_name = malloc(50);
  strcpy(temp->full_name, full_name);
  temp->flight = flight;
  temp->next = NULL;
  return temp;
}

QueueInfo *createQueueInfo() {
  //this creates the queue
  QueueInfo *q = malloc(sizeof(QueueInfo));
  q->head = q->tail = NULL;
  return q;
}

void enqueue(QueueInfo *q, char *full_name, int flight){
  //this adds a node to the queue
  QNode *temp = newNode(full_name, flight);

  if(q->tail == NULL){
    q->head = q->tail = temp;
    return;
  }

  q->tail->next = temp;
  q->tail = temp;
}

QNode *dequeue(QueueInfo *q){
  //this removes a node from the queue
  if(q->head == NULL) {
    return NULL;
  }

  QNode *temp = q->head;
  q->head = q->head->next;

  if(q->head == NULL) {
    q->tail = NULL;
  }

  return temp;
}

void push(Stack *s, float tip) {
  //this pushes a value to the stack
  if(s->top == (20-1)){//check if stack is full
    printf("Cannot add another tip to the jar.\n");
    return;
  }
  else {
    s->top = s->top + 1;
    s->stk[s->top] = tip;
  }
  return;
}

float pop(Stack *s){
  //this pops a value from the stack
  float tip;
  if(s->top == -1){//check if stack is empty
    printf("Tip jar is empty!\n");
    return (s->top);
  }
  else{
    tip = s->stk[s->top];
    s->top = s->top - 1;
  }
  return(tip);
}

void display(Stack *s){
  //displays the stack
  int i;
  if(s->top == -1){//check if stack is empty
    printf("Tip jar is empty!\n");
    return;
  }
  else{
    printf("\n***Tip Jar***\n");
    for(i = s->top; i >=0; i--){
      printf("$%0.2f\n", s->stk[i]);
    }
  }
  printf("\n");
}

void updated_display(Stack *s){
  //displays the updated stack
  //same as the above display but instead includes the word "(Updated)"
  int i;
  if(s->top == -1){//check is stack is empty
    printf("Tip jar is empty!\n");
    return;
  }
  else{
    printf("\n***Tip Jar (Updated)***\n");
    for(i = s->top; i >=1; i--){
      printf("$%0.2f\n", s->stk[i]);
    }
  }
  printf("\n");
}
