#ifndef STACK_QUEUE
#define STACK_QUEUE

struct QueueNode {
  char *full_name;
  int flight;
  struct QueueNode *next;
};
typedef struct QueueNode QNode;

struct QueueInfo {
  QNode *head;
  QNode *tail;
};
typedef struct QueueInfo QueueInfo;

struct stack {
  float stk[20];
  int top;
};
typedef struct stack Stack;


QNode *newNode(char* full_name, int flight);
QueueInfo *createQueueInfo();
void enqueue(QueueInfo *q, char *full_name, int flight);
QNode *dequeue(QueueInfo *q);

void push(Stack *s, float tip);
float pop(Stack *s);
void display(Stack *s);

#endif
