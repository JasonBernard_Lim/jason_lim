//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code

#include <stdio.h>
#include <math.h>
#define PI 3.14159265358979

int main(int argc, char ** argv){
    double theta;
    printf("Please enter in a value for theta to calculate cosine with: ");
    scanf("%lf", &theta);

    double cos_radians = sin((PI/2)-theta);

    printf("The value of cos(%lf) in radians is: %lf.\n\n", theta, cos_radians);
}
