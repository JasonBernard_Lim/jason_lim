//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code

#include <stdio.h>

int main(int argc, char ** argv){
  int cases;
  printf("How many cases of the flu were there this year?: ");
  scanf("%d", &cases);

  int initial;
  printf("How many initial cases of the flu were there?: ");
  scanf("%d", &initial);

  int years = 0;

  while(initial < cases){
    initial = initial * 2;
    years++;
  }

  printf("The doubling of flu cases has been going on for about %d years.\n\n", years);
}
