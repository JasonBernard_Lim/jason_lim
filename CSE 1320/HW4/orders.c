//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 5: Write a Program

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct order{
  char name[100];
  char payment_type;
  int numOfItems;
  float price;
  int status;
}order;

void input_orders(order *o, int size, char *filename){
  FILE *fp;
  fp = fopen(filename, "r");
  char *token;
  int i;

  if(fp == NULL){
    printf("Incorrect file openend. Exiting....\n");
    exit(0);
  }
  else{
    char line[100];

    for(i = 0; i < size; i++){
      fgets(line,100,fp);

      token = strtok(line, ",");
      strcpy(o[i].name, token);

      token = strtok(NULL, ",");
      o[i].payment_type = token[0];

      token = strtok(NULL, ",");
      o[i].numOfItems = atoi(token);

      token = strtok(NULL, ",");
      o[i].price = atof(token);

      token = strtok(NULL, ",\n");
      o[i].status = atoi(token);
    }
  }
  fclose(fp);
}

void print_out(int status, order *o, int size){//1 means fulfilled, 2 means not fulfilled
  int i;
  for(i = 0; i < size; i++){
    if(o[i].status == status){
      printf("%s\n", o[i].name);
    }
  }
}

int pick_next(order *o, int size){
  int index = -1;
  int i;
  float highest = 0;
  char *next;
  for(i=0; i < size; i++){
    if(o[i].price > highest && o[i].status == 2){
      next = o[i].name;
      index = i;
      highest = o[i].price;
    }
  }
  printf("Next order to fill: %s", next);
  return index;
}

void output_file(char *filename, order *o, int size){
  FILE *fp;
  int i;
  char *mode = "w+";
  fp = fopen(filename, mode);
  if(fp == NULL){
    printf("Incorrect file openend. Exiting....");
    exit(0);
  }
  else{
    for(i = 0; i < size; i++){
      fprintf(fp, "%s,%c,%d,%0.2f,%d\n",o[i].name, o[i].payment_type, o[i].numOfItems, o[i].price, o[i].status);
    }
  }
  fclose(fp);
}

int main(int argc, char **argv){
  int size = atoi(argv[1]);
  order *orders = malloc(sizeof(order) * size);
  int x = 1;
  int choice;

  printf("\n\n***Buongiorno Chef Bartolomeo!***\n\n");
  input_orders(orders, size, argv[2]);

  while(x == 1){
    printf("\nWhat to do?\n1. print out orders\n2. pick next order to fulfill\n3. exit\n");
    scanf("%d", &choice);

    if(choice == 1){
      int status = 0;
      char answer[20];
      printf("\nDo you want to see all orders to fill or orders already completed? Type fill or completed\n");
      scanf("%s", answer);
      if(strcmp(answer, "fill") == 0){
        status = 2;
        printf("Need to fill:\n");
        print_out(status, orders, size);
      }
      else if(strcmp(answer, "completed") == 0){
        status = 1;
        printf("Completed:\n");
        print_out(status, orders, size);
      }
      else{
        printf("Invalid Choice. Returning to menu.....");
      }
    }
    else if(choice == 2){
      char answer2[20];
      int index = pick_next(orders, size);
      if(index != -1){
        printf("\nGo ahead and fulfill this order? y or n\n");
        scanf("%s", answer2);
        if(strcmp(answer2, "y") == 0){
          orders[index].status = 1;
          printf("Fulfilled!\n");
        }
        else if(strcmp(answer2, "n") == 0){
          printf("Not fulfilled.\n");
        }
        else{
          printf("Invalid Choice. Returning to menu...");
        }
      }
      else{
        printf("\nAll orders fulfilled!\n");
      }
    }
    else if(choice == 3){
      x=0;
      printf("Saving information....Ciao!\n");
      output_file(argv[2], orders, size);
      free(orders);
    }
    else{
      printf("Not a valid menu choice.\n");
    }
  }
}
