//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code

#include <stdio.h>

int main(int argc, char** argv){
  float pop;
  printf("Hello! What is the current population of your town?: ");
  scanf("%f", &pop);
  float years;
  printf("How many years has it been since the last population count?: ");
  scanf("%f", &years);
  float percent;
  printf("What was the percent increase of the new people in town per year? (Please keep in percent not a decimal): ");
  scanf("%f", &percent);

  percent = (percent / 100.0) + 1;

  int i;
  for(i = 0; i < years; i++){
    pop = pop * percent;
  }

  printf("The new population for the town is about %0.0f.\n\n", pop);
}
