//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 3: Write a Program

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int registered(FILE *fp){
  printf("\n***Registered so far:***\n\n");

  int counter = 0;
  char line[100];

  while(fgets(line, 100, fp) != NULL) {
    printf("%s", line);
    counter++;
  }
  printf("\n");
  return counter;
}

void new_register(FILE *fp){
  char enter;
  char line[100];

  printf("Enter name: ");
  scanf("%c", &enter);
  fgets(line, 100, stdin);
  printf("Adding: %s\n", line);

  fprintf(fp, "%s", line);
}

int main(int argc, char ** argv){
  int people;
  int lines;
  FILE *fp;
  char *mode = "a+";

  fp = fopen(argv[1], mode);
  if(fp == NULL){
    printf("No file opened.\n");
  }
  else{
    fseek(fp, 0, SEEK_SET);
    lines = registered(fp);
    if(lines == 10){
      printf("\n\nTarget Reached! Exiting...\n\n");
      exit(0);
    }
    else{
      int i;
      printf("\nHow many people to ask right now?\n");
      scanf("%d", &people);

      for(i = 0; i < people; i++){
        char choice[100];
        printf("\n-Person %d: Would you like to register to vote?\n", i+1);
        scanf("%s", choice);
        if(strcmp(choice, "n") == 0){
          printf("Ok.\n");
        }
        else if(strcmp(choice, "y") == 0){
          fprintf(fp, "%d. ", lines+1);
          new_register(fp);
          fseek(fp, 0, SEEK_SET);
          lines = registered(fp);
          if(lines == 10){
            printf("\nTarget Reached! Exiting...\n\n");
            exit(0);
          }
        }
        else{
          printf("Invalid Option.\n");
          i--;
        }
      }
      printf("\n%d people asked! Taking a break!\n\n", people);
      fclose(fp);
    }
  }
}
