// Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 4: Write a Program

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct dragon {
  char name[100];
  char colors[3];
  int heads;
  int tails;
} dragon;

void dragon_info(dragon *d) {
  char line[500];
  int i, j;
  char *token;

  fgets(line, 500, stdin);
  token = strtok(line, ",");
  for (i = 0; i < 4; i++) {
    token = strtok(NULL, ",");
    strcpy(d[i].name, token);
    token = strtok(NULL, ",");
    d[i].colors[0] = token[0];
    token = strtok(NULL, ",");
    d[i].colors[1] = token[0];
    token = strtok(NULL, ",");
    d[i].colors[2] = token[0];
    token = strtok(NULL, ",");
    d[i].heads = atoi(token);
    token = strtok(NULL, ",\n");
    d[i].heads = atoi(token);
  }
}

void color(char *color, dragon *d) {
  char *string = NULL;

  if (color[0] == 'r') {
    string = "red";
  } else if (color[0] == 'b') {
    string = "blue";
  } else if (color[0] == 'w') {
    string = "white";
  } else if (color[0] == 'g') {
    string = "green";
  } else if (color[0] == 'y') {
    string = "yellow";
  }

  printf("***All the %s dragons:***\n", string);
  int i, j;
  for (i = 0; i < 4; i++) { // 4 can be hardcoded as the main cannot be modified
    if (d[i].colors[0] == color[0]) {
      printf("%s is %s\n", d[i].name, string);
    } else if (d[i].colors[1] == color[0]) {
      printf("%s is %s\n", d[i].name, string);
    } else if (d[i].colors[2] == color[0]) {
      printf("%s is %s\n", d[i].name, string);
    }
  }
}

int main(int argc, char **argv) {
  dragon total[4];
  dragon_info(total);
  char c = 'b';
  color(&c, total);
}
