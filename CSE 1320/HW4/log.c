//Jason Bernard "JB" Lim
// ID: 1001640993
// Problem 2: Tiny Code

#include <stdio.h>
#include <math.h>

int main(int argc, char** argv){
  double x;
  double inside;
  printf("Please type the number inside the parentheses in log_3: ");
  scanf("%lf", &inside);
  x = log(inside) / log(3); //isnt this in math.h???
  printf("\nThat is equal to %lf.\n\n", x);
}
