/*

    NAME: Jason Bernard Lim
    ID: 1001640993

*/

#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#define WHITESPACE " \t\n"      // We want to split our command line up into tokens
                                // so we need to define what delimits our tokens.
                                // In this case  white space
                                // will separate the tokens on our command line

#define MAX_COMMAND_SIZE 255    // The maximum command-line size

//*Requirement 7
#define MAX_NUM_ARGUMENTS 10    

int main() {
    //set up the pid array outside of the while loop 
    //    so the array doesnt get constantly reset
    int pids[15];
    int pid_counter = 0;

    //set up the cmd_klist array for the history command
    //    same logic as above
    char **cmd_list = (char **) malloc(15 * MAX_COMMAND_SIZE);
    int cmd_counter = 0;

    char * cmd_str = (char*) malloc( MAX_COMMAND_SIZE );

    while( 1 ) {
        // Print out the msh prompt
        printf ("msh> ");

        // Read the command from the commandline.  The
        // maximum command that will be read is MAX_COMMAND_SIZE
        // This while command will wait here until the user
        // inputs something since fgets returns NULL when there
        // is no input
        while( !fgets (cmd_str, MAX_COMMAND_SIZE, stdin) );

        //check if the cmd_str entered contains a '!'
        //if so, set the cmd_str to the correct index from the list
        if(strchr(cmd_str, '!') != NULL) {
            //take cmd_str[1] then convert to int
            int index = atoi(&cmd_str[1]);
            //check if the index is greater than the size of the list
            //if so, print out the error
            if(index >= cmd_counter) {
                printf("Command not in history.\n");
            }
            else {
                //if not, use the converted index and set it as the cmd for the new process
                cmd_str = strdup(cmd_list[index]);
            }
        }

        /* Parse input */
        char *token[MAX_NUM_ARGUMENTS];

        int  token_count = 0;                                 
                                                            
        // Pointer to point to the token
        // parsed by strsep
        char *arg_ptr;                                         
                                                            
        char *working_str  = strdup( cmd_str );                

        // we are going to move the working_str pointer so
        // keep track of its original value so we can deallocate
        // the correct amount at the end
        char *working_root = working_str;

        // Tokenize the input strings with whitespace used as the delimiter
        while ( ( (arg_ptr = strsep(&working_str, WHITESPACE ) ) != NULL) && 
                (token_count<MAX_NUM_ARGUMENTS)) {
        token[token_count] = strndup( arg_ptr, MAX_COMMAND_SIZE );
        if( strlen( token[token_count] ) == 0 ) {
            token[token_count] = NULL;
        }
            token_count++;
        }

        //*Requirement 6
        //This satisfies requirement 6 as the condition checks
        //     for if the user types a blank space (NULL)
        if (token[0] != NULL) {


            //*Requirement 12
            //Check if the list has reached max capacity
            if(cmd_counter == 15) {
                //if so, move every item down one index
                //     then set the last index to the new item
                int i;
                for(i = 0; i < 14; i++) {
                    cmd_list[i] = cmd_list[i+1];
                }
                cmd_list[14] = strdup(cmd_str);
            }
            else {
                //if not, set the item to the list
                //     then increment the counter
                cmd_list[cmd_counter] = strdup(cmd_str);
                cmd_counter++;
            }
            

            //*Requirement 10
            //Check to see if the first "command" is "cd"
            //     if so, change the directory
            if(strcmp(token[0], "cd") == 0) {
                chdir(token[1]);
            }

            //* Requirement 5 
            //Self-explanatory
            //     If user types quit/exit, exit with status 0
            else if(strcmp(token[0], "quit") == 0 || strcmp(token[0], "exit") == 0 ) {
                exit(0);
            }    
            
            //If the user enters history list the previous commands
            else if(strcmp(token[0], "history") == 0) {
                int i;
                for(i = 0; i < cmd_counter; i++) {
                    printf("%d. %s", i, cmd_list[i]);           
                }
            }
            else if(strcmp(token[0], "showpids") == 0) {//* Requirement 11 basically done 
            //* 0th case shouldnt work as no process was spawned
                int i;
                for(i = 0; i < pid_counter; i++) {
                    printf("%d. %d\n", i, pids[i]);
                }
            }

            //fork the process to execute it
            pid_t pid = fork(); 

            if (pid == 0) {
                //*execvp() satisfies Requirement 8
                int ret = execvp(token[0], token);

                //if the user types any of these don't print the error message
                if(strcmp(token[0], "cd") == 0 || strcmp(token[0], "showpids") == 0
                || strcmp(token[0], "history") == 0 || strchr(cmd_str, '!') != NULL) {
                    return 0;
                }
                else if(ret == -1) {
                    //If there are too many arguments print out a statement
                    if(token_count >= MAX_NUM_ARGUMENTS) {
                        printf("Too many arguments.\n");
                        return 0;
                    }
                    //*Requirement 2
                    else {
                        //if the command was not found through execvp print out the message
                        printf("%s: Command not found.\n", token[0]);
                        return 0;
                    }
                }
            }
            else {
                //*Requirement 11
                //add the pid to the pid list
                //if array is full move the list then add the new pid
                if(pid_counter == 15) {
                    int i;
                    for(i = 0; i < 14; i++) {
                        pids[i] = pids[i+1];
                    }
                    pids[14] = pid;
                }
                else {
                    //else just add the pid and increment the counter
                    pids[pid_counter] = pid;
                    pid_counter++;
                }
                
                int status;
                wait(&status);

            }

        }
        free( working_root );

    }
    return 0;
}
