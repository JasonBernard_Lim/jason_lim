// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: height.cpp
--------------------------------
Function Description:
  This program checks to see if two riders are allowed to ride their ride in the
  amusement park. This program includes 3 classes: Rider, Ride, and Amusement_park.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ height.cpp
    ./a.out

  Sample run:

    student@cse1325:/media/sf_jason_lim/CSE 1325/HW2$ g++ height.cpp
    student@cse1325:/media/sf_jason_lim/CSE 1325/HW2$ ./a.out
    ~~~Amusement Park Info~~~

    Ride 1- Enter minimum ride height and ride name:
    40 ride1

    Ride 2- Enter minimum ride height and ride name:
    20 ride2

    Ride 3- Enter minimum ride height and ride name:
    50 ride3

    -Adding rider to line.


    ~~~Amusement Park Info~~~

    Ride 1- Enter minimum ride height and ride name:
    50 ride1

    Ride 2- Enter minimum ride height and ride name:
    60 ride2

    -Sorry can't add rider-too short.



--------------------------------
Notes:
