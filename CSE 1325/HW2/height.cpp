// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

class Rider {//rider class
  string name;
  int height;

public:
  Rider(string name, int height) {
    this->name = name;
    this->height = height;
  }

  Rider(int height) {
    this->height = height;
  }

  Rider() {//default constructor

  }

  string get_name() {
    return name;
  }

  int get_height() {
    return height;
  }
};

class Ride {//the ride class
  string ride_name;
  int min_height;

  Rider r;

public:
  Ride(string ride_name, int min_height) {
    this->ride_name = ride_name;
    this->min_height = min_height;
  }

  int get_min_height() {
    return min_height;
  }

  string get_ride_name() {
    return ride_name;
  }

  void add_line(Rider r) {//allow to add a rider to a ride
      //get the necessary variables
      int ride_req = get_min_height();
      int rider_height = r.get_height();

      //check if rider's height is appropriate
      if(rider_height < ride_req) {
        cout << "\n-Sorry can't add rider-too short.\n\n" << endl;
      }
      else {
        cout << "\n-Adding rider to line.\n\n" << endl;
      }
  }
};

class Amusement_park {//the amusement park class
  vector<Ride> all_rides;

public:
  Amusement_park(int num_of_rides) {
    string answer;
    string ride_name;
    int ride_height;

    cout << "~~~Amusement Park Info~~~" << endl;

    for(int i = 1; i < (num_of_rides+1); i++) {
      cout << "\nRide " << i << "- Enter minimum ride height and ride name: " << endl;
      getline(cin, answer);

      stringstream ss(answer);

      ss >> ride_height >> ride_name;

      Ride ride(ride_name, ride_height);
      all_rides.push_back(ride);

    }
  }

  Ride get_ride(int n) {//find the ride in the vector of rides
    return all_rides[n-1];
  }

};


int main() {//DO NOT MODIFY
  Rider r1("Yaris", 45); //name, height in inches
  Rider r2(49); //height in inches

  Amusement_park a1(3); //3 is the number of rides in the amusement park
  a1.get_ride(1).add_line(r1); //add a rider to the line of a ride

  Amusement_park a2(2); //2 is the number of rides in the amusement park
  a2.get_ride(1).add_line(r2); //add a rider to the line of a ride

  return 0;
}
