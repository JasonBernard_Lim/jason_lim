// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

class money {//money class
  string type;
  float conversion_rate;

public:

  money(string type, float conversion_rate) {//money constructor
    this->type = type;
    this->conversion_rate = conversion_rate;
  }

  money() {//default constructor

  }

  string get_type() {
    return type;
  }

  float get_conversion_rate() {
    return conversion_rate;
  }

};

class exchange_office {//office class
  string name;
  string conversions;
  string manager;
  money m;

public:

  exchange_office(string name, string conversions, string manager) {
    this->name = name;
    this->conversions = conversions;
    this->manager = manager;
  }

  exchange_office() {//default contructor

  }

  float convert_money() {//this function allows for the conversion of money
    float amount = 0;
    string user_type;

    //get all the necessary variables
    string name = get_name();
    string manager = get_manager();
    string money_type = m.get_type();
    float conversion_rate = m.get_conversion_rate();

    //ask user input
    cout << "Welcome to " << name << " Conversions. Please contact the manager " << manager << " if you have any complaints." << endl;
    cout << "What currency are you converting to dollars and how much? ";
    cin >> amount >> user_type;

    //if the type of money is allowed convert if not prompt
    if(money_type.compare(user_type) == 0) {
      amount = amount / conversion_rate;
      cout << setprecision(2) << fixed << "Here you go: $" << amount;
      return amount;
    }
    else {
      cout << "We do not convert the " << user_type << " currency here. Sorry." << endl;
      return 0;
    }
  }

  string get_name() {
    return name;
  }

  string get_conversions() {
    return conversions;
  }

  string get_manager() {
    return manager;
  }

  money get_money() {
    return m;
  }

  void set_money(money m) {
    this->m = m;
  }
};

void print_out_dollars(float total_dollars) {//print total dollar function
  cout << setprecision(2) << fixed << "$$$Total dollars given out: $" << total_dollars << "\nExiting..." << endl;
}

int main(int argc, char **argv) {
  //create necessary variables
  string line;
  string name;
  string conversions;
  string manager;
  bool check = true;
  float total_dollars = 0.00;
  //create the 2 money objects
  money yen("yen", 108.40);
  money peso("peso", 19.41);

  //create the 2 offices
  cout << "Enter the name of exchange office and manager: ";
  getline(cin, line);

  stringstream ss(line);
  ss >> name >> conversions >> manager;

  exchange_office o1(name, conversions, manager);
  //////////////////////////////////////////////////////////////////////////////
  cout << "Enter the name of exchange office and manager: ";
  getline(cin, line);

  stringstream ss2(line);
  ss2 >> name >> conversions >> manager;

  exchange_office o2(name, conversions, manager);


  while(check) {
    //ask user for location
    string answer;
    cout << "\n\n********\nHello traveler! Where are you in the airport? ";
    cin >> answer;
    cout << "********" << endl;

    //if exit print out total and exit while loop
    if(answer.compare("exit") == 0 || answer.compare("Exit") == 0) {
      print_out_dollars(total_dollars);
      check = false;
    }
    else {
      //store yen into ABC and peso int DEF
      string name = o1.get_name();
      if(name.compare("ABC") == 0) {//o1 is ABC
        o1.set_money(yen);
        o2.set_money(peso);
      }
      else {//o2 is ABC
        o1.set_money(peso);
        o2.set_money(yen);
      }

      //convert the money
      if(answer.compare("North") == 0 || answer.compare("north") == 0 || answer.compare("East") == 0 || answer.compare("east") == 0){
        if(o1.get_name().compare("ABC") == 0) {
          total_dollars += o1.convert_money();
        }
        else if(o2.get_name().compare("ABC") == 0) {
          total_dollars += o2.convert_money();
        }
      }
      else if(answer.compare("South") == 0 || answer.compare("south") == 0 || answer.compare("West") == 0 || answer.compare("west") == 0){
        if(o1.get_name().compare("DEF") == 0) {
          total_dollars += o1.convert_money();
        }
        else if(o2.get_name().compare("DEF") == 0) {
          total_dollars += o2.convert_money();
        }
      }
      else {
        cout << "Unable to locate........." << endl;
      }

    }
  }











}
