// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: money.cpp
--------------------------------
Function Description:
  This program allows the user to enter in two conversion offices in an airport.
  The program asks for a location in the airport and prompts the user to the correct
  conversion office. The user can then asks for a conversion of money but only
  from the currencies that office is allowed to convert.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ money.cpp
    ./a.out

  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW2$ g++ money.cpp
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW2$ ./a.out
  Enter the name of exchange office and manager: DEF Conversions John
  Enter the name of exchange office and manager: ABC Conversions Bob


  ********
  Hello traveler! Where are you in the airport? north
  ********
  Welcome to ABC Conversions. Please contact the manager Bob if you have any complaints.
  What currency are you converting to dollars and how much? 200 yen
  Here you go: $1.85

  ********
  Hello traveler! Where are you in the airport? West
  ********
  Welcome to DEF Conversions. Please contact the manager John if you have any complaints.
  What currency are you converting to dollars and how much? 500 peso
  Here you go: $25.76

  ********
  Hello traveler! Where are you in the airport? left
  ********
  Unable to locate.........


  ********
  Hello traveler! Where are you in the airport? North
  ********
  Welcome to ABC Conversions. Please contact the manager Bob if you have any complaints.
  What currency are you converting to dollars and how much? 500 euro
  We do not convert the euro currency here. Sorry.


  ********
  Hello traveler! Where are you in the airport? exit
  ********
  $$$Total dollars given out: $27.60
  Exiting...


--------------------------------
Notes:
  - The program only works with "ABC" and "DEF" Conversion offices. The program will
    not work accordingly if the offices are named otherwise.

  - The only currencies that are allowed in the program are yen and peso, however
    ABC is only allowed to convert from yen and DEF is only allowed to convert
    from peso.

  - In the actual conversion of money if you input only a string instead of a
    number and a string this will cause a bug with an infinite loop so you will
    have to force exit.
