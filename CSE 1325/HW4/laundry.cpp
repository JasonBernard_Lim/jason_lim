// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

class Person {
protected:
  string name;
  string id;
  float balance;
  float detergent;
  int load = 0;
public:
  Person(string name, string id, float balance, float detergent) {
    this->name = name;
    this->id = id;
    this->balance = balance;
    this->detergent = detergent;
  }

  string get_name() {
    return this->name;
  }

  string get_id() {
    return this->id;
  }

  float get_balance() {
    return this->balance;
  }

  void add_balance(float cost) {
    this->balance = this->balance + cost;
  }

  void sub_balance(float cost) {
    this->balance = this->balance - cost;
  }

  float get_detergent() {
    return this->detergent;
  }

  virtual string get_type() {

  }

  void add_detergent(float detergent) {
    this->detergent = this->detergent + detergent;
  }

  void sub_detergent(float detergent) {
    this->detergent = this->detergent - detergent;
  }

  int get_load() {
    return this->load;
  }

  void set_load(int num) {//slow = 1, fast = 2, very slow = 3
    this->load = num;
  }
};

class discount: public Person {
private:
  string type;

public:
  discount(string name, string id, float balance, float detergent):Person(name, id, balance, detergent) {
    this->type = "discount";
  }

  string get_type() {
    return this->type;
  }
};

class regular: public Person {
private:
  string type;

public:
  regular(string name, string id, float balance, float detergent):Person(name, id, balance, detergent) {
    this->type = "regular";
  }

  string get_type() {
    return this->type;
  }
};

class elite: public Person {
private:
  string type;

public:
  elite(string name, string id, float balance, float detergent):Person(name, id, balance, detergent) {
    this->type = "elite";
  }

  string get_type() {
    return this->type;
  }
};

class Machine {
protected:
  float cost;
  float deter_need;
public:
  Machine(float cost, float deter_need) {
    this->cost = cost;
    this->deter_need = deter_need;
  }

  float get_cost() {
    return this->cost;
  }

  float get_deter_need() {
    return this->deter_need;
  }

  virtual string get_machine_type() {

  }

};

class fast: public Machine {
private:
  string type = "fast";
public:
  fast(float cost, float deter_need):Machine(cost, deter_need) {

  }

  string get_machine_type() {
    return this->type;
  }
};

class slow: public Machine {
private:
  string type = "slow";
public:
  slow(float cost, float deter_need):Machine(cost, deter_need) {

  }

  string get_machine_type() {
    return this->type;
  }
};

class very_slow: public Machine {
private:
  string type = "very";
public:
  very_slow(float cost, float deter_need):Machine(cost, deter_need) {

  }

  string get_machine_type() {
    return this->type;
  }
};

class laundromat {
  public:
    int numFasts = 0;
    int numSlows = 0;
    int numVerySlows = 0;
    vector<Person> all_users; 

    laundromat(string user_file, string machine_file) {
      cout << "WELCOME!" << endl;
      ifstream inFile;

      //*************READ IN MACHINES********************************************************
      inFile.open(machine_file);

      if(!inFile.is_open()) {
        cout << "No File Found!" << endl;
        exit(1);
      }

      string intermediate;
      string line_from_file;

      getline(inFile, line_from_file);

      stringstream delimt_line(line_from_file);

      getline(delimt_line, intermediate, ',');
      numFasts = stoi(intermediate);

      getline(delimt_line, intermediate, ',');
      numSlows = stoi(intermediate);

      getline(delimt_line, intermediate, '\n');
      numVerySlows = stoi(intermediate);

      inFile.close();
      //*********************READ IN USERS**************************************
      inFile.open(user_file);

      if(!inFile.is_open()) {
        cout << "No File Found!" << endl;
        exit(1);
      }

      while(!inFile.eof()) {
        getline(inFile, line_from_file);
        stringstream delimt_line2(line_from_file);

        getline(delimt_line2, intermediate, ',');
        string name = intermediate;

        getline(delimt_line2, intermediate, ',');
        string id = intermediate;
        string substr = id.substr(1,3);
        int num = stoi(substr);

        getline(delimt_line2, intermediate, ',');
        float balance = stof(intermediate);

        getline(delimt_line2, intermediate, '\n');
        float detergent = stof(intermediate);

        if(num <= 500) {
          all_users.push_back(discount (name, id, balance, detergent));
        }
        else if(num <= 900) {
          regular p(name, id, balance, detergent);
          all_users.push_back(p);
        }
        else {
          elite p(name, id, balance, detergent);
          all_users.push_back(p);
        }
      }
      cout << "Laundromat indo read in:" << endl << endl;
      cout << "***AVAILABLE***" << endl;
      cout << "Fast Machines: " << numFasts << endl;
      cout << "Slow Machines: " << numSlows << endl;
      cout << "Very Slow Machines: " << numVerySlows << endl << endl;
      cout << "All Registered Users info read in." << endl << endl;
    }

    void print_out_washers(string type) {
      cout << "\n***AVAILABLE***" << endl;
      if(type.compare("discount") == 0) {
        cout << "Slow Machines: " << numSlows << endl;
        cout << "Very Slow Machines: " << numVerySlows << endl << endl;
      }
      else if(type.compare("regular") == 0) {
        cout << "Slow Machines: " << numSlows << endl;
        cout << "Fast Machines: " << numFasts << endl;
      }
      else {
        cout << "Fast Machines: " << numFasts << endl;
        cout << "Slow Machines: " << numSlows << endl;
        cout << "Very Slow Machines: " << numVerySlows << endl << endl;
      }
    }

    void laundry(string id) {
      fast f(0.5, 2);
      slow s(0.35, 1.5);
      very_slow vs(0.25, 1);

      Person *current = NULL;

      for(int i = 0; i < all_users.size(); i++) {
        if(all_users[i].get_id().compare(id) == 0) {
          current = &all_users.at(i);
          break;
        }
      }
      if(current == NULL) {
        cout << "Sorry, we do not have this ID registered.\n-------------" << endl;
        return;
      }
      string answer;
      cout << "~Welcome " << current->get_type() << " member." << endl;
      cout << "1) Start a laundry load" << endl << "2) Finish a laundry load" << endl;
      cin >> answer;

      if(answer.compare("start") == 0) {
        if(current->get_load() != 0) {
          cout << "~You already have one load in a machine. You can only have one load in at a time." << endl;
          return;
        }
        print_out_washers(current->get_type());

        cout << "~Which machine would you like to use?" << endl;
        cin >> answer;

        if(current->get_type().compare("discount") == 0) {
          bool check = true;
          while(check) {
            if(answer.compare("slow") == 0 && numSlows == 0) {
              cout << "~Sorry, we do not have any slow machines currently available Enter again." << endl;
              cin >> answer;
            }
            else if(answer.compare("very") == 0 && numVerySlows == 0) {
              cout << "~Sorry, we do not have any very slow machines currently available Enter again." << endl;
              cin >> answer;
            }
            else {
              check = false;
            }
          }
          
          if(answer.compare("slow") == 0) {
            float amount = 0;
            while(current->get_balance() < s.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << s.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < s.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << s.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numSlows--;
            current->set_load(1);
            current->sub_balance(s.get_cost());
            current->sub_detergent(s.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
          else if(answer.compare("very") == 0) {
            float amount = 0;
            while(current->get_balance() < vs.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << vs.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < vs.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << vs.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numVerySlows--;
            current->set_load(3);
            current->sub_balance(vs.get_cost());
            current->sub_detergent(vs.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
        }
        else if(current->get_type().compare("regular") == 0) {
          bool check = true;
          while(check) {
            if(answer.compare("slow") == 0 && numSlows == 0) {
              cout << "~Sorry, we do not have any slow machines currently available Enter again." << endl;
              cin >> answer;
            }
            else if(answer.compare("fast") == 0 && numFasts == 0) {
              cout << "~Sorry, we do not have any fast machines currently available Enter again." << endl;
              cin >> answer;
            }
            else {
              check = false;
            }
          }

          if(answer.compare("slow") == 0) {
            float amount = 0;
            while(current->get_balance() < s.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << s.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < s.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << s.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numSlows--;
            current->set_load(1);
            current->sub_balance(s.get_cost());
            current->sub_detergent(s.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
          else if(answer.compare("fast") == 0) {
            float amount = 0;
            while(current->get_balance() < f.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << f.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < f.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << f.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numFasts--;
            current->set_load(2);
            current->sub_balance(f.get_cost());
            current->sub_detergent(f.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
        }
        else if(current->get_type().compare("elite") == 0){
          bool check = true;
          while(check) {
            if(answer.compare("slow") == 0 && numSlows == 0) {
              cout << "~Sorry, we do not have any slow machines currently available Enter again." << endl;
              cin >> answer;
            }
            else if(answer.compare("very") == 0 && numVerySlows == 0) {
              cout << "~Sorry, we do not have any very slow machines currently available Enter again." << endl;
              cin >> answer;
            }
            else if(answer.compare("fast") == 0 && numFasts == 0) {
              cout << "~Sorry, we do not have any fast machines currently available Enter again." << endl;
              cin >> answer;
            }
            else {
              check = false;
            }
          }

          if(answer.compare("slow") == 0) {
            float amount = 0;
            while(current->get_balance() < s.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << s.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < s.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << s.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numSlows--;
            current->set_load(1);
            current->sub_balance(s.get_cost());
            current->sub_detergent(s.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
          else if(answer.compare("fast") == 0) {
            float amount = 0;
            while(current->get_balance() < f.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << f.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < f.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << f.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numFasts--;
            current->set_load(2);
            current->sub_balance(f.get_cost());
            current->sub_detergent(f.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
          else if(answer.compare("very") == 0) {
            float amount = 0;
            while(current->get_balance() < vs.get_cost()) {
              cout << "~Sorry you do not have enough money for this. You have " << current->get_balance() << " and you need " << vs.get_cost() << endl;
              cout << "~Would you like to add money to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How much?" << endl;
                cin >> amount;
                current->add_balance(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            while(current->get_detergent() < vs.get_deter_need()) {
              cout << "~Sorry you do not have enough detergent for this. You have " << current->get_detergent() << " and you need " << vs.get_deter_need() << endl;
              cout << "~Would you like to add detergent to your account?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                cout << "~How many oz?" << endl;
                cin >> amount;
                current->add_detergent(amount);
              }
              else if(answer.compare("no") == 0) {
                return;
              }
            }

            cout << "~Loading machine..." << endl;
            numVerySlows--;
            current->set_load(3);
            current->sub_balance(vs.get_cost());
            current->sub_detergent(vs.get_deter_need());
            cout << "~You currently have: $" << current->get_balance() << " left and " << current->get_detergent() << " oz of detergent left." << endl;
            cout << "-------------" << endl;
          }
        }
      }
      else if(answer.compare("finish") == 0) {
        if(current->get_load() == 0) {
          cout << "~You do not have a load in a machine." << endl;
          return;
        }

        while(true) {
          cout << "~You currently have one laundry load. Would you like to pick up?" << endl;
          cin >> answer;

          if(answer.compare("no") == 0) {
            cout << "Ok." << endl;
            cout << "-------------" << endl;
            return;
          }
          else if(answer.compare("yes") == 0) {
            while(true) {
              cout << "~Would you like a receipt?" << endl;
              cin >> answer;

              if(answer.compare("yes") == 0) {
                ofstream outFile;
                outFile.open("receipt.txt");

                if(current->get_load() == 1) {//slow
                  outFile << current->get_name() << " Slow " << current->get_balance() << " " << current->get_detergent();
                  numSlows++;
                  current->set_load(0);
                }
                else if(current->get_load() == 2) {//fast
                  outFile << current->get_name() << " Fast " << current->get_balance() << " " << current->get_detergent();
                  numFasts++;
                  current->set_load(0);
                }
                else if(current->get_load() == 3) {//very slow
                  outFile << current->get_name() << " Very Slow " << current->get_balance() << " " << current->get_detergent();
                  numVerySlows++;
                  current->set_load(0);
                }

                cout << "~Thank you for your business." << endl;
                cout << "-------------" << endl;

                outFile.close();
              }
              else if(answer.compare("no") == 0) {
                if(current->get_load() == 1) {//slow
                  numSlows++;
                  current->set_load(0);
                }
                else if(current->get_load() == 2) {//fast
                  numFasts++;
                  current->set_load(0);
                }
                else if(current->get_load() == 3) {//very slow
                  numVerySlows++;
                  current->set_load(0);
                }

                cout << "~Thank you for your business." << endl;
                cout << "-------------" << endl;
              }
            }
          }
        }
      }
    }
}; 

int main(int argc, char **argv) {
  bool check = true;
  laundromat l(argv[1], argv[2]);
  string answer;
  while(check) {
    cout << "~Please enter your ID:" << endl;
    cin >> answer;

    if(answer.compare("exit") == 0) {
      check = false;
    }
    else {
      l.laundry(answer);
    }
  }
}
