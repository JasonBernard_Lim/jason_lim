// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

class Watch {//create the watch parent
protected://create the variabels to be passed down
  int age;
  float price;
  string type; 
  float price_modifier;
public:
  Watch(int age, int price, string type) {//constructor
    this->age = age;
    this->price = price;
    this->type = type;
  }

  void repair() {//repair function
    cout << "The cost to repair your watch will be: $" << (this->price * this->price_modifier) << endl;
  }

  //the various getters
  int get_age(){
    return this->age;
  }

  float get_price(){
    return this->price;
  }

  string get_type(){
    return this->type;
  }
};

class designer: public Watch {
private:
  string make;//for designer watches the make is the only unique variable

public:
  designer(int age, int price, string type):Watch(age, price, type) {

  }

  void set_make(string make) {
    this->make = make;
  }

  void set_modifier() {//function to set the price modifier
    if(this->make.compare("gold") == 0) {
      this->price_modifier = 0.25;
    }
    else if(this->make.compare("silver") == 0) {
      this->price_modifier = 0.33;
    }
  }
};

class non_designer: public Watch {
private:
  bool on_list;//for nons the only unique factor is that if it is on the list

public:
  non_designer(int age, int price, string type):Watch(age, price, type) {

  }

  void check_list() {//function to check the list
    int answer;
    cout << "Is your watch on the 'Free Repair List'? 1.Yes 2.No: ";
    cin >> answer;
    if(answer == 1) {
      this->on_list = true;
    }
    else if(answer == 2) {
      this->on_list = false;
    }
  }

  void set_modifier() {
    if(this->on_list == true) {
      this->price_modifier = 0;
    }
    else {
      this->price_modifier = 0.15;
    }
  }
};

class antique: public Watch {

public:
  antique(int age, int price, string type):Watch(age, price, type) {

  }

  void antique_repair() {//for antiques the only thing that is unique is the repair condition
      cout << "The cost to repair your watch will be: $" << (this->age * 0.5) << endl;
  }
};

int main(int argc, char **argv) {
  //ask for input
  int age = 0;
  float price = 0;
  cout << "Please enter the age of your watch: ";
  cin >> age;
  cout << "Please enter the price of your watch: $";
  cin >> price;

  if(age > 50) {//if the watch is antique
    antique w(age, price, "antique");
    cout << "You have a/an " << w.get_type() << " watch that costs $" << w.get_price() << " and is " << w.get_age() << " yrs old." << endl;
    cout << "Would you like to repair? 1.Yes 2.No: ";
    cin >> age;
    if(age == 1) {
      w.antique_repair();
    }
    else if(age == 2) {
      cout << "Exiting....." << endl;
    }
  }
  else if(price > 800) {//if the watch is deisgner
    designer w(age, price, "designer");
    cout << "You have a/an " << w.get_type() << " watch that costs $" << w.get_price() << " and is " << w.get_age() << " yrs old." << endl;
    cout << "Would you like to repair? 1.Yes 2.No: ";
    cin >> age;
    if(age == 1) {
      string answer;
      cout << "Is your watch gold or silver? ";
      cin >> answer;
      w.set_make(answer);
      w.set_modifier();
      w.repair();
    }
    else if(age == 2) {
      cout << "Exiting....." << endl;
    }
  }
  else {//otherwise the watch is non-designer
    non_designer w(age, price, "non-designer");
    cout << "You have a/an " << w.get_type() << " watch that costs $" << w.get_price() << " and is " << w.get_age() << " yrs old." << endl;
    cout << "Would you like to repair? 1.Yes 2.No: ";
    cin >> age;
    if(age == 1) {
      w.check_list();
      w.set_modifier();
      w.repair();
    }
    else if(age == 2) {
      cout << "Exiting....." << endl;
    }
  }
}
