// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: watches.cpp
--------------------------------
Function Description:
  This program allows the user to input the age and and price of their watch and set its type accordingly. 
  The program will then ask if you want to repair the watch and will print out the price accordingly.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ watches.cpp
    ./a.out 

  Sample run:

    student@cse1325:/media/sf_jason_lim/CSE 1325/HW4$ g++ -g watches.cpp
    student@cse1325:/media/sf_jason_lim/CSE 1325/HW4$ ./a.out 
    Please enter the age of your watch: 60
    Please enter the price of your watch: $200
    You have a/an antique watch that costs $200 and is 60 yrs old.
    Would you like to repair? 1.Yes 2.No: 1
    The cost to repair your watch will be: $30
    student@cse1325:/media/sf_jason_lim/CSE 1325/HW4$ ./a.out 
    Please enter the age of your watch: 20
    Please enter the price of your watch: $2000
    You have a/an designer watch that costs $2000 and is 20 yrs old.
    Would you like to repair? 1.Yes 2.No: 1
    Is your watch gold or silver? gold
    The cost to repair your watch will be: $500
    student@cse1325:/media/sf_jason_lim/CSE 1325/HW4$ ./a.out 
    Please enter the age of your watch: 20
    Please enter the price of your watch: $200
    You have a/an non-designer watch that costs $200 and is 20 yrs old.
    Would you like to repair? 1.Yes 2.No: 2
    Exiting.....


--------------------------------
Notes:
  - When the program asks if you want to repair, the program only takes in 1 or 2 for input.
    Anything inputed otherwise will cause the program to work less than expected.

  - For designer watches, when the program asks for gold or silver the program only takes in
    lower case input.