// Jason Bernard "JB" Lim
// ID: 1001640993
#include "chipotle.h"
#include <gtkmm.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <fstream>

using namespace std;

burrito::burrito() {//create the burito
    bool check = true;
    Gtk::Window w;//open a window and create a while loop until the person's order is correct
    while(check) {
        Gtk::Dialog *dialog = new Gtk::Dialog();
        dialog->set_transient_for(w);
        dialog->set_title("Price: $6.75");

        //ask for the user's choices
        Gtk::Label *protein_label = new Gtk::Label("-Protein: Tofu, Steak, Chicken");
        dialog->get_content_area()->pack_start(*protein_label);
        protein_label->show();

        dialog->add_button("Cancel", 0);
        dialog->add_button("OK", 1);

        Gtk::Entry *protein_entry = new Gtk::Entry();
        protein_entry->set_text("Pick...");
        protein_entry->set_max_length(70);
        protein_entry->show();
        dialog->get_vbox()->pack_start(*protein_entry);

        Gtk::Label *rice_label = new Gtk::Label("-Rice: Cilantro-Lime Brown, Cilantro-Lime White");
        dialog->get_content_area()->pack_start(*rice_label);
        rice_label->show();

        Gtk::Entry *rice_entry = new Gtk::Entry();
        rice_entry->set_text("Pick...");
        rice_entry->set_max_length(100);
        rice_entry->show();
        dialog->get_vbox()->pack_start(*rice_entry);

        Gtk::Label *other_label = new Gtk::Label("-Other: Queso, Sour Cream, Fresh Tomato Salsa");
        dialog->get_content_area()->pack_start(*other_label);
        other_label->show();

        Gtk::Entry *other_entry = new Gtk::Entry();
        other_entry->set_text("Pick...");
        other_entry->set_max_length(70);
        other_entry->show();
        dialog->get_vbox()->pack_start(*other_entry);

        //run the dialog
        int result = dialog->run();

        if(result == 1) {//if the person says ok create a dialog to ask if the person's order is correct
            dialog->close();
            delete dialog;
            delete protein_label;
            delete rice_label;
            delete other_label;

            Gtk::Dialog *dialog2 = new Gtk::Dialog();
            dialog2->set_transient_for(w);
            dialog2->set_title("Order Confirmation");

            string answer;

            if(protein_entry->get_text().compare("none") == 0) {
                /////
            }
            else {
                answer = "Order: " + protein_entry->get_text();
            }

            if(rice_entry->get_text().compare("none") == 0) {
                ////
            }
            else {
                if(answer.compare("") == 0) {
                    answer = "Order: " + rice_entry->get_text();
                }
                else {
                    answer = answer + ", " + rice_entry->get_text();
                }
            }

            if(other_entry->get_text().compare("none") == 0) {
                ////
            }
            else {
                if(answer.compare("") == 0) {
                    answer = "Order: " + other_entry->get_text();
                }
                else {
                    answer = answer + ", " + other_entry->get_text();
                }
            }

            Gtk::Label *order = new Gtk::Label(answer);
            dialog2->get_content_area()->pack_start(*order);
            order->show();

            dialog2->add_button("No", 0);
            dialog2->add_button("Yes", 1);

            int result2 = dialog2->run();

            if(result2 == 1) {//if the order is correct set the order as the order string for the object else, repeat process
                check = false;
                order_str = answer;
                dialog2->close();
                delete dialog2;
                delete order;
                delete protein_entry;
                delete rice_entry;
                delete other_entry;
            }
            else {
                dialog2->close();
                delete dialog2;
                delete order;  
                delete protein_entry;
                delete rice_entry;
                delete other_entry;              
            }
        }
        else {//if the user never wanted to create an order set the order to canceled
            check = false;
            order_str = "CANCELED";

            dialog->close();
            delete dialog;
            delete protein_label;
            delete rice_label;
            delete other_label;
            delete protein_entry;
            delete rice_entry;
            delete other_entry;
        }
    }
}

bowl::bowl() {//this constructor is exactly the same as the burrito except with a different price
              //refer to burrito for more detailed comments about the process
    bool check = true;
    Gtk::Window w;
    while(check) {
        Gtk::Dialog *dialog = new Gtk::Dialog();
        dialog->set_transient_for(w);
        dialog->set_title("Price: $6.50");

        Gtk::Label *protein_label = new Gtk::Label("-Protein: Tofu, Steak, Chicken");
        dialog->get_content_area()->pack_start(*protein_label);
        protein_label->show();

        dialog->add_button("Cancel", 0);
        dialog->add_button("OK", 1);

        Gtk::Entry *protein_entry = new Gtk::Entry();
        protein_entry->set_text("Pick...");
        protein_entry->set_max_length(70);
        protein_entry->show();
        dialog->get_vbox()->pack_start(*protein_entry);

        Gtk::Label *rice_label = new Gtk::Label("-Rice: Cilantro-Lime Brown, Cilantro-Lime White");
        dialog->get_content_area()->pack_start(*rice_label);
        rice_label->show();

        Gtk::Entry *rice_entry = new Gtk::Entry();
        rice_entry->set_text("Pick...");
        rice_entry->set_max_length(100);
        rice_entry->show();
        dialog->get_vbox()->pack_start(*rice_entry);

        Gtk::Label *other_label = new Gtk::Label("-Other: Queso, Sour Cream, Fresh Tomato Salsa");
        dialog->get_content_area()->pack_start(*other_label);
        other_label->show();

        Gtk::Entry *other_entry = new Gtk::Entry();
        other_entry->set_text("Pick...");
        other_entry->set_max_length(70);
        other_entry->show();
        dialog->get_vbox()->pack_start(*other_entry);

        int result = dialog->run();

        if(result == 1) {
            dialog->close();
            delete dialog;
            delete protein_label;
            delete rice_label;
            delete other_label;

            Gtk::Dialog *dialog2 = new Gtk::Dialog();
            dialog2->set_transient_for(w);
            dialog2->set_title("Order Confirmation");

            string answer;

            if(protein_entry->get_text().compare("none") == 0) {
                /////
            }
            else {
                answer = "Order: " + protein_entry->get_text();
            }

            if(rice_entry->get_text().compare("none") == 0) {
                ////
            }
            else {
                if(answer.compare("") == 0) {
                    answer = "Order: " + rice_entry->get_text();
                }
                else {
                    answer = answer + ", " + rice_entry->get_text();
                }
            }

            if(other_entry->get_text().compare("none") == 0) {
                ////
            }
            else {
                if(answer.compare("") == 0) {
                    answer = "Order: " + other_entry->get_text();
                }
                else {
                    answer = answer + ", " + other_entry->get_text();
                }
            }

            Gtk::Label *order = new Gtk::Label(answer);
            dialog2->get_content_area()->pack_start(*order);
            order->show();

            dialog2->add_button("No", 0);
            dialog2->add_button("Yes", 1);

            int result2 = dialog2->run();

            if(result2 == 1) {
                check = false;
                order_str = answer;
                dialog2->close();
                delete dialog2;
                delete order;
                delete protein_entry;
                delete rice_entry;
                delete other_entry;
            }
            else {
                dialog2->close();
                delete dialog2;
                delete order;   
                delete protein_entry;
                delete rice_entry;
                delete other_entry;             
            }
        }
        else {
            check = false;
            order_str = "CANCELED";

            dialog->close();
            delete dialog;
            delete protein_label;
            delete rice_label;
            delete other_label;
            delete protein_entry;
            delete rice_entry;
            delete other_entry;
        }
    }
}

void order_window::bowl_clicked() {//check if there are delivery people availabel, if yes, construct the bowl
    if(delivery_people.size() == 0) {
        Gtk::MessageDialog dialog(*this, "No Delivery People", false, Gtk::MESSAGE_INFO);
        dialog.set_secondary_text("No delivery people hired. Order cannot be proccessed.");
        dialog.run();
    }
    else if(counter == delivery_people.size()) {
        Gtk::MessageDialog dialog(*this, "No Delivery People", false, Gtk::MESSAGE_INFO);
        dialog.set_secondary_text("All delivery people are out for delivery. Order cannot be processed.");
        dialog.run();
    }
    else {
        bowl bowl;

        if(bowl.order_str.compare("CANCELED") != 0) {//if an order was made
            total = total + bowl.price;
            
            string person = delivery_people.at(counter);
            counter++;

            Gtk::MessageDialog dialog(*this, "Delivery Being Sent", false, Gtk::MESSAGE_INFO);
            dialog.set_secondary_text(person + " will be delivering your order. Thank You.");
            dialog.run();
        }
    }
}

void order_window::burrito_clicked() {//this is the exact same function as bowl_clicked() but with a burrito instead
    if(delivery_people.size() == 0) {
        Gtk::MessageDialog dialog(*this, "No Delivery People", false, Gtk::MESSAGE_INFO);
        dialog.set_secondary_text("No delivery people hired. Order cannot be proccessed.");
        dialog.run();
    }
    else if(counter == delivery_people.size()) {
        Gtk::MessageDialog dialog(*this, "No Delivery People", false, Gtk::MESSAGE_INFO);
        dialog.set_secondary_text("All delivery people are out for delivery. Order cannot be processed.");
        dialog.run();
    }
    else {
        burrito burrito;

        if(burrito.order_str.compare("CANCELED") != 0) {//if an order was made
            total = total + burrito.price;

            string person = delivery_people.at(counter);
            counter++;

            Gtk::MessageDialog dialog(*this, "Delivery Being Sent", false, Gtk::MESSAGE_INFO);
            dialog.set_secondary_text(person + " will be delivering your order. Thank You.");
            dialog.run();
        }
    }
}

void order_window::apply_clicked() {//if apply is clicked show a dialog that allows the user to enter in their name and add them to the vector of delivery people
    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Application for Delivery Job");

    Gtk::Label *label = new Gtk::Label("Enter your first name");
    dialog->get_content_area()->pack_start(*label);
    label->show();

    dialog->add_button("Cancel", 0);
    dialog->add_button("OK", 1);

    Gtk::Entry *entry_first = new Gtk::Entry();
    entry_first->set_text("Enter");
    entry_first->set_max_length(50);
    entry_first->show();
    dialog->get_vbox()->pack_start(*entry_first);

    Gtk::Label *label1 = new Gtk::Label("Enter your last name");
    dialog->get_content_area()->pack_start(*label1);
    label1->show();

    Gtk::Entry *entry_last = new Gtk::Entry();
    entry_last->set_text("Enter");
    entry_last->set_max_length(50);
    entry_last->show();
    dialog->get_vbox()->pack_start(*entry_last);

    int result = dialog->run();
    string name;

    if(result == 1) {
        name = entry_first->get_text();
        delivery_people.push_back(name);

        Gtk::MessageDialog dialog2(*this, "Newest Delivery Person:", false, Gtk::MESSAGE_INFO);
        dialog2.set_secondary_text(name);
        dialog2.run();
    }
    else {
        name = "CANCELED";
    }

    dialog->close();

    delete dialog;
    delete label;
    delete label1;
    delete entry_first;
    delete entry_last;
}

void order_window::total_clicked() {//if total is clicked print out the total money earned
    string total_str = to_string(total);
    Gtk::MessageDialog dialog(*this, "Total Money Earned", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text("Total Earned: $" + total_str);
    dialog.run();
}

void order_window::read_in_clicked() {//if read in is clicked, allow the user to enter in the file name, if the file name is correct read the info
    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Enter File Name");

    Gtk::Label *label = new Gtk::Label("Enter the file name");
    dialog->get_content_area()->pack_start(*label);
    label->show();

    dialog->add_button("Cancel", 0);
    dialog->add_button("OK", 1);

    Gtk::Entry *entry_file = new Gtk::Entry();
    entry_file->set_text("Enter");
    entry_file->set_max_length(50);
    entry_file->show();
    dialog->get_vbox()->pack_start(*entry_file);

    int result = dialog->run();

    if(result == 1) {
        string filename = entry_file->get_text();
        ifstream inFile;

        inFile.open(filename);

        if(!inFile.is_open()) {
            Gtk::MessageDialog dialog2(*this, "ERROR", false, Gtk::MESSAGE_INFO);
            dialog2.set_secondary_text("File not found!");
            dialog2.run();

            dialog->close();

            delete dialog;
            delete label;
            delete entry_file;  
        }
        else {
            while(!inFile.eof()) {
                string intermediate;
                string line_from_file;

                getline(inFile, line_from_file);

                stringstream delimt_line(line_from_file);

                getline(delimt_line, intermediate, ' ');
                delivery_people.push_back(intermediate);

                getline(delimt_line, intermediate, '\n');
            }

            Gtk::MessageDialog dialog2(*this, "Delivery People Read", false, Gtk::MESSAGE_INFO);
            dialog2.set_secondary_text("All Delivery People Read");
            dialog2.run();
            
            dialog->close();

            delete dialog;
            delete label;
            delete entry_file; 
        }
    }   
}

order_window::order_window() {//this is the constructor for the main window
    resize(500,500);
    set_title("~~Ronnie's Delivery Service~~");
    //add the buttons, labels, and signal handlers
    bowl_button.add_label("Order a Bowl");
    bowl_button.signal_pressed().connect(sigc::mem_fun(*this, &order_window::bowl_clicked));
    layout.pack_start(bowl_button);

    burrito_button.add_label("Order a Burrito");
    burrito_button.signal_pressed().connect(sigc::mem_fun(*this, &order_window::burrito_clicked));
    layout.pack_start(burrito_button);

    apply.add_label("Apply");
    apply.signal_pressed().connect(sigc::mem_fun(*this, &order_window::apply_clicked));
    layout.pack_start(apply);

    read_in.add_label("Read-In");
    read_in.signal_pressed().connect(sigc::mem_fun(*this, &order_window::read_in_clicked));
    layout.pack_start(read_in);

    total_button.add_label("Total Earned");
    total_button.signal_pressed().connect(sigc::mem_fun(*this, &order_window::total_clicked));
    layout.pack_start(total_button);

    exit.add_label("Exit");
    exit.signal_pressed().connect(sigc::mem_fun(*this, &order_window::close));
    layout.pack_start(exit);
    
    layout.show_all();
    add(layout);
}

order_window::~order_window() {//this is the destructor for the main window

}