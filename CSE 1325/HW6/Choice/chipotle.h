// Jason Bernard "JB" Lim
// ID: 1001640993
#ifndef CHIPOTLE_H
#define CHIPOTLE_H

#include <gtkmm.h>

using namespace std;

class burrito {//this is the burrito class

    public:
    float price = 6.75;
    string order_str;

    burrito();
};

class bowl {//this is the bowl class

    public:
    float price = 6.50;
    string order_str;

    bowl();
};

class order_window:public Gtk::Window {//this is the main window for the program holding all of the main GUI widgets and functions

    public:
    int counter = 0;
    vector<string> delivery_people;
    float total = 0;

    order_window();
    virtual ~order_window();

    protected:
    void bowl_clicked(); 
    void burrito_clicked(); 
    void apply_clicked(); 
    void total_clicked(); 
    void read_in_clicked();

    Gtk::Button bowl_button, burrito_button, apply, read_in, total_button, exit;
    Gtk::VBox layout;

};

#endif