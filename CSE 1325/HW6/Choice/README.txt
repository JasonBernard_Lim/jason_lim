// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: chipotle.cpp, chipotle.h, choice.cpp, makefile
--------------------------------
Function Description:
  This program is a recreation of the chipotle program from HW3.

  This program allows the user to either be a customer, which allows the user to
  order either a bowl or a burrito and to choose what they want from each category
  of ingredients, or apply to be a delivery person. For every order a delivery
  person will send out the order. 

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    make
    ./choice


  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW6$ make
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW6$ ./choice

--------------------------------
Notes:
  - The program, during the ordering, does not check if the item is from the
    available or not.

  - To ask for a burrito or bowl, the program only accepts lowercase.

  - The read in only works in the format "First Name" "Last Name". Any other input will
    cause the program to work less than expected.

  - All movement through the program is through a GUI
