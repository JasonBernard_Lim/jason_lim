// Jason Bernard "JB" Lim
// ID: 1001640993
#include "chipotle.h"
#include <gtkmm/application.h>

//this is the main to the chipotle program
int main(int argc, char* argv[]) {
    Gtk::Main app(argc, argv);
    order_window w;
    Gtk::Main::run(w);
    return 0;
}