#include <iostream>

using namespace std;

class treasure_chest {

public:
  int num_pearls;
  int num_diamonds;
  int num_coins;

  float find_total(float p, float d, float c) {
    return (num_pearls * p) + (num_diamonds + d) + (num_coins * c);
  }

};

int main(int argc, char **argv) {
  cout <<"--Treasure Calculator--" << endl;

  float price_pearls, price_diamonds, price_coins;

  cout << "Enter the price of each pearl: ";
  cin >> price_pearls;

  cout << "Enter the price of each diamond: ";
  cin >> price_diamond;

  cout << "Enter the price of each coin: ";
  cin >> price_coins;

  treasure_chest first_found;
  treasure_chest second_found;

  first_found.num_pearls = 4;
  first_found.num_diamonds = 7;
  first_found.num_coins = 3;

  second_found.num_pearls = 5;
  second_found.num_diamonds = 2;
  second_found.num_coins = 4;

  float total_first = first_found.find_total(price_pearls, price_diamond, price_coins);
  float total_second = second_found.find_total(price_pearls, price_diamond, price_coins);

  cout << "-Total in treasure chest 1: $" << total_first << endl;
  cout << "-Total in treasure chest 2: $" << total_second << endl;
}
