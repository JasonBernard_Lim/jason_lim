#include <iostream>
#include <vector>
#include <string>

using namespace std;

class puppy {
  string name;
  string breed;
  int age_weeks;

public:
  bool shots;

  void give_info();
  void print_info();

};

void puppy::give_info() {

  cout << "Name: ";
  getline(cin, name);

  cout << "Breed: ";
  getline(cin, breed);
  cout << "Age in weeks: ";
  getline(cin, age_weeks);
}


void puppy::print_info() {
  cout << "--" << name << endl;
  cout << "Breed: " << breed << endl;
  cout << "Age weeks: " << age_weeks << endl;
}

void print_puppies(vector<puppy> animal_shelter) {
  cout << "**All puppies available:**\n" << endl;

  for(int i = 0; i < animal_shelter.size(); i++) {
    animal_shelter[i].print_info();

    if(animal_shelter[i].shots) {
      cout << "--Puppy had shots.\n\n";
    }
    else {
      cout << "--Puppy has not had shots.\n\n";
    }
  }
}

int main(int argc, char **argv) {
  vector<puppy> = animal_shelter;

  string answer = "";

  while(answer.compare("exit") != 0) {
    cout << "found or see?" << endl;

    if(answer.compare("found") == 0){
      puppy p;
      p.give_info();
      cout << "Vaccinate?" << endl;
      getline(cin, answer);

      if(answer.compare("yes") == 0) {
        p.shots = true;
      }
      else {
        p.shots = false;
      }

      animal_shelter.push_back(p);
    }
    else if(answer.compare("see") == 0) {
      if(animal_shelter.size == 0) {
        cout << "No animals yet!" << endl;
      }
      else {
        print_puppies(animal_shelter);
      }
    }
  }
}
