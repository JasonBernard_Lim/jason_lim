//Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool check_duplicate(vector <string> s, string word) {
  bool ret = false;
  for(int i = 0; i < s.size() && !ret; i++) {
    if(s[i] == word) {
      ret = true;
    }
  }

  return ret;
}

int main(int argc, char **argv) {
  int max_num;
  int i = 0;
  string name;
  vector <string> all_friends;

  cout << "Enter max number of friends for the group: " << endl;
  cin >> max_num;

  while(i < max_num) {
    cout << "Enter friend " << (i+1) << ":" << endl;
    cin >> name;

    if(!check_duplicate(all_friends, name)) {
      all_friends = push_back(name);
      i++;
    }
    else {
      cout <<"Sorry, there is already a friend with this name!" << endl;
    }
  }

  cout << "All friends added!" << endl;
}
