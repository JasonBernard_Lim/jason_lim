#include <iostream>
#include <string>

using namespace std;

class Monkey {
  int num_bananas;
  bool happy;

  void print_status() {
    if(happy) {
      cout << "MONKEY IS HAPPY with " << num_bananas << "banana(s)!" << endl;
    }
    else {
      cout << "NOT HAPPY" << endl;
    }
  }

public:
  Monkey(int banana) {
    num_bananas = banana;
    if(banana < 40) {
      happy = false;
    }
    else {
      happy = true;
    }
    print_status();
  }

  Monkey(int banana, string feelz) {
   num_bananas = banana;

   if(feelz == "happy") {
     happy = true;
   }
   else {
     happy = false;
   }
   print_status();
  }

  Monkey() {
    cout << "??? Monkey status and num of bananas currently unknown... ???" << endl;
  }
};


int main() {
  Monkey m1(370);
  Monkey m2(33, "bored");
  Monkey m3;
}
