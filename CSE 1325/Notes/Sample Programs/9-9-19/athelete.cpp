#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Athlete{
  std::string name;
  std::vector <int> time_seconds;

public:
  bool no_show;

  Athlete(std::string name, bool no_show) {
    this->name = name;
    this->no_show = no_show;
  }

  Athlete(std::string name) {
    this->name = name;
    this->no_show = true;
  }

  void log_time(){
    int answer;
    std::cout << "Enter new time (in seconds): " << std::endl;
    std::cin >> answer;

    time_seconds.push_back(answer);
    std::sort(time_seconds.begin(), time_seconds.end());
  }

  void check_time() {
    std::string answer;

    if(time_seconds.empty() || !no_show) {
      std::cout << "No times to show OR user has made times private." << std::endl;
    }
    else {
      std::cout << "Would you like to see the best time or worst time?" << std::endl;
      std::cin >> answer;
    }

    if(answer == "best") {
      std::cout << "Best times is: " << time_seconds.at(0) << std::endl;
    }
    else if(answer == "worst") {
      std::cout << "Worst time is: " <<time_seconds.at(time_seconds.size()-1) << std::endl;
    }
    else {
      std::cout << "Sure." << std::endl;
    }
  }

  std::string get_name() {
    return name;
  }
};

int main(int argc, char **argv) {

}
