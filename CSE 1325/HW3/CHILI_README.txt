// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: chili.cpp, chili.txt
--------------------------------
Function Description:
  This program allows the user (in this fictional case "Louise"), to sell her
  chili by reading in a file of orders. The program also allows her to take breaks
  between orders. If the chili is below a certain amount the program will tell.
  If there is not enough chili to complete an order the program asks if you want
  to make another batch of chili.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ chili.cpp
    ./a.out chili.txt

  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ g++ chili.cpp
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ ./a.out chili.txt
  Checking today's customers...done!

  --Welcome Louise--

  How many batches of your famous chili are you making today? 1


  .-Customer 1: Natassa's order is Grande
  Order served. Not much chili left.

  Continue with orders or take a break?
  continue

  .-Customer 2: Demy's order is Tall
  Order served. Not much chili left.

  Continue with orders or take a break?
  break
  Ok. Press enter to continue when you are finished with your break.


  .-Customer 3: Elena's order is Short
  Sorry, not enough chili. Would you like to make another batch or quit?
  make

  How many batches of your famous chili are you making today? 1


  .-Customer 3: Elena's order is Short
  Order served. Still got lots of chili!

  Continue with orders or take a break?
  continue

--------------------------------
Notes:
  - The read in for cup sizes only works for capitalized words (i.e "Venti", "Grande").

  - If you enter in a string for the amount of batches the program will fail without error
    thus causing the program to work less than expected.

  - The read in works from command line and if you want to read in the folder
    you need to make sure that the file is in the same directory as the program.
    The .txt file that comes with the program is chili.txt

  - The read in only works in the format "Name","Cup size". Any other input will
    cause the program to work less than expected

  - One batch of chili is equal to 4 cups

  - Venti = 2.5 cups, Grande = 2 cups, Tall = 1.5 cups, Short = 1 Cup. These are
    the only types supported with the program.

  - For the break_time() input is only suppported with lower case inputs. For the
    asking if you would like to make another batch, to make another batch you need
    to type in "make" or, if you want to quit type "quit".
