// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>

using namespace std;

class delivery {
public:
  vector<string> delivery_people;

  delivery(string filename) {//constructor for the delivery people
    //create the inFile object and read in all of the information
    string name;
    ifstream inFile;

    inFile.open(filename);

    if(!inFile.is_open()) {
      cout << "File not found!" << endl;
      exit(1);
    }

    while(!inFile.eof()) {
      string intermediate;
      string line_from_file;

      getline(inFile, line_from_file);

      stringstream delimt_line(line_from_file);

      getline(delimt_line, intermediate, ' ');
      delivery_people.push_back(intermediate);

      getline(delimt_line, intermediate, '\n');
    }
  }

  void apply(string name) {//this is to apply for a delivery position
    string first_name;
    stringstream ss(name);

    getline(ss, first_name, ' ');

    delivery_people.push_back(first_name);

    cout << "Newest delivery person: " << first_name << endl << endl;
  }
};

class burrito {

public:
  float price = 6.75;

  burrito() {//constructor for burrito almost the exact same as bowl except for price
    string answer;
    string protein;
    string rice;
    string other;
    bool check = true;

    cout << "Price will be $" << price << endl << endl;

    while(check) {
      //ask for protein and add it to the string
      cout << "-Pick: Tofu, Steak, Chicken" << endl;
      getline(cin, protein);

      if(protein.compare("none") == 0) {

      }
      else {
        answer = protein;
      }

      //do the same for rice and other ingredients
      cout << "\n-Pick: Cilantro-Lime Brown, Cilantro-Lime White" << endl;
      getline(cin, rice);

      if(rice.compare("none") == 0) {

      }
      else {
        if(answer.compare("") == 0) {
          answer = rice;
        }
        else {
          answer = answer + ", " + rice;
        }
      }

      cout << "\n-Pick: Queso, Sour Cream, Fresh Tomato Salsa" << endl;
      getline(cin, other);

      if(other.compare("none") == 0) {

      }
      else {
        if(answer.compare("") == 0) {
          answer = other;
        }
        else {
          answer = answer + ", " + other;
        }
      }

      //ask if the order is correct if not, repeat the process
      string choice;
      cout << "\nConfirm order (yes or no):" << endl;
      cout << "Burrito: " << answer << endl;
      getline(cin, choice);

      if(choice.compare("yes") == 0|| choice.compare("Yes") == 0) {
        check = false;
      }

    }
  }
};

class bowl {

public:
  float price = 6.50;

  bowl() {//constructor for burrito almost the exact same as bowl except for price
          //check burrito for more detailed comments
    string answer;
    string protein;
    string rice;
    string other;
    bool check = true;

    cout << "Price will be $" << price << endl << endl;

    while(check) {
      cout << "-Pick: Tofu, Steak, Chicken" << endl;
      getline(cin, protein);

      if(protein.compare("none") == 0) {

      }
      else {
        answer = protein;
      }

      cout << "\n-Pick: Cilantro-Lime Brown, Cilantro-Lime White" << endl;
      getline(cin, rice);

      if(rice.compare("none") == 0) {

      }
      else {
        if(answer.compare("") == 0) {
          answer = rice;
        }
        else {
          answer = answer + ", " + rice;
        }
      }

      cout << "\n-Pick: Queso, Sour Cream, Fresh Tomato Salsa" << endl;
      getline(cin, other);

      if(other.compare("none") == 0) {

      }
      else {
        if(answer.compare("") == 0) {
          answer = other;
        }
        else {
          answer = answer + ", " + other;
        }
      }

      string choice;
      cout << "\nConfirm order (yes or no):" << endl;
      cout << "Bowl: " << answer << endl;
      getline(cin, choice);

      if(choice.compare("yes") == 0|| choice.compare("Yes") == 0) {
        check = false;
      }

    }
  }
};

int main(int argc, char **argv) {
  string choice;
  delivery list(argv[1]);
  float total = 0;
  int counter = 0;

  cout << "~~Ronnie's Delivery Service~~" << endl << endl;

  bool check = true;
  while(check) {//ask the user which option and do the appropriate operation
    cout << "--------------------------------------" << endl;
    cout << "Pick from the following menu:\n1. Customer\n2. Apply\n3. Exit" << endl;
    getline(cin, choice);

    if(choice.compare("1") == 0) {//ask for either a burrito or bowl
      cout << "\n\n***Place your order***\nBurrito or Bowl?" << endl;
      getline(cin, choice);

      if(choice.compare("burrito") == 0) {
        burrito burrito;
        total = total + burrito.price;

        if(counter >= list.delivery_people.size()) {
          counter = 0;
        }

        cout << "\n\nOk, " << list.delivery_people[counter] << " will be delivering your order. Thank you." << endl;
        counter++;
      }
      else if(choice.compare("bowl") == 0) {
        bowl bowl;
        total = total + bowl.price;

        if(counter >= list.delivery_people.size()) {
          counter = 0;
        }

        cout << "\n\nOk, " << list.delivery_people[counter] << " will be delivering your order. Thank you." << endl;
        counter++;
      }
    }
    else if(choice.compare("2") == 0) {//ask for the full name for the application
      cout << "Enter your full name: ";
      getline(cin, choice);
      list.apply(choice);
    }
    else if(choice.compare("3") == 0) {//output the total amount to a file
      cout << "Exiting...\nTotal made: $" << total << endl;

      ofstream outFile;
      outFile.open("total.txt");

      if(!outFile.is_open()) {
        cout << "Output File not found!" << endl;
        exit(1);
      }

      outFile << "Total made: $" << total << endl;

      outFile.close();

      check = false;
    }
  }
}
