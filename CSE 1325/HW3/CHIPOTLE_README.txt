// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: chipotle.cpp, chipotle.txt, total.txt
--------------------------------
Function Description:
  This program allows the user to either be a customer, which allows the user to
  order either a bowl or a burrito and to choose what they want from each category
  of ingredients, or apply to be a delivery person. For every order a delivery
  person will send out the order. After you exit the total amount will be shown
  and output to a file called total.txt.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ chipotle.cpp
    ./a.out chipotle.txt

  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ g++ chipotle.cpp
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ ./a.out chipotle.txt
  ~~Ronnie's Delivery Service~~

  --------------------------------------
  Pick from the following menu:
  1. Customer
  2. Apply
  3. Exit
  1


  ***Place your order***
  Burrito or Bowl?
  burrito
  Price will be $6.75

  -Pick: Tofu, Steak, Chicken
  Tofu, Chicken

  -Pick: Cilantro-Lime Brown, Cilantro-Lime White
  none

  -Pick: Queso, Sour Cream, Fresh Tomato Salsa
  Queso

  Confirm order (yes or no):
  Burrito: Tofu, Chicken, Queso
  yes


  Ok, Serj will be delivering your order. Thank you.
  --------------------------------------
  Pick from the following menu:
  1. Customer
  2. Apply
  3. Exit
  1


  ***Place your order***
  Burrito or Bowl?
  bowl
  Price will be $6.5

  -Pick: Tofu, Steak, Chicken
  Steak

  -Pick: Cilantro-Lime Brown, Cilantro-Lime White
  none

  -Pick: Queso, Sour Cream, Fresh Tomato Salsa
  none

  Confirm order (yes or no):
  Bowl: Steak
  yes


  Ok, Daron will be delivering your order. Thank you.
  --------------------------------------
  Pick from the following menu:
  1. Customer
  2. Apply
  3. Exit
  2
  Enter your full name: Jason Lim
  Newest delivery person: Jason

  --------------------------------------
  Pick from the following menu:
  1. Customer
  2. Apply
  3. Exit
  3
  Exiting...
  Total made: $13.25


--------------------------------
Notes:
  - The program, during the ordering, does not check if the item is from the
    available or not.

  - To ask for a burrito or bowl, the program only accepts lowercase.

  - The read in works from command line and if you want to read in the folder
    you need to make sure that the file is in the same directory as the program.
    The .txt file that comes with the program is chipotle.txt.

  - The read in only works in the format "First Name" "Last Name". Any other input will
    cause the program to work less than expected.

  - The program only outputs the total amount to total.txt.
