// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>

using namespace std;

class orders {
public:
  vector<string> cup_type;
  vector<string> names;

  orders(string filename) {//constructor for orders
    //create the inFile object
    string name;
    ifstream inFile;

    inFile.open(filename);

    if(!inFile.is_open()) {//check if the file exists or not
      cout << "Checking today's customers... No File Found!" << endl;
      exit(1);
    }
    while(!inFile.eof()) {//while the file has not ended read in the imformation
      string intermediate;
      string line_from_file;
      int size = 0;

      getline(inFile, line_from_file);

      stringstream delimt_line(line_from_file);

      getline(delimt_line, intermediate, ',');
      names.push_back(intermediate);

      getline(delimt_line, intermediate, '\n');
      cup_type.push_back(intermediate);
    }
    cout << "Checking today's customers...done!" << endl;
  }
};

class Louise {
public:
  float cups_of_chili = 0;

  Louise() {
    cout << "\n--Welcome Louise--" << endl;

    //ask for the original amount of chili
    cout << "\nHow many batches of your famous chili are you making today? ";
    cin >> cups_of_chili;
    cups_of_chili = cups_of_chili * 4;
    cout << endl;
  }

  float another_batch() {//function for adding another batch of chili
    float another_batch = 0;

    cout << "\nHow many batches of your famous chili are you making today? ";
    cin >> another_batch;
    another_batch = another_batch * 4;
    cout << endl;

    return another_batch;
  }

  void break_time() {//function for taking a break
    string answer;
    bool check = true;
    while(check) {
      cout << "\nContinue with orders or take a break?" << endl;
      cin >> answer;

      if(answer.compare("break") == 0) {
        cout << "Ok. Press enter to continue when you are finished with your break.\n";
        cin.get();
        cin.get();
        check = false;
      }
      else if(answer.compare("continue") == 0) {
        check = false;
      }
    }
  }
};

int main(int argc, char** argv) {
  orders orders(argv[1]); //create the information for the orders

  Louise Louise; //create and initialize the amount of chili

  for(int i = 0; i < orders.names.size()-1; i++) {
    //check for the amount of cups of chili needed
    float size = 0;

    if(orders.cup_type[i].compare("Venti") == 0) {
      size = 2.5;
    }
    else if(orders.cup_type[i].compare("Grande") == 0) {
      size = 2;
    }
    else if(orders.cup_type[i].compare("Tall") == 0) {
      size = 1.5;
    }
    else if(orders.cup_type[i].compare("Short") == 0) {
      size = 1;
    }

    //go through the orders
    cout << "\n--Customer " << (i+1) << ": " << orders.names[i] << "'s order is " << orders.cup_type[i] << "." << endl;

    if(Louise.cups_of_chili >= size) { //if amount of chili is enough
      if((Louise.cups_of_chili - size) > (Louise.cups_of_chili / 2)) {//if the chili after the order is greater than half of the original amount
        cout << "Order served. Still got lots of chili!" << endl;
      }
      else {
        cout << "Order served. Not much chili left." << endl;
      }

      Louise.cups_of_chili = Louise.cups_of_chili - size; //subtract the chili
      Louise.break_time(); //ask for a break
    }
    else { //if current chili is not enough
      string answer;
      cout << "Sorry, not enough chili. Would you like to make another batch or quit?" << endl;
      cin >> answer;

      bool check = true;
      while(check) {
        if(answer.compare("make") == 0) {//ask to make another batch
          Louise.cups_of_chili = Louise.cups_of_chili + Louise.another_batch();
          check = false;
          i--;
        }
        else if(answer.compare("quit") == 0) {//quit outright
          exit(0);
        }
      }
    }
  }
}
