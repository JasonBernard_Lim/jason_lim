#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <gtkmm.h>

//init the textbox_window class
class Textbox_window : public Gtk::Window {
    public:
    Textbox_window();
    virtual ~Textbox_window();

    protected:
    void convert_to_lbs();
    void convert_to_kg();

    Gtk::Label name_label, weight_label;
    Gtk::Entry name_entry;
    Gtk::Entry weight_entry;
    Gtk::Button exit, kg_to_lbs, lbs_to_kg;
    Gtk::VBox box;
};

#endif