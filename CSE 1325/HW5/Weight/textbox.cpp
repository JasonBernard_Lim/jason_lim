#include "textbox.h"
#include <iostream>

using namespace std;

//construct the textwindow
Textbox_window::Textbox_window() {
    resize(500,200);
    set_title("Weight Converter");
    //add the labels and the entries
    name_label.set_text("Enter name:");
    box.pack_start(name_label);
    name_entry.set_max_length(50);
    name_entry.set_text("Enter name");
    name_entry.select_region(0, name_entry.get_text_length());
    box.pack_start(name_entry);

    weight_label.set_text("Enter weight");
    box.pack_start(weight_label);
    weight_entry.set_max_length(50);
    weight_entry.set_text("Enter weight");
    weight_entry.select_region(0, weight_entry.get_text_length());
    box.pack_start(weight_entry);

    kg_to_lbs.add_label("Convert to lbs");
    kg_to_lbs.signal_pressed().connect(sigc::mem_fun(*this, &Textbox_window::convert_to_lbs));
    box.pack_start(kg_to_lbs);

    lbs_to_kg.add_label("Convert to kg");
    lbs_to_kg.signal_pressed().connect(sigc::mem_fun(*this, &Textbox_window::convert_to_kg));
    box.pack_start(lbs_to_kg);

    exit.add_label("Exit");
    exit.signal_pressed().connect(sigc::mem_fun(*this, &Textbox_window::close));
    box.pack_start(exit);
    //add the box
    box.show_all();
    add(box);

}

Textbox_window::~Textbox_window() {

}

//create the signal functions
void Textbox_window::convert_to_lbs() {
    string name = name_entry.get_text();
    string weight = weight_entry.get_text();

    float weight_float = stof(weight);
    weight_float = weight_float * 2.2;

    weight = to_string(weight_float);

    string input = "Hi " + name + "- you weigh " + weight + " pounds.";

    Gtk::MessageDialog dialog(*this, "Weight Conversion to lbs:", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text(input);
    dialog.run();
}

void Textbox_window::convert_to_kg() {
    string name = name_entry.get_text();
    string weight = weight_entry.get_text();

    float weight_float = stof(weight);
    weight_float = weight_float / 2.2;

    weight = to_string(weight_float);

    string input = "Hi " + name + "- you weigh " + weight + " kilos.";

    Gtk::MessageDialog dialog(*this, "Weight Conversion to kilos:", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text(input);
    dialog.run();
}