// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: Makefile, weight_main.cpp, textbox.h, textbox.cpp
--------------------------------
Function Description:
  This program is a recreation of the weight program and allows the user to enter his name
  and weight and converts the weight from pounds to kilos, or from kilos to pounds

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    make
    ./weight


  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW5$ make
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ ./weight
  
--------------------------------
Notes:
    - When you type in your weight make sure to type in the number opposite to what you want to convert.
      (i.e if you want lbs type in the amount in kilos and vice versa)
