#include "textbox.h"
#include <gtkmm/application.h>

//initialize the window and run the program
int main(int argc, char* argv[]) {
    Gtk::Main app(argc, argv);
    Textbox_window w;
    Gtk::Main::run(w);
    return 0;
}