// Jason Bernard "JB" Lim
// ID: 1001640993
#include "shapes.h"
#include <gtkmm/application.h>

//this is the main to the shape program
int main(int argc, char* argv[]) {
    Gtk::Main app(argc, argv);
    Shapes_window w;
    Gtk::Main::run(w);
    return 0;
}