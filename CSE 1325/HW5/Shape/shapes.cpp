// Jason Bernard "JB" Lim
// ID: 1001640993
#include "shapes.h"
#include <iostream>

Shapes_window::Shapes_window() {
    //create the shapes window
    resize(400,200);

    //add the specific buttons to the shapes
    square.add_label("Square");
    square.signal_pressed().connect(sigc::mem_fun(*this, &Shapes_window::square_info));
    layout.pack_start(square);

    triangle.add_label("Triangle");
    triangle.signal_pressed().connect(sigc::mem_fun(*this, &Shapes_window::triangle_info));
    layout.pack_start(triangle);

    circle.add_label("Circle");
    circle.signal_pressed().connect(sigc::mem_fun(*this, &Shapes_window::circle_info));
    layout.pack_start(circle);

    exit.add_label("Exit");
    exit.signal_pressed().connect(sigc::mem_fun(*this, &Shapes_window::close));
    layout.pack_start(exit);

    layout.show_all();
    add(layout);
}

Shapes_window::~Shapes_window() {

}

//create the signal functions for the buttons
void Shapes_window::square_info() {
    Gtk::MessageDialog dialog(*this, "Square Info", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text("area = length * length");
    dialog.run();
}

void Shapes_window::triangle_info() {
    Gtk::MessageDialog dialog(*this, "Triangle Info", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text("area = (base * height) / 2");
    dialog.run();
}

void Shapes_window::circle_info() {
    Gtk::MessageDialog dialog(*this, "Circle Info", false, Gtk::MESSAGE_INFO);
    dialog.set_secondary_text("area = pi * r^2 (r-squared)");
    dialog.run();    
}