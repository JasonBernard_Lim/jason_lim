// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: makefile, shape_main.cpp, shapes.h, shapes.cpp
--------------------------------
Function Description:
  This program is a recreation of the shapes program and allows the user to select from
  a GUI information about the specific shapes.

--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    make
    ./shapes


  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW5$ make
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW3$ ./shapes
  
--------------------------------
Notes:
