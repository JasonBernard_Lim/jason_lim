// Jason Bernard "JB" Lim
// ID: 1001640993
#ifndef SHAPES_H
#define SHAPES_H

#include <gtkmm.h>

//define the window class
class Shapes_window : public Gtk::Window {
    public:
    Shapes_window();
    virtual ~Shapes_window();

    protected:
    void        square_info();
    void        triangle_info();
    void        circle_info();
    Gtk::Button     square;
    Gtk::Button     triangle;
    Gtk::Button     circle;
    Gtk::Button     exit;
    Gtk::VBox       layout;  
};

#endif