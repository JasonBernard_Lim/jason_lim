#include "money.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

office::office() {
    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Enter info...");

    Gtk::Label *office_label = new Gtk::Label("Enter name of exchange office:");
    dialog->get_content_area()->pack_start(*office_label);
    office_label->show();

    dialog->add_button("Cancel", 0);
    dialog->add_button("Next",1);

    Gtk::Entry *entry_office = new Gtk::Entry();
    entry_office->set_text("default_text");
    entry_office->set_max_length(70);
    entry_office->show();
    dialog->get_vbox()->pack_start(*entry_office);

    Gtk::Label *manager_label = new Gtk::Label("Enter name of manager:");
    dialog->get_content_area()->pack_start(*manager_label);
    manager_label->show();

    Gtk::Entry *entry_manager = new Gtk::Entry();
    entry_manager->set_text("default_text");
    entry_manager->set_max_length(50);
    entry_manager->show()
    dialog->get_vbox()->pack_start(*entry_manager);

    int result = dialog->run();

    if(result==1) {
        name = entry_office->get_text();
        manager = entry_manager->get_text();        
    }
    else {
        name = "CANCELED";
        manager = "CANCELED";
    }

    dialog->close();

    delete dialog;
    delete label;
    delete entry_manager;
    delete entry_office;
}

conversions::conversions(office o1, office o2) {
    this->o1 = &o1;
    this->o2 = &o2;
    set_title("Where are you in the airport?");
    set_size_request(500,200);
    add(vbox);

    north.set_label("North");
    north.signal_pressed().connect(sigc::mem_fun(*this,&conversion_box::abc_conversions));
    vbox.pack_start(north);

    east.set_label("East");
    east.signal_pressed().connect(sigc::mem_fun(*this,&conversion_box::abc_conversions));
    vbox.pack_start(east);

    south.set_label("South");
    south.signal_pressed().connect(sigc::mem_fun(*this,&conversion_box::def_conversions));
    vbox.pack_start(south);

    west.set_label("West");
    west.signal_pressed().connect(sigc::mem_fun(*this,&conversion_box::def_conversions));
    vbox.pack_start(west);    
}

conversions::~conversions() {

}

void conversions::abc_conversions(office o1, office o2) {
    string name;
    string manager;
    string type;
    float rate;

    if(o1.name.compare("ABC Conversions") == 0) {
        name = o1.name;
        manager = o1.manager;
        type = o1.type;
        rate = o1.rate;
    }
    else {
        name = o2.name;
        manager = o2.manager;
        type = o2.type;
        rate = o2.rate;
    }

    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Welcome to ABC Conversions");   

    Gtk::Label *info_label = new Gtk::Label("Please contact the manager if" + manager + "if you have any complaints.");
    dialog->get_content_area()->pack_start(*info_label);
    info_label->show();

    Gtk::Label *conversion_label = new Gtk::Label("What currency are you converting to dollars and how much?");
    dialog->get_content_area()->pack_start(*conversion_label);
    conversion_label->show();    

    dialog->add_button("Cancel", 0);
    dialog->add_button("Convert",1);

    Gtk::Entry *entry_amount = new Gtk::Entry();
    entry_amount->set_text("default_text");
    entry_amount->set_max_length(70);
    entry_amount->show();
    dialog->get_vbox()->pack_start(*entry_amount);

    int result = dialog->run();

    if(result == 0) {
        hide();
    }     
    else {
        string answer = entry_amount.get_text();
        stringstream ss(answer);
        float user_amount;
        string user_type;

        ss >> user_amount >> user_type;

        if(user_type.compare(type) == 0) {
            user_amount = user_amount / rate;

            string amount = to_string(user_amount);

            string input = "$" + amount;

            Gtk::MessageDialog message(*this, "Here you go:", false, Gtk::MESSAGE_INFO);
            message.set_secondary_text(input);
            message.run();
        }
        else {
            string input = "Sorry we do not convert the " + user_type + " currency.";
            Gtk::MessageDialog message(*this, "Here you go:", false, Gtk::MESSAGE_INFO);
            message.set_secondary_text(input);
            message.run();
        }
    }

    delete dialog;
    delete info_label;
    delete conversion_label;
    delete entry_amount;
}

void conversions::def_conversions(office o1, office o2) {
    string name;
    string manager;
    string type;
    float rate;

    if(o1.name.compare("DEF Conversions") == 0) {
        name = o1.name;
        manager = o1.manager;
        type = o1.type;
        rate = o1.rate;
    }
    else {
        name = o2.name;
        manager = o2.manager;
        type = o2.type;
        rate = o2.rate;
    }

    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Welcome to ABC Conversions");   

    Gtk::Label *info_label = new Gtk::Label("Please contact the manager if" + manager + "if you have any complaints.");
    dialog->get_content_area()->pack_start(*info_label);
    info_label->show();

    Gtk::Label *conversion_label = new Gtk::Label("What currency are you converting to dollars and how much?");
    dialog->get_content_area()->pack_start(*conversion_label);
    conversion_label->show();    

    dialog->add_button("Cancel", 0);
    dialog->add_button("Convert",1);

    Gtk::Entry *entry_amount = new Gtk::Entry();
    entry_amount->set_text("default_text");
    entry_amount->set_max_length(70);
    entry_amount->show();
    dialog->get_vbox()->pack_start(*entry_amount);

    int result = dialog->run();

    if(result == 0) {
        hide();
    }     
    else {
        string answer = entry_amount.get_text();
        stringstream ss(answer);
        float user_amount;
        string user_type;

        ss >> user_amount >> user_type;

        if(user_type.compare(type) == 0) {
            user_amount = user_amount / rate;

            string amount = to_string(user_amount);

            string input = "$" + amount;

            Gtk::MessageDialog message(*this, "Here you go:", false, Gtk::MESSAGE_INFO);
            message.set_secondary_text(input);
            message.run();
        }
        else {
            string input = "Sorry we do not convert the " + user_type + " currency.";
            Gtk::MessageDialog message(*this, "Here you go:", false, Gtk::MESSAGE_INFO);
            message.set_secondary_text(input);
            message.run();
        }
    }
    delete dialog;
    delete info_label;
    delete conversion_label;
    delete entry_amount;
}