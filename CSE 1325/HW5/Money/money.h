#ifndef MONEY_H
#define MONEY_H

#include <gtkmm.h>

using namespace std;

class office {
    public:
    string name;
    string manager;
    string type;
    float rate;

    office();
};

class conversions {
    public:
    office o1;
    office o2;
    conversions(office o1, office o2);
    virtual ~conversions();

    Gtk::Button north, south, east, west, exit;
    Gtk::VBox vbox;

    protected:
    void abc_conversions(office o1, office o2);
    void def_conversions(office o1, office o2);
};

#endif