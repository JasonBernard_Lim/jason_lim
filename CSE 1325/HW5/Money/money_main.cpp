#include "money.h"
#include <gtkmm/application.h>

int main(int argc, char* argv[]) {
    Gtk::Main app(argc, argv);
    office o1;
    if(o1.name.compare("ABC Conversions") == 0) {
        o1.type = "yen";
        o1.rate = 108.40; 
    }
    else {
        o1.type = "peso";
        o1.rate = 19.41;
    }
    if(o1.name.compare("CANCELED") == 0) {
        return 0;
    }
    office o2;
    if(o2.name.compare("ABC Conversions") == 0) {
        o2.type = "yen";
        o2.rate = 108.40; 
    }
    else {
        o2.type = "peso";
        o2.rate = 19.41;
    }    
    if(o2.name.compare("CANCELED") == 0) {
        return 0;
    }

    conversions w(o1, o2);

    Gtk::Main::run(w);

    return 0;
}