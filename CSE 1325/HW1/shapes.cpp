// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

//create the namespaces for the shapes used
namespace square {
  string area_formula = "area = length * length";
}

namespace triangle {
  string area_formula = "area = (base * height) / 2";
}

namespace circle {
  string area_formula = "area = pi * r^2 (r-squared)";
}

//create a function to print out the info
//as well as trying to figure out which shape the user want
int print_out(int num, string word) {
  if(word.compare("Square") == 0 || word.compare("square") == 0) {
    cout << square::area_formula << endl << endl;
    num = num + 1;
  }
  else if(word.compare("Triangle") == 0 || word.compare("triangle") == 0) {
    cout << triangle::area_formula << endl << endl;
    num = num + 1;
  }
  else if(word.compare("Circle") == 0 || word.compare("circle") == 0) {
    cout << circle::area_formula << endl << endl;
    num = num + 1;
  }
  else {
    cout << "Sorry, information for this shape is not available." << endl << endl;
    num = num + 1;
  }

  return num;
}

int main(int argc, char **argv) {
  cout << "**********" << endl << "Shapes!" << endl << "**********" << endl << endl;

  bool ret = true;
  int i = 0;
  string shape;

  //use in a while loop
  while(ret) {
    cout << (i+1) << ". Shape: ";
    cin >> shape;

    if(shape.compare("exit") == 0 || shape.compare("Exit") == 0) {
      cout << endl << "-" << i << " shapes entered." << endl << "Exiting..." << endl;
      ret = false;
    }
    else {
      i = print_out(i, shape);
    }

  }
}
