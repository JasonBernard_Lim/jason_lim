// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: shapes.cpp
--------------------------------
Function Description:
  This function allows the user to enter in a shape and the program will output
  information based on which shape they want. Shape info is limited.
--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ shapes.cpp
    ./a.out

  Sample run:

  student@cse1325:/media/sf_jason_lim/CSE 1325/HW1$ g++ shapes.cpp
  student@cse1325:/media/sf_jason_lim/CSE 1325/HW1$ ./a.out
  **********
  Shapes!
  **********

  1. Shape: square
  area = length * length

  2. Shape: circle
  area = pi * r^2 (r-squared)

  3. Shape: diamond
  Sorry, information for this shape is not available.

  4. Shape: exit

  -3 shapes entered.
  Exiting...

--------------------------------
Notes:
  Only the shapes square, triangle, and circle are included in this program.
