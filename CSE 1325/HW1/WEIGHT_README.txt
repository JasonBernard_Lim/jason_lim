// Jason Bernard "JB" Lim
// ID: 1001640993

--------------------------------------README------------------------------------
Files Included: weight.cpp
--------------------------------
Function Description:
  This program takes a user's weight (in kilograms or pounds) and converts it to
  the opposite (kg to lbs or lbs to kg).
  The input should be in the form of:
    Bob 150 pounds
    Bob 70 kilos

  Any other form of input will cause the program to not fully function and give
  an error.
--------------------------------
Compilation Instructions:
  This program is compiled in a Lubuntu virtual machine
  In terminal:
    g++ weight.cpp
    ./a.out

  Sample run:

    student@cse1325:/media/sf_jason_lim/CSE 1325/HW1$ g++ weight.cpp
    student@cse1325:/media/sf_jason_lim/CSE 1325/HW1$ ./a.out
    Please enter your name and weight: JB 170 pounds
    Hi JB- you weigh 77.2727 kilos.
    Please enter your name and weight: Jason 75 kilos
    Hi Jason- you weigh 165 pounds.
    Please enter your name and weight: exit
    Exiting...

--------------------------------
Notes:
