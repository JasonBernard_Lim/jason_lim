// Jason Bernard "JB" Lim
// ID: 1001640993

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

float lbs_to_kg(float w) {//this function takes the weight from pounds and converts it to kilos
  float weight = w / 2.2;
  return weight;
}

float  kg_to_lbs(float w) {//this function takes the weight from kilos and converts it to pounds
  float weight = w * 2.2;
  return weight;
}

int main(int argc, char **argv) {
  //use a while loop
  bool ret = true;
  while(ret) {
    //create the string for the input
    string info;

    //ask the user for their name or weight
    cout << "Please enter your name and weight: ";
    getline(cin, info); //use getline to get the entire line

    //check for exit
    if(info.compare("exit") == 0 || info.compare("Exit") == 0) {
      ret = false;
      cout << "Exiting..." << endl;
      break;
    }
    //use string stream in orfer to split the information
    stringstream ss(info);

    //create the three variables for the different information
    string name;
    string type;
    float weight = 0;

    //split up the string stream
    ss >> name >> weight >> type;

    //check for weight type then use the corresponding convert function
    if(type.compare("pounds") == 0 || type.compare("Pounds") == 0) {
      weight = lbs_to_kg(weight);
      cout << "Hi " << name << "- you weigh " << weight << " kilos." << endl;
    }
    else if(type.compare("kilos") == 0 || type.compare("Kilos") == 0) {
      weight = kg_to_lbs(weight);
      cout << "Hi " << name << "- you weigh " << weight << " pounds." << endl;
    }
    else {
      cout << "Not enough info to convert." << endl;
    }
  }
}