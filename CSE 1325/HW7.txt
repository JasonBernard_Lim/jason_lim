// Jason Bernard "JB" Lim
// ID: 1001640993

Multiple Choice:
1. D
2. A
3. B
4. C
5. C
6. C
7. A
8. A
9. B
10. C
11. B
12. B
13. D
14. A
15. B
16. A
17. A
18. A
19. D
20. C
21. B
22. D
23. A
24. B
25. C
26. D

Free Response:
1. 
#include <iostream>
#include <ifstream>
#include <vector>
#include <sstream>
#include <ofstream>

using namespace std;

void read_in(string filename) {
    ifstream inFile;
    inFile.open(filename);

    if(!inFile.is_open()) {
        cout << "File not Found!" << endl;
    }

    vector<string> names;

    while(!inFile.eof()) {
        string line;
        string intermediate;

        getline(inFile, line);

        stringstream delimit(line);

        getline(delimit,intermediate, ' ');
        names.push_back(intermediate);
    }

    for(int i = 0; i < names.size(); i++) {
        cout << names.at(i) << endl;
    }

    sort(names.begin(), names.end());

    for(int i = 0; i < names.size(); i++) {
        cout << names.at(i) << endl;
    }

    ofstream outFile
    outFile.open("output.txt");

    for(int i = 0; i < names.size(); i++) {
        outFile << names.at(i) << endl;
    }
}

2.

Color(int red, int green, int blue) {
    this->_red = red;
    this->_green = green;
    this->_blue = blue;
}

Color operator+(const Color& rhs) {
    Color temp(0,0,0);

    temp._red = this->_red + rhs._red;
    temp._blue = this->_blue + rhs._blue;
    temp._green = this->_green + rhs._green;

    return temp;
}

friend std::ostream& operator<<(std::ostream& ost, const Color& c) {
    cout << "(" << (c._red/2) << "," << (c._green/2) << "," << (c._blue/2) << ")"; 
}

3. 
#include <iostream>
#include <vector>

class Fruit {
    public:
        virtual void whoamI();
};

class Orange : virtual public Fruit {
    public:
        void whoamI() {
            std::cout << "Orange";
        }
};

class Grapefruit: virtual public Fruit {
    public:
        void whoamI() {
            std::cout << "Grapefruit";
        }
};

class Orangelo : public Orange, public Grapefruit {
    public:
        void whoamI() {
            std::cout << "Grapefruit";
        }
};

int main(void) {
    vector<Fruit*> Basket;
    
    Fruit *f = new Orange;
    Basket.push_back(f);
    f = new Grapefruit;
    Basket.push_back(f);
    f = new Orangelo;
    Basket.push_back(f);

    for(int i = 0; i < F.size(); i++) {
        Basket.at(i)->whoamI();
    }
}

4. 
#include <gtkmm.h>
#include <iostream>

int main(int argc, char *argv[]) {
    Gtk::Main kit(argc, argv);

    Gtk::Window w;
    Gtk::Dialog *dialog = new Gtk::Dialog();
    dialog->set_transient_for(w);
    dialog->set_title("Make Money Fast");

    Gtk::Label label = new Gtk::Label("Enter email");
    dialog->get_content_area()->pack_start(*label);

    Gtk::Entry *entry = new Gtk::Entry();
    entry->set_max_length(50);
    dialog->get_content_area()->pack_start(*entry);

    dialog->add_button("Yes, I'm Gullible!", 1);

    label->show();
    entry->show();

    int result = dialog->run();

    string email;

    if(result == 1) {
        email = entry->get_text();
    }

    dialog->close();
    while(Gtk::Main::events_pending()) Gtk::Main::iteration();

    if(result == 1) {
        cout << email << endl;
    }
    else {

    }
}

5.
int main() {
    std::thread t1(some_function, "hello");
    std::thread t2(some_function, "there");

    t1.join();
    t2.join();
    return 0;
}

6. 
template <class T>
T maximum(T first, T second, T third) {
    T retval;
    retval = first>second? first : second;
    retval = retval>third? retval : third;
    return retval;
}
