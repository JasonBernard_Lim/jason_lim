/******************************************************************************
* @file jel0993_p1.s
* @author Jason Bernard Lim
******************************************************************************/
#include <stdio.h>

extern unsigned int bit_reverse(unsigned int num);
extern unsigned int convEndian(unsigned int num);

void binaryPrint(unsigned num) {
        unsigned i; 
    for (i = 1 << 31; i > 0; i = i / 2) 
        (num & i)? printf("1"): printf("0");          
}

int main()  {
    //store the unsigned numbers
    unsigned int array_a[3];
    scanf("%X", &array_a[0]);
    scanf("%X", &array_a[1]);
    scanf("%X", &array_a[2]);

    //print out the values in the array
    printf("array_a[0]\t=\t");
    binaryPrint(array_a[0]);
    printf("\t%u\t%X\n", array_a[0], array_a[0]);

    printf("array_a[1]\t=\t");
    binaryPrint(array_a[1]);
    printf("\t%u\t%X\n", array_a[1], array_a[1]);

    printf("array_a[2]\t=\t");
    binaryPrint(array_a[2]);
    printf("\t%u\t%X\n", array_a[2], array_a[2]);


    //reverse the bit and print out values
    printf("BIT REVERSE ORDER\n");

    array_a[0] = bit_reverse(array_a[0]);
    array_a[1] = bit_reverse(array_a[1]);
    array_a[2] = bit_reverse(array_a[2]);

    printf("array_a[0]\t=\t");
    binaryPrint(array_a[0]);
    printf("\t%u\t%X\n", array_a[0], array_a[0]);

    printf("array_a[1]\t=\t");
    binaryPrint(array_a[1]);
    printf("\t%u\t%X\n", array_a[1], array_a[1]);

    printf("array_a[2]\t=\t");
    binaryPrint(array_a[2]);
    printf("\t%u\t%X\n", array_a[2], array_a[2]);

    //re-reverse in order to do the endian conversion
    array_a[0] = bit_reverse(array_a[0]);
    array_a[1] = bit_reverse(array_a[1]);
    array_a[2] = bit_reverse(array_a[2]);


    //perform the endian conversion and print out the values
    printf("Endian Conversion\n");

    array_a[0] = convEndian(array_a[0]);
    array_a[1] = convEndian(array_a[1]);
    array_a[2] = convEndian(array_a[2]);

    printf("array_a[0]\t=\t");
    binaryPrint(array_a[0]);
    printf("\t%u\t%X\n", array_a[0], array_a[0]);

    printf("array_a[1]\t=\t");
    binaryPrint(array_a[1]);
    printf("\t%u\t%X\n", array_a[1], array_a[1]);

    printf("array_a[2]\t=\t");
    binaryPrint(array_a[2]);
    printf("\t%u\t%X\n", array_a[2], array_a[2]);

}
