/******************************************************************************
* @file jel0993_pfinal.s
* @author Jason Bernard Lim
******************************************************************************/

.global bit_reverse
.func bit_reverse

bit_reverse:
    MOV R2, R0 
    MOV R3, #31 
    LSR R0, R0, #1 
loop:
    LSL R2, R2, #1
    AND R4, R0, #1
    ORR R2, R2, R4
    LSR R0, R0, #1
    SUB R3, R3, #1
    CMP R3, #0
    BNE loop
    LSL R2, R2, R3
    MOV R0, R2
    BX LR

.endfunc

.global convEndian
.func convEndian

convEndian:
    REV R0, R0
    BX LR

.endfunc
