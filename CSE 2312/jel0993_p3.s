/******************************************************************************
* @file jel0993_p3.s
* @author Jason Bernard Lim
* @ID: 1001640993
******************************************************************************/
 
.global main
.func main
   
main:
    BL  _scanf              @ branch to scan procedure with return
    MOV R8, R0              @ store n in R8
    MOV R1, R0              @ pass n to partition procedure
    BL _scanf
    MOV R9, R0              @ store m in R9
    MOV R5, R0              @ pass m to partition procedure
    BL  _count_partitions    
    MOV R1, R0              @ pass result to printf procedure
    MOV R2, R8              @ pass n to printf procedure
    MOV R3, R9              @ pass m to printf procedure
    BL  _printf            
    B   main                             
       
_printf:
    PUSH {LR}              
    LDR R0, =printf_str
    MOV R1, R1
    MOV R2, R2
    MOV R3, R3     
    BL printf              
    POP {PC}                
   
_scanf:
    PUSH {LR}               
    PUSH {R1}              
    LDR R0, =format_str    
    SUB SP, SP, #4         
    MOV R1, SP             
    BL scanf               
    LDR R0, [SP]            
    ADD SP, SP, #4       
    POP {R1}               
    POP {PC}              
 
_count_partitions:
    PUSH {LR}               
    CMP R1, #0             
    MOVEQ R0, #1
    POPEQ {PC}             
    MOVLT R0, #0
    POPLT {PC}
     
    CMP R5, #0
    MOVEQ R0, #0
    POPEQ {PC}             
   
    PUSH {R1}              
    PUSH {R5}
    SUB R1, R1, R5
    BL _count_partitions              
    MOV R7, R0
    POP {R5}
    POP {R1}
    PUSH {R7}               
    SUB R5, R5, #1
    BL _count_partitions
    POP {R7}
    ADD R0, R0, R7          
    POP  {PC}              
 
.data
number:         .word       0
format_str:     .asciz      "%d"
printf_str:     .asciz      "There are %d partitions of %d using integers up to %d\n"
