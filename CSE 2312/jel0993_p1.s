/******************************************************************************
* @file jel0993_p1.s
* Simple calculator with sum, logical shift right, or, and max functions
* @author Jason Bernard Lim
******************************************************************************/

.global main
.func main

main:
    BL _scanf
    MOV R1, R0
    BL _getchar
    MOV R3, R0
    BL _scanf
    MOV R2, R0
    BL _compare
    BL _printf
    BL _printnewLine
    B main

_scanf:
    PUSH {LR}         
    MOV R9, R1   
    MOV R10, R2
    SUB SP, SP, #4         
    LDR R0, =format_str    
    MOV R1, SP             
    BL scanf               
    LDR R0, [SP]            
    ADD SP, SP, #4  
    MOV R1, R9 
    MOV R2, R10 
    POP {PC}                                    

 _getchar:
    MOV R9, R1
    MOV R7, #3              
    MOV R0, #0             
    MOV R2, #1              
    LDR R1, =read_char      
    SWI 0                  
    LDR R0, [R1]           
    AND R0, #0xFF 
    MOV R1, R9        
    MOV PC, LR             

_compare:
    MOV R11, LR
    CMP R3, #'+'          
    BLEQ _sum              
    CMP R3, #'|'          
    BLEQ _or                
    CMP R3, #'>'           
    BLEQ _shift_right       
    CMP R3, #'m'            
    BLEQ _max         
    MOV PC, R11      

_sum:
    MOV R0, R1              @copy input R1 and put it in R0
    ADD R0, R2             @add second input R2 into R0
    MOV PC, LR              @return

_or:
    MOV R0, R1              @copy input R1 into R0
    ORR R0, R2              @Perform bitwise or with second input R2 and stroe result into R0
    MOV PC, LR              @return

_shift_right:
    MOV R0, R1              @copy input R1 into R0
    LSR R0, R2              @perforn logical shift right by R2 and store result into R0
    MOV PC, LR              @return

_max:
    CMP R1, R2              @Compare R1 and R2
    MOVGT R0, R1            @If R2 is less than R1 copy R1 into R0
    MOVLS R0, R2            @If R2 is greater than R1 copy it into R0
    MOV PC, LR

_printf:
    MOV R4, LR  
    MOV R1, R0            
    LDR R0, =format_str              
    BL printf            
    MOV PC, R4   

_printnewLine:
    MOV R4, LR              
    LDR R0, =newLine_str              
    BL printf            
    MOV PC, R4   

.data
format_str:     .asciz      "%d"
read_char:      .ascii      " "
printf_str:     .asciz     "%d"
newLine_str:    .asciz     "\n"
