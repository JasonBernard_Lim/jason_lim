/******************************************************************************
* @file jel0993_p2.s
* @author Jason Bernard Lim
* @ID: 1001640993
******************************************************************************/
 
.global main
.func main
main:
    BL _seedrand          
    MOV R0, #0           
writeloop: 
    CMP R0, #10           
    BEQ writedone          
    LDR R1, =a              
    LSL R2, R0, #2         
    ADD R2, R1, R2         
    PUSH {R0}            
    PUSH {R2}              
    BL _getrand             
    BL _mod_unsigned
    POP {R2}                
    STR R0, [R2]           
    POP {R0}              
    ADD R0, R0, #1          
    B   writeloop       
writedone:
    MOV R0, #0             
readloop:
    CMP R0, #10            
    BEQ readextremes            
    LDR R1, =a             
    LSL R2, R0, #2         
    ADD R2, R1, R2         
    LDR R1, [R2]          
    PUSH {R0}             
    PUSH {R1}             
    PUSH {R2}              
    MOV R2, R1             
    MOV R1, R0             
    BL  _printf             
    POP {R2}                
    POP {R1}                
    POP {R0}               
    ADD R0, R0, #1        
    B   readloop           

readextremes: 
   MOV R3, #1000
   MOV R4, #0
   MOV R0, #0
readout: 
    CMP R0, #10            
    BEQ readdone            
    LDR R1, =a              
    LSL R2, R0, #2          
    ADD R2, R1, R2        
    LDR R1, [R2]          
    CMP R1, R3
    MOVLT R3, R1
    CMP R1, R4
    MOVGT R4, R1
    ADD R0, R0, #1
    B   readout           
readdone:
    BL _printMin
    BL _printMax
    B _exit
    
_exit:  
    MOV R7, #4              
    MOV R0, #1              
    MOV R2, #21            
    LDR R1, =exit_str       
    SWI 0                  
    MOV R7, #1             
    SWI 0                  

_mod_unsigned:
    MOV R1, R0
    MOV R2, #1000
    MOV R0, #0          
    B _modloopcheck     
    _modloop:
        ADD R0, R0, #1  
        SUB R1, R1, R2 
    _modloopcheck:
        CMP R1, R2      
        BHS _modloop   
    MOV R0, R1         
    MOV PC, LR            
       
_printf:
    PUSH {LR}               
    LDR R0, =printf_str    
    BL printf              
    POP {PC}                

_printMax:
    PUSH {LR}              
    LDR R0, =printMax_str    
    MOV R1, R4
    BL printf              
    POP {PC}               

_printMin:
    PUSH {LR}              
    LDR R0, =printMin_str     
    MOV R1, R3
    BL printf              
    POP {PC}               

_seedrand:
    PUSH {LR}              
    MOV R0, #0             
    BL time                 
    MOV R1, R0             
    BL srand               
    POP {PC}               
    
_getrand:
    PUSH {LR}               
    BL rand                 
    POP {PC}                
   
.data

.balign 4
a:              .skip       40
printf_str:     .asciz      "a[%d] = %d\n"
printMin_str:     .asciz      "MINIMUM = %d\n"
printMax_str:     .asciz      "MAXIMUM = %d\n"
debug_str:
.asciz "R%-2d   0x%08X  %011d \n"
exit_str:       .ascii      "Terminating program.\n"
