/******************************************************************************
* @file jel0993_p4.s
* @author Jason Bernard Lim
* @ID: 1001640993
******************************************************************************/

.global main
.func main

main:
    BL _scanf
    MOV R1, R0              @R1 = numerator
    BL _scanf
    BL _divide   
    
    VCVT.F64.F32 D4, S2     @ covert the result to double precision for printing
    VMOV R1, R2, D4         @ split the double VFP register into two ARM registers
    BL  _printf_result      @ print the result
    
    B   _main

_divide:
    PUSH {LR}
    VMOV S0, R0             
    VMOV S1, R1            
    VCVT.F32.S32 S0, S0  
    VCVT.F32.S32 S1, S1     
	
    VDIV.F32 S2, S1, S0 
    POP {PC}

_printf_result:
    PUSH {LR}               @ push LR to stack
    LDR R0, =result_str     @ R0 contains formatted string address
    BL printf               @ call printf
    POP {PC}                @ pop LR from stack and return
    
_scanf:
    PUSH {LR}               @ store LR since scanf call overwrites
    SUB SP, SP, #4          @ make room on stack
    LDR R0, =format_str     @ R0 contains address of format string
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL scanf                @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ restore the stack pointer
    POP {PC}                @ return

.data
format_str:     .asciz      "%d"
result_str:     .asciz      "%f\n"