#   Name: Jason Bernard Lim
#   ID#: 1001640993
#   Date: 6-23-2021
#   Assignment 1 Task 1 find_route.py

import sys

######################################################
# Create a state class that is able to manage the
# nodes as well as the information inside of the nodes.
#
# g_n = the cumulative distance used for uninformed search
#       as well as used for tracing
#
# f_n = parameter used for only informed search to track
#       g_n + the value of the heuristic
#    
# name = the name of the current node
#
# parent = the name of the parent of the current node
######################################################

class State:
    def __init__(state_node, g_n, f_n, name, parent):
        state_node.g_n = g_n
        state_node.f_n = f_n
        state_node.name = name
        state_node.parent = None


######################################################
# This function is used to sort the fringe for 
# uninformed search. 
######################################################
def fringeSort(state_node):
    return state_node.g_n

######################################################
# This function is used to sort the fringe for 
# informed search. 
######################################################
def informedfringeSort(state_node):
    return(state_node.f_n)


#Create the main function that is to be used for uninformed search
def uninformed_search():
    #store the start and destination locations
    start = sys.argv[2]
    destination = sys.argv[3]

    #read in the information from the input txt file
    graph = [] #graph = read in list for the map
    file_name = open(sys.argv[1], "r")

    for line in file_name:
        if "END OF INPUT" in line:
            break
        else:
            route = line.split()
            path1 = [route[0],route[1],float(route[2])]
            path2 = [route[1],route[0],float(route[2])]
            graph.append(path1)
            graph.append(path2)

    closed = [] #tracks which nodes have been visited / are in the closed set
    popped = 0 #tracks the number of the nodes popped
    expanded = 0 #tracks the number of the nodes expanded

    generated = 1 #tracks the number of the nodes generated
    #generated is set to 1 to account for the generation of the start node

    failed = False #flag to check whether the search for the destination was succesful or not

    #create the fringe as well as append the start node to it for initial expansion
    fringe = []
    fringe.append(State(0, 0, start, None))

    #initialization of a final node to be used for tracing during final output
    final_node = State(0, 0, start, None)

    #infinite loop to be used based on the graph search pseudo code within the notes
    while True:
        #if the length of the fringe is 0, the search has failed therefore set flag to true
        #then break from the loop
        if len(fringe) == 0:
            failed = True
            break
        else:
            #else, pop the next node from the fringe to be analyzed for possible expansion
            cur = fringe.pop()
            popped += 1 #increment the popped counter

            #if the current node's name is the destination set that node to the final node
            #then break from the loop
            if cur.name == destination:
                final_node = cur
                break
            else:
                #else check if the current node has been visited already
                #if it has continue to the next node, bypassing the current node
                #else process it and add it to the closed set
                if cur.name not in closed:
                    closed.append(cur.name)
                    for edge in graph:
                        #for every edge in the graph,
                        #if the current node's name is the start for an edge
                        if cur.name == edge[0]:
                            #create state_node for that edge with a new g_n
                            successor = State(cur.g_n + edge[2], 0, edge[1], None)
                            #add the parent to the successor then append that to the fringe
                            successor.parent = cur
                            fringe.append(successor)
                            #finally increment the generated counter
                            generated += 1

                    #sort the fringe by g_n and increment the expanded counter
                    fringe.sort(reverse=True, key=fringeSort)
                    expanded += 1 
                else:
                    continue
    
    #FINAL PRINT OUT

    #Print out the necessary information if or if not, the search was completed
    if failed != True:
        print("Nodes Popped: " + str(popped))
        print("Nodes Expanded: " + str(expanded))
        print("Nodes Generated: " + str(generated))
        print("Distance: " + str(final_node.g_n) + " km")

        #To print out the route, create a list of parents then reverse the list
        #to have the list in correct order
        print("Route:")
        route = []
        while final_node.parent != None:
            route.append(final_node.name)
            final_node = final_node.parent
        route.append(final_node.name)
        route.reverse()

        #Search for the correct distance from one node to the other used the graph list
        for i in range(len(route)-1):
            for edge in graph:
                if route[i] == edge[0] and route[i+1] == edge[1]:
                    print(edge[0] + " to " + edge[1] + ", " + str(edge[2]) + " km")
    else:
        print("Nodes Popped: " + str(popped))
        print("Nodes Expanded: " + str(expanded))
        print("Nodes Generated: " + str(generated))
        print("Distance: Infinity" )
        print("Route:\nNone")
    
def informed_search():
    #store the start and destination locations
    start = sys.argv[2]
    destination = sys.argv[3]

    #read in the information from the input txt file
    graph = [] #graph = read in list for the map
    heur = [] #heur = heuristic list
    input_file = open(sys.argv[1], "r")
    heur_file = open(sys.argv[4], "r")

    for line in input_file:
        if "END OF INPUT" in line:
            break
        else:
            route = line.split()
            path1 = [route[0],route[1],float(route[2])]
            path2 = [route[1],route[0],float(route[2])]
            graph.append(path1)
            graph.append(path2)

    for line in heur_file:
        if "END OF INPUT" in line:
            break
        else:
            vals = line.split()
            h = [vals[0], float(vals[1])]
            heur.append(h)

    #NOTE: MOST INFORMATION IS THE SAME AS UNINFORMED SEARCH 
    #      UNINCLUDED TO REDUCE CLUTTER AND REDUNDANCY

    closed = []
    popped = 0
    expanded = 0
    generated = 1 #generated is set to 1 to account for the generation of the start node
    failed = False

    fringe = []
    fringe.append(State(0, 0, start, None))

    final_node = State(0, 0, start, None)

    while True:
        if len(fringe) == 0:
            failed = True
            break
        else:
            cur = fringe.pop()
            popped += 1
            if cur.name == destination:
                final_node = cur
                break
            else:
                if cur.name not in closed:
                    closed.append(cur.name)
                    for edge in graph:
                        if cur.name == edge[0]:
                            #Look for the heuristic value of this specific destination node
                            h_n = 0
                            for place in heur:
                                if place[0] == edge[1]:
                                    h_n = place[1]
                            #create state_node
                            successor = State(cur.g_n + edge[2], cur.g_n + edge[2] + h_n, edge[1], None)
                            successor.parent = cur
                            fringe.append(successor)
                            generated += 1

                    #sort the fringe by f_n which is the cumulative between g_n and the heuristic value
                    fringe.sort(reverse=True, key=informedfringeSort)
                    expanded += 1 
                else:
                    continue
    #PRINT OUT
    if failed != True:
        print("Nodes Popped: " + str(popped))
        print("Nodes Expanded: " + str(expanded))
        print("Nodes Generated: " + str(generated))
        print("Distance: " + str(final_node.g_n) + " km")
        print("Route:")
        route = []
        while final_node.parent != None:
            route.append(final_node.name)
            final_node = final_node.parent
        route.append(final_node.name)
        route.reverse()
        for i in range(len(route)-1):
            for edge in graph:
                if route[i] == edge[0] and route[i+1] == edge[1]:
                    print(edge[0] + " to " + edge[1] + ", " + str(edge[2]) + " km")
    else:
        print("Nodes Popped: " + str(popped))
        print("Nodes Expanded: " + str(expanded))
        print("Nodes Generated: " + str(generated))
        print("Distance: Infinity" )
        print("Route:\nNone")

#MAIN DRIVER 

#if the amount of arguments is 4, that means uninformed
#if the amount of arguments is 5, that meas informed
#else there are not enough arguments
if len(sys.argv) == 4:
    uninformed_search()
elif len(sys.argv) == 5:
    informed_search()
else:
    print("NOT ENOUGH ARGUMENTS")
