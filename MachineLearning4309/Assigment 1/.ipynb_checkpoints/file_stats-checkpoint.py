import math

def file_stats(pathname):
    #set the avg and stdev to 0
    avg = 0
    stdev = 0
    
    #create a list to store the numbers in
    numbers = []
    
    #read the info from the file
    f = open(pathname, "r")
    file = f.readlines()
    for x in file:
        #store the values into the list
        numbers.append(float(x))
        
    #calculate the average    
    for x in range(len(numbers)):
        avg += numbers[x]
        
    avg /= len(numbers)
        
    print(avg)
         
    #calculate the standard deviation     
    for x in range(len(numbers)):
        stdev += (numbers[x] - avg) ** 2
    
    stdev = math.sqrt(stdev / (len(numbers) - 1) )
    
    print(stdev)
    
    return avg, stdev