import math
import sys

#Training Stage
trainingArray = []

if len(sys.argv) < 3:
    print("NOT ENOUGH ARGUMENTS")
    exit(0)
    
with open(sys.argv[1], 'r') as f:
    for line in f.readlines():
        line = line.split()
        line = [float(i) for i in line]
        trainingArray.append(line)
        
trainingDict = {}
trainingMean = []
trainingSTDEV = []
totalItems = len(trainingArray)

for i in range(len(trainingArray)):
    line = trainingArray[i]
    trainingClass = int(line[-1])
    if(trainingClass not in trainingDict):
        trainingDict[trainingClass] = []
    trainingDict[trainingClass].append(line[:-1])
        
classes = []
for i in sorted(trainingDict):
    classes.append(i)
    

P_C = []

#For every class
for i in sorted(trainingDict):
    #set the mean and stdev lists
    mean = [0] * len(trainingDict[i][0])
    stdev = [0] * len(trainingDict[i][0])
    P_C.append((float(len(trainingDict[i]) / totalItems)) )
    
    #Calculate the mean
    for j in range(len(trainingDict[i])):
        for k in range(len(trainingDict[i][j])):
            mean[k] += trainingDict[i][j][k]
                
    for j in range(len(mean)):
        mean[j] = mean[j]/len(trainingDict[i])
    
    #Calculate the standard deviation
    for j in range(len(trainingDict[i])):
        for k in range(len(trainingDict[i][j])):
            stdev[k] += (trainingDict[i][j][k] - mean[k])**2

    for j in range(len(stdev)):
        stdev[j] = math.sqrt( stdev[j] / (len(trainingDict[i]) - 1) )
        if stdev[j] < 0.01:
                stdev[j] = 0.01
                
        #print out the class info required
        print("Class %d, attribute %d, mean = %.2f, std = %.2f" % (i,(j+1),mean[j],stdev[j]))
    trainingMean.append(mean)
    trainingSTDEV.append(stdev)

#Test stage
testArray = []

with open(sys.argv[2], 'r') as f:
    for line in f.readlines():
        line = line.split()
        line = [float(i) for i in line]
        testArray.append(line)

#for every test case
accTotal = 0
for i in range(len(testArray)):
    
    highestProb = 0
    predClass = 0
    N_xTotal = [1] * len(trainingMean)
    accuracy = 1
    
    #for every class
    for j in range(len(trainingMean)):
        
        #for every attribute
        for k in range(len(trainingMean[j])):
            exponent = math.exp(-((testArray[i][k]-trainingMean[j][k])**2 / (2 * trainingSTDEV[j][k]**2 )))
            N_xTotal[j] *= (1 / (math.sqrt(2 * math.pi) * trainingSTDEV[j][k])) * math.exp(-((testArray[i][k]-trainingMean[j][k])**2 / (2 * trainingSTDEV[j][k]**2 )))
            
        N_xTotal[j] *= P_C[j]    
        
    for j in range(len(trainingMean)):
        if (N_xTotal[j]/sum(N_xTotal)) == highestProb:
            accuracy += 1
        elif (N_xTotal[j]/sum(N_xTotal)) > highestProb:
            highestProb = N_xTotal[j]/sum(N_xTotal)
            predClass = classes[j]
            accuracy = 1
    if predClass == int(testArray[i][-1]):
        accuracy = 1/accuracy 
    else:
        accuracy = 0
        
    accTotal += accuracy    
    print("ID=%5d, predicted=%3d, probability = %0.4f, true=%3d, accuracy=%4.2f" % (i+1,predClass, highestProb, int(testArray[i][-1]), accuracy))
    
print("classification accuracy=%.4f" % (accTotal/len(testArray)))   
