package Printtokens;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class PrinttokensTest {

//UNIT TESTS

	/**
	 * Tests for get_token()
	 * input:		BufferedReader br
	 * output: 		String
	 * --------------------------------
	 * Test cases:
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase1.txt" ==> null
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase2.txt" ==> null
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase3.txt" ==> "("
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase4.txt" ==> " " "
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase5.txt" ==> " ;a "
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase6.txt" ==> " aa "
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase7.txt" ==> " "a" "
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase8.txt" ==> " "a; "
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase9.txt" ==> "abc"
	 * "C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase10.txt" ==> "aa"
	 * --------------------------------
	 */
	@Test
	void get_token_test() {
		FileReader fr1 = null;
		FileReader fr2 = null;
		FileReader fr3 = null;
		FileReader fr4 = null;
		FileReader fr5 = null;
		FileReader fr6 = null;
		FileReader fr7 = null;
		FileReader fr8 = null;
		FileReader fr9 = null;
		FileReader fr10 = null;
		try {
			fr1 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase1.txt");
			fr2 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase2.txt");
			fr3 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase3.txt");
			fr4 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase4.txt");
			fr5 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase5.txt");
			fr6 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase6.txt");
			fr7 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase7.txt");
			fr8 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase8.txt");
			fr9 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase9.txt");
			fr10 = new FileReader("C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase10.txt");
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		BufferedReader testcase1 = new BufferedReader(fr1);
		BufferedReader testcase2 = new BufferedReader(fr2);
		BufferedReader testcase3 = new BufferedReader(fr3);
		BufferedReader testcase4 = new BufferedReader(fr4);
		BufferedReader testcase5 = new BufferedReader(fr5);
		BufferedReader testcase6 = new BufferedReader(fr6);
		BufferedReader testcase7 = new BufferedReader(fr7);
		BufferedReader testcase8 = new BufferedReader(fr8);
		BufferedReader testcase9 = new BufferedReader(fr9);
		BufferedReader testcase10 = new BufferedReader(fr10);
		
		Printtokens instance = new Printtokens();
		assertEquals(null, instance.get_token(testcase1));
		
		assertEquals(null, instance.get_token(testcase2));
		
		assertEquals("(", instance.get_token(testcase3));
		
		assertEquals("\"", instance.get_token(testcase4));
		
		assertEquals(";a", instance.get_token(testcase5));
		
		assertEquals("aa", instance.get_token(testcase6));
		
		assertEquals("\"a\"", instance.get_token(testcase7));
		
		assertEquals("\"a;", instance.get_token(testcase8));
		
		assertEquals("abc", instance.get_token(testcase9));
		
		assertEquals("aa", instance.get_token(testcase10));
	}
	
	
	
	
	
	/**
	 * Tests for is_token_end()
	 * input:		int str_com_id, int res
	 * output: 		boolean
	 * --------------------------------
	 * Test cases:
	 * 1,-1 			=====> true
	 * 1,34 			=====> true
	 * 1,97				=====> false
	 * 2,10				=====> true
	 * 2,97				=====> false
	 * 0,40				=====> true
	 * 0,59  			=====> true
	 * 0,97				=====> false
	 * --------------------------------
	 */
	@Test
	void is_token_end_test() {
		assertEquals(true, Printtokens.is_token_end(1, -1));
		
		assertEquals(true, Printtokens.is_token_end(1, 34));
		
		assertEquals(false, Printtokens.is_token_end(1, 97));
		
		assertEquals(true, Printtokens.is_token_end(2, 10));
		
		assertEquals(false, Printtokens.is_token_end(2, 97));
		
		assertEquals(true, Printtokens.is_token_end(0, 40));
		
		assertEquals(true, Printtokens.is_token_end(0, 59));
		
		assertEquals(false, Printtokens.is_token_end(0, 97));
	}

	
	/**
	 * Tests for is_keyword()
	 * input:		String str
	 * output: 		boolean
	 * --------------------------------
	 * Test cases:
	 * "and" 			=====> true
	 * "no"				=====> false
	 * --------------------------------
	 */
	@Test
	void is_keyword_test() {
		assertEquals(true, Printtokens.is_keyword("and"));
		
		assertEquals(false, Printtokens.is_keyword("no"));
	}

	/**
	 * Tests for is_num_constant()
	 * input:		String str
	 * output: 		boolean
	 * --------------------------------
	 * Test cases:
	 * "a" 				=====> false
	 * "1a"				=====> false
	 * "123"			=====> true
	 * --------------------------------
	 */
	@Test
	void is_num_constant_test() {
		assertEquals(false, Printtokens.is_num_constant("a"));
		
		assertEquals(false, Printtokens.is_num_constant("1a"));
		
		assertEquals(true, Printtokens.is_num_constant("123"));
	}
	
	/**
	 * Tests for is_str_constant()
	 * input:		String str
	 * output: 		boolean
	 * -------------------------------------
	 * Test cases:
	 * "no" 					=====> false
	 * ""alphabet"				=====> false
	 * ""alphabet""				=====> true
	 * -------------------------------------
	 */
	@Test
	void is_str_constant_test() {
		assertEquals(false, Printtokens.is_str_constant("no"));
		
		assertEquals(false, Printtokens.is_str_constant("\"alphabet"));
		
		assertEquals(true, Printtokens.is_str_constant("\"alphabet\""));
	}
	
	/**
	 * Tests for is_identifier()
	 * input:		String str
	 * output: 		boolean
	 * --------------------------------
	 * Test cases:
	 * "1" 					=====> false
	 * "a123"				=====> true
	 * "a123!"				=====> false
	 * --------------------------------
	 */
	@Test
	void is_identifier_test() {
		assertEquals(false, Printtokens.is_identifier("1\0"));
		
		assertEquals(true, Printtokens.is_identifier("a123\0"));
		
		assertEquals(false, Printtokens.is_identifier("a123!\0"));
	}	
	
    /**
     * Tests for main()
     * input:      String[] args
     * output:     Console Output
     * ------------------------------
     * Test cases:
     * ["a","a","a"]				======>	"Error! Please give the token stream"
     * [], Console Input = "abc"	======>	"identifier,\"abc\"."
     * ["C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase10.txt"}); ======>	"identifier,\"abc\".identifier,\"aa\".\\ncomment,\";\".
     */
	@Test
	void main_test() throws IOException {
		PrintStream stdout = System.out;
		InputStream stdin = System.in;
		
		ByteArrayOutputStream actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		ByteArrayInputStream input = new ByteArrayInputStream("abc".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("identifier,\"abc\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"a","a","a"});
		assertEquals("Error! Please give the token stream\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase10.txt"});
		assertEquals("identifier,\"aa\".\ncomment,\";\".\n", actual_output.toString());
		
		System.setOut(stdout);
		System.setIn(stdin);
	}
	
    /**
     * End-to-End Test Cases:
     * ------------------------------
     * Test cases:
     * ["a","a","a"]				======>	"Error! Please give the token stream"
     * [], Console Input = "abc"	======>	"numeric,123.\n"
     * [C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\EndtoEnd1.txt] ==> "lparen.\nrparen.\nlsquare.\nrsquare.\nerror,\"/\".\nbquote.\nquote.\ncomma.\n"
     * [], Console Input ""a""  ======> "string,"hello".\n"
     * [], Console Input "and"  ======> "keyword,\"and\".\n"
     * [], Console Input "aa"  ======> "identifier,\"aa\".\n"
     * [], Console Input "#a"  ======> "character,\"a\".\n"
     * [], Console Input ";a"  ======> "comment,\";a\".\n"
     * [], Console Input """  ======> "error,\"\"\".\n"
     * [C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase2.txt] ==> ""
     * [], Console Input "aa("  ======> "identifier,\"aa\".\nlparen.\n"
     * [C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\EndtoEnd2.txt] ==> "comment,\";\".\n"
     * [], Console Input "a!;"  ======> "error,\"a!\".\n"
     * ------------------------------
     */
	@Test
	void EndtoEnd_test() throws IOException {
		PrintStream stdout = System.out;
		InputStream stdin = System.in;
		
		ByteArrayOutputStream actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		ByteArrayInputStream input = new ByteArrayInputStream("123".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("numeric,123.\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"a","a","a"});
		assertEquals("Error! Please give the token stream\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\EndtoEnd1.txt"});
		assertEquals("lparen.\n"
				+ "rparen.\n"
				+ "lsquare.\n"
				+ "rsquare.\n"
				+ "error,\"/\".\n"
				+ "bquote.\n"
				+ "quote.\n"
				+ "comma.\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("\"a\"".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("string,\"a\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("and".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("keyword,\"and\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("aa".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("identifier,\"aa\".\n", actual_output.toString());
		
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("#a".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("character,\"a\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream(";a".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("comment,\";a\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("\"".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("error,\"\"\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\testcase2.txt"});
		assertEquals("", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("aa(".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("identifier,\"aa\".\n"
				+ "lparen.\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		Printtokens.main(new String[] {"C:\\Users\\Jason_Lim's BB\\eclipse-workspace\\Printtokens\\src\\Printtokens\\EndtoEnd2.txt"});
		assertEquals("comment,\";\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("1a;".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("error,\"1a\".\n"
				+ "comment,\";\".\n", actual_output.toString());
		
		actual_output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(actual_output));
		input = new ByteArrayInputStream("a!".getBytes());
		System.setIn(input);
		Printtokens.main(new String[] {});
		assertEquals("error,\"a!\".\n", actual_output.toString());
		
		System.setOut(stdout);
		System.setIn(stdin);
	}
	
}
