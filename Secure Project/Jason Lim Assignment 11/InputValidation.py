import re
import sys 
import os
import datetime

#regex for the phone number input validation
phone_number = re.compile(r"""
    ^(
    ([0-9]{5})      #5 digit internal 
    |       
    #North American Phone Number
    (\+)? (1)? ([(\s\-\.])?  
    ([0-9]{3})? ([)\s\-\.])?    #area code
    ([0-9]{3}) ([\s\-\.]) ([0-9]{4}) #subsriber number
    |
    #International number
    (\+)? ([0-9]{2,3})? (\s)? 
    (1)? (\s)?
    ([(\s\-\.])? ([0-9]{2,3})? ([)\s\-\.])?    #area code
    ([0-9]{3}) ([\s\-\.])? ([0-9]{4}) #subsriber number
    |
    (\+) ([0-9]{2,3})? (\s)? 
    (1)? (\s)?
    ([(\s\-\.])? ([0-9]{2})? ([)\s\-\.])?    #area code
    (\s)?
    ([0-9]{3}) ([\s\-\.])? ([0-9]{4}) #subsriber number
    |
    ([0-9]{5}) ([\s\.]) ([0-9]{5})
    |
    #Denmark number
    (\+)? (45)? (\s)?
    (([0-9]{2}) \s ([0-9]{2}) \s ([0-9]{2}) \s ([0-9]{2}))?
    (([0-9]{4})([\s\.])([0-9]{4}))?
    )
    $""", re.VERBOSE)

#regex for the name input validation
person_name = re.compile(r"""
    ^(
    ([A-Za-z])+  # first name
    (\s)?    
    (([A-Za-z])+)?     #optional middle
    (\s)?
    ((O’)? ([A-Za-z])+ -? ([A-Za-z]+)?)      # last name
    |
    ((O’)? ([A-Za-z])+ -? ([A-Za-z]+)?)      # last name
    (,\s)
    ([A-Za-z])+  # first name
    (\s)?
    ([A-Za-z]\.)?     #optional middle initial
    (([A-Za-z])+)?     #optional middle name
    )
    $""", re.VERBOSE)

#function to handle delete case when you have to rewrite the listing
def new_listing(listing):
    file = open("Data.txt", 'w')
    for entry in listing:
        file.write(entry[0] +"|"+entry[1])
    file.close()

#audit log stuff
def audit(command):
    file = open("log.txt", "a")
    ts = datetime.datetime.now()
    ts = str(ts)
    uid = os.getuid()
    uid = str(uid)
    file.write("RUID: " + uid + " " + ts + " " + command + "\n")
    file.close()    

#read in the current phone listing
phone_listing = []

with open("Data.txt", 'r') as f:
    for line in f.readlines():
        line = line.split('|')
        line = [i for i in line]
        phone_listing.append(line)

#if no args input, send out help info
if(len(sys.argv) < 2):
    print("ADD \"<Person>\" \"<Telephone #>\" - Add a new person \nDEL \"<Person>\" - Remove someone by name\nDEL \"<Telephone #>\" - Remove someone by telephone #\nLIST - Produce a list of the members of the database")
    exit(0)

#if there are too many args, print out and exit(1)
if(len(sys.argv) > 4):
    print("TOO MANY ARGS")
    exit(1)


#ADD Functionality
if(sys.argv[1] == 'ADD'): 
    #validate the input, if valid append to the existing document
    #else, state so to the user then exit
    Name = sys.argv[2]
    Number = sys.argv[3]
    if (re.match(person_name, Name) and re.match(phone_number, Number) != None):
        file = open("Data.txt", 'a')
        file.write(Name+"|"+Number+"\n")
        file.close()
        #audit log append
        audit("ADD " + Name)
        exit(0)
    else: 
        print("INCORRECT INPUT TYPE ENTERED")
        exit(1)

#DEL Functionality
elif (sys.argv[1] == 'DEL'):
    to_Del = sys.argv[2]
    #if too many arguments present for a delete, state so then exit
    if(len(sys.argv) > 3):
        print("TOO MANY ARGS FOR DEL STATEMENT")
        exit(1)

    #delete via person
    #validate input, if valid name search for all entries that contain that name and delete them
    #ASSUMPTION: MANY PEOPLE HAVE THE SAME FIRST NAME/LAST NAME, MUST BE SPECIFIC WITH INPUT
    if (re.match(person_name, to_Del) != None):
        for entry in phone_listing:
            if(to_Del in entry[0]):
                phone_listing.remove(entry)
        new_listing(phone_listing)
        #audit log append
        audit("DEL " + to_Del)

    #delete via phone number
    #if not valid under person name, must be valid under number
    #if valid, delete all entries with that number
    elif(re.match(phone_number, to_Del) != None):
        for entry in phone_listing:
            if(to_Del in entry[1]):
                phone_listing.remove(entry)
        new_listing(phone_listing)

    else:
        print("INCORRECT INPUT TYPE ENTERED")
        exit(1)

#LIST Functionality
elif (sys.argv[1] == 'LIST'):
    print("###### CURRENT TELEPHONE LISTING ######")
    for val in phone_listing:
        print("Name: " + val[0] + "\nTelephone #: " + val[1])
    #audit log append
    audit("LIST")

else:
    print("INCORRECT INPUT TYPE ENTERED")
    exit(1)